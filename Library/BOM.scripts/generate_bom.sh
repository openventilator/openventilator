#!/bin/bash

echo "Generate BOM file"

# Problem with space in filenames

BlowerPCBNetlist="./BlowerDriver/ctrb0505-A.xml"
BlowerBOMFile=BOMBlowerDriver.html

Battery_ConnectionPCBNetlist="./Battery_Connection/10096510BatteryConnection.xml"
Battery_ConnectionBOMFile=BOMBattery_Connection.html

BMS_PCBPCBNetlist="./BMS_PCB/PB560_BMS.xml"
BMS_PCBBOMFile=BOMPB560_BMS.html

CPU_RB0505PCBNetlist="./CPU_RB0505/10125623A00.xml"
CPU_RB0505BOMFile=BOMCPU_RB0505.html

Power_ManagementPCBNetlist="./Power_Management/10005011PowerManagement.xml"
Power_ManagementBOMFile=BOMPower_Management.html

PowerPackPCBNetlist="./PowerPack/10106052PowerPack.xml"
PowerPackBOMFile=BOMPowerPack.html

# Generate xml file
#xsltproc -o "%O.csv" "/usr/share/kicad/plugins/bom2grouped_csv.xsl" "%I"

# python3 "/home/olivier/openventilator/kicaKiBoM-master/KiBOM_CLI.py" "%I" "%O.html"

python3 "./KiBoM-master/KiBOM_CLI.py" $BlowerPCBNetlist --cfg ./bom.ini -d "./BlowerDriver" $BlowerBOMFile
python3 "./KiBoM-master/KiBOM_CLI.py" $Battery_ConnectionPCBNetlist --cfg ./bom.ini -d "./Battery_Connection" $Battery_ConnectionBOMFile
python3 "./KiBoM-master/KiBOM_CLI.py" $BMS_PCBPCBNetlist --cfg ./bom.ini -d "./BMS_PCB" $BMS_PCBBOMFile
python3 "./KiBoM-master/KiBOM_CLI.py" $CPU_RB0505PCBNetlist --cfg ./bom.ini -d "./CPU_RB0505" $CPU_RB0505BOMFile
python3 "./KiBoM-master/KiBOM_CLI.py" $Power_ManagementPCBNetlist --cfg ./bom.ini -d "./Power_Management" $Power_ManagementBOMFile
python3 "./KiBoM-master/KiBOM_CLI.py" $PowerPackPCBNetlist --cfg ./bom.ini -d "./PowerPack" $PowerPackBOMFile
