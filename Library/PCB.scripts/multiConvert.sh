#!/bin/bash
# Run as source multiConvert.sh
#
# This script will convert the Zuken files to JSON.
# The output will be stored in the same directory as the path variable, with an extension .json for each file
#
# The output can be visualized with e.g. http://jsonviewer.stack.hu/ using copy-paste, or by any other JSON viewer

python3 zuken2json.py --path ../../Zuken/cbr0505-A     --fname cbr0505-A     --name cbr0505-03
python3 zuken2json.py --path ../../Zuken/ctrb0505-A    --fname ctrb0505-A
python3 zuken2json.py --path ../../Zuken/GR106771-A    --fname GR106771-A
python3 zuken2json.py --path ../../Zuken/GR106772-A    --fname GR106772-A
python3 zuken2json.py --path ../../Zuken/powersupply-c --fname powersupply-c
