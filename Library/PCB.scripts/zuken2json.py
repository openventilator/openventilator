#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#__author__ = Fernando Hueso-González'

"""
Script to convert Zuken CR-5000 ASCII PCB files to JSON
    Board file (.brd)
    Footprint ASCII file (.ftf)
    PC Board ASCII file (.pcf)
    Library file (.pro)
    http://www.mweda.com/cst/cst2013/mergedProjects/CST_PARTICLE_STUDIO/common_tools/import_zcr5000.htm
Developed in the context of the KiCAD OpenVentilator Group http://openventilator.gitlab.io/
"""

import argparse
import sys
import os
import re
from typing import NamedTuple
import json

#import pcbnew
#sys.path.append('kicad-python')
#from kicad.pcbnew import Arc, Circle, Line, Polygon
#from kicad.util import Point2D
#from kicad.pcbnew import Via

logFileName = '/tmp/log.json'
decimalFormat = "([-]?\d+\.\d+)"
#[-+]?(\d+([.,]\d*)?|[.,]\d+)([eE][-+]?\d+)? https://stackoverflow.com/questions/4703390/how-to-extract-a-floating-number-from-a-string
uintFormat = "([0-9]+)"
intFormat = "([-]?\d+)"
symbolFormat = r'(\b[A-Za-z0-9_\.+-]+\b)'
placementFormat = r'(\bTOP\b|\bBOTTOM\b|\bALL\b|\bBOTH\b)'

# https://stackoverflow.com/questions/35988/c-like-structures-in-python/31062667
class Point4(NamedTuple):
    """
    Class describing a Zuken Point with an index ID, a 2D point x and y in mm, and an angle a in degrees
    """
    ID: int
    x: float
    y: float
    a: float
    
class Hole6(NamedTuple):
    """
    Class describing a Zuken Hole with hole diameter in mm, x and y in mm, and 3 string tags (electrical, reference, type)
    """
    d: float
    x: float
    y: float
    e: str
    r: str
    t: str
    
class Module(NamedTuple):
    """
    Class describing a Zuken Module with footprint name, value, symbol reference, x y z position in mm, and rotation angle in degrees, layer and fixation type
    """
    name: str
    value: str
    ref: str
    x: float
    y: float
    z: float
    a: float
    l: str
    t: str
    

def ParseHoles(lines, startPosition, startTag, stopTag, headerFormat):
    """
    Parses an array of strings containing a Zuken drilled holes into a set of holes (7 elements).
    The array of string includes one line with the START flag, and one line with the STOP flag.
    :param: lines an array of strings with each line of the ASCII file
    :param: startPosition the index in the array where to start the search
    :param: startTag the string marking the start flag
    :param: stopTag the string marking the stop flag
    :param: headerFormat a line before the set of points with meta information
    """
    
     # Check that the provided start position is correct
    if startPosition < 0 or startPosition >= len(lines):
        print('Incorrect offset provided',startPosition,'vs',len(lines))
        sys.exit()
    # Check that the provided startTag is correct
    if lines[startPosition] != startTag:
        print('Incorrect startTag',startTag,'at offset',startPosition)
        sys.exit()
        
    #Find stopTag
    stopPosition = lines[startPosition:].index(stopTag) + startPosition
    if stopPosition >= len(lines) or stopPosition < startPosition:
        print('Incorrect stop position')
        sys.exit()
    blockLines = stopPosition - startPosition + 1
    dataLines = blockLines - 2 # We remove the start line and the stop line 
    
    # Create the regexp of this outline
    holeFormat = startTag + headerFormat
    for i in range(dataLines):
        holeFormat += decimalFormat + ' ' + decimalFormat + ' ' + decimalFormat + r' (\bNPTH\b) (\b[A-Z]+[0-9]+[-]?[A-z]*[0-9]*\b|\bBOARD\b) (\bPIN\b|\bVIA\b) ECAD\n'
    holeFormat += stopTag
    
    # Convert hole array to single string and apply regexp
    holes = ''.join(lines[startPosition:stopPosition+1])
    m = re.match(holeFormat,holes)
    
    if m is None:
        print('not found pattern')
        print(holes)
        sys.exit()
    
    # Extract header
#    headerCaptures = re.compile(headerFormat).groups
#    header = m.groups()[:headerCaptures]
    
    # Extract data and convert to Point4
    dataOffset = 1
    holeData = []
    dw = 6
    for i in range(dataLines):
        m.group
        holeData.append(Hole6(    float(m.group(dataOffset+i*dw+0)),
                                  float(m.group(dataOffset+i*dw+1)),
                                  float(m.group(dataOffset+i*dw+2)),
                                       (m.group(dataOffset+i*dw+3)),
                                       (m.group(dataOffset+i*dw+4)),
                                       (m.group(dataOffset+i*dw+5))))
        
    oName = startTag.rstrip('\n')
    jhead = ''
#    jhead = '{'
    jhead += '"'+ oName +'": {'
#    for i in range(headerCaptures):
#        jhead += '"hc": "' + header[i] + '",'
    for hole in holeData:
        jhead += '"hole":   {'
        jhead += '"d": ' + str(hole.d) + ','
        jhead += '"x": ' + str(hole.x) + ','
        jhead += '"y": ' + str(hole.y) + ','
        jhead += '"e":"' + str(hole.e) + '",'
        jhead += '"r":"' + str(hole.r) + '",'
        jhead += '"t":"' + str(hole.t) + '",'
        jhead +=                          '},'
    jhead +=               '},'
#    jhead+='}'
    
    return blockLines, jhead, holeData


def ParseOutline(lines, startPosition, startTag, stopTag, headerFormat):
    """
    Parses an array of strings containing a Zuken outline into a set of 4D points (id, x, y, angle).
    The array of string includes one line with the START flag, one line with a header metainformation, and one line with the STOP flag.
    :param: lines an array of strings with each line of the ASCII file
    :param: startPosition the index in the array where to start the search
    :param: startTag the string marking the start flag
    :param: stopTag the string marking the stop flag
    :param: headerFormat a line before the set of points with meta information
    """

    # Check that the provided start position is correct
    if startPosition < 0 or startPosition >= len(lines):
        print('Incorrect offset provided',startPosition,'vs',len(lines))
        sys.exit()
    # Check that the provided startTag is correct
    if lines[startPosition] != startTag:
        print('Incorrect startTag',startTag,'at offset',startPosition, lines[startPosition])
        sys.exit()
    
    #Find stopTag
    stopPosition = lines[startPosition:].index(stopTag) + startPosition
    if stopPosition >= len(lines) or stopPosition < startPosition:
        print('Incorrect stop position')
        sys.exit()
    blockLines = stopPosition - startPosition + 1
    dataLines = blockLines - 3 # We remove the start line, the header, and the stop line 
    
    # Create the regexp of this outline
    outlineFormat = startTag + headerFormat
    for i in range(dataLines):
        outlineFormat += uintFormat + ' ' + decimalFormat + ' ' + decimalFormat + ' ' + decimalFormat + '\n'
    outlineFormat += stopTag
    
    # Convert outline on array to single string and apply regexp
    outline = ''.join(lines[startPosition:stopPosition+1])
    m = re.match(outlineFormat,outline)

    if m is None:
        print('not found pattern')
        print(outline)
        sys.exit()
    
    # Extract header
    headerCaptures = re.compile(headerFormat).groups
    header = m.groups()[:headerCaptures]
        
    # Extract data and convert to Point4
    dataOffset = headerCaptures + 1
    outlineData = []
    dw = 4
    for i in range(dataLines):
        outlineData.append(Point4(  int(m.group(dataOffset+i*dw+0)),
                                  float(m.group(dataOffset+i*dw+1)),
                                  float(m.group(dataOffset+i*dw+2)),
                                  float(m.group(dataOffset+i*dw+3))))
        
    isBoard = (startTag == '.BOARD_OUTLINE ECAD\n')
    isOther = (startTag == '.OTHER_OUTLINE ECAD\n')
    isRoute = (startTag == '.ROUTE_OUTLINE ECAD\n' or startTag == '.ROUTE_KEEPOUT ECAD\n')
    isPlace = (startTag == '.PLACE_OUTLINE ECAD\n')
    isKeepout = (startTag == '.PLACE_KEEPOUT ECAD\n')
    isLibrary = (startTag == '.ELECTRICAL\n')
    oName = startTag.rstrip('\n')
    jhead = ''
#    jhead = '{'
    jhead += '"'+ oName +'": {'
    
    if isBoard:
        if headerCaptures!=1:
            print('Unexpected headerCaptures',headerCaptures)
            sys.exit()
        jhead += '"thickness": ' + header[0] + ','
    elif isOther:
        if headerCaptures!=3:
            print('Unexpected headerCaptures',headerCaptures)
            sys.exit()
        jhead += '"name": "' + header[0] + '",'
        jhead += '"thickness": ' + header[1] + ','
        jhead += '"layer": "' + header[2] + '",'
    elif isRoute:
        if headerCaptures!=1:
            print('Unexpected headerCaptures',headerCaptures)
            sys.exit()
        jhead += '"layer": "' + header[0] + '",'
    elif isPlace:
        if headerCaptures!=2:
            print('Unexpected headerCaptures',headerCaptures)
            sys.exit()
        jhead += '"layer": "' + header[0] + '",'
        jhead += '"thickness": ' + header[1] + ','
    elif isKeepout:
        if headerCaptures!=2:
            print('Unexpected headerCaptures',headerCaptures)
            sys.exit()
        jhead += '"layer": "' + header[0] + '",'
        jhead += '"thickness": ' + header[1] + ','
    elif isLibrary:
        if headerCaptures!=4:
            print('Unexpected headerCaptures',headerCaptures)
            sys.exit()
        jhead += '"name": "' + header[0] + '",'
        jhead += '"value": "' + header[1] + '",'
        jhead += '"units": "' + header[2] + '",'
        jhead += '"factor": ' + header[3] + ','
    else:
        print('Unrecognized header format',header,startTag)
        sys.exit()
#        for i in range(headerCaptures):
#            jhead += '"hc": "' + header[i] + '",'
    for outline in outlineData:
        jhead += '"outline_'+str(outline.ID)+'":   {'
#        jhead += '"id": ' + str(outline.ID) + ','
        jhead += '"x": ' + str(outline.x) + ','
        jhead += '"y": ' + str(outline.y) + ','
        jhead += '"a": ' + str(outline.a) + ','
        jhead +=                          '},'
    jhead +=                 '},'
#    jhead+='}'
    
    return blockLines, jhead, outlineData
    
    
def ParseHeader(lines, name, extraLine):
    """
    Parses the .pro header
    :param: lines an array of strings with each line of the ASCII file
    :param: name the name of the parsed file without extension
    """
    headerPosition = 0
    headerLines = 3 if extraLine == "" else 4
    headerFormat = ''
    headerFormat += '.HEADER\n'
    headerFormat += '(BOARD_FILE|LIBRARY_FILE) (3.0) "(CR-5000 Board Designer) V(\d+).(\d\d\d)" (\d\d\d\d)/(\d\d)/(\d\d).(\d\d):(\d\d):(\d\d) (\d)\n'
    headerFormat += extraLine
    headerFormat += '.END_HEADER\n'
    
    header = ''.join(lines[headerPosition:headerPosition+headerLines])
    m = re.match(headerFormat,header)
    if m is None:
        print('not found pattern')
        print(header)
        print(headerFormat)
        sys.exit()
            
    fileType, fformat, sw, major, minor, year, month, day, hour, minutes, seconds, zone = m.groups()
    
    jhead = ''
#    jhead = '{'
    jhead += '"header": {'
    jhead +=             ' "name": "' + name + '",'
    jhead +=             ' "type": "' + fileType + '",'
    jhead +=             ' "fformat": "' + fformat + '",'
    jhead +=             ' "sw": "'+sw+'",'
    if 'MM' in extraLine:
        jhead +=             ' "units": "MM",'
    jhead +=             ' "MajorVersion": "' + major + '",' 
    jhead +=             ' "MinorVersion": "' + minor + '",'
    jhead +=             ' "Year": "' + year + '",'
    jhead +=             ' "Month": "' + month + '",'
    jhead +=             ' "Day": "' + day + '",'
    jhead +=             ' "Hour": "' + hour + '",'
    jhead +=             ' "Minutes": "' + minutes + '",'
    jhead +=             ' "Seconds": "' + seconds + '",'
    jhead +=             ' "Zone": "' + zone + '",'
    jhead +=           '},'
#    jhead+='}'
    
    return headerLines, jhead


def ParseModulePlacement(lines, startPosition):
    """
    Parses an array of strings containing the Zuken module placement.
    The array of string includes one line with the START flag and one line with the STOP flag.
    :param: lines an array of strings with each line of the ASCII file
    :param: startPosition the index in the array where to start the search
    :param: startTag the string marking the start flag
    :param: stopTag the string marking the stop flag
    :param: headerFormat a line before the set of points with meta information
    """
    
    startTag = '.PLACEMENT\n'
    stopTag = '.END_PLACEMENT\n'
    
    # Check that the provided start position is correct
    if startPosition < 0 or startPosition >= len(lines):
        print('Incorrect offset provided',startPosition,'vs',len(lines))
        sys.exit()
    # Check that the provided startTag is correct
    if lines[startPosition] != startTag:
        print('Incorrect startTag',startTag,'at offset',startPosition)
        sys.exit()
    
    #Find stopTag
    stopPosition = lines[startPosition:].index(stopTag) + startPosition
    if stopPosition >= len(lines) or stopPosition < startPosition:
        print('Incorrect stop position')
        sys.exit()
    blockLines = stopPosition - startPosition + 1
    dataLines = blockLines - 2 # We remove the start line and the stop line 
    if dataLines%2 != 0:
        print('Incorrect format of modules')
        sys.exit()
    
    # Create the regexp of this set of modules
    dataOffset = 1
    moduleData = []
#    dw = 9
    
    moduleFormat = decimalFormat + ' ' + decimalFormat + ' ' + decimalFormat + ' ' + decimalFormat + ' ' + r'(\bTOP\b|\bBOTTOM\b|\bALL\b) (\bFIXED\b|\bPLACED\b)' + '\n'
    for i in range(dataLines//2):
        bindex = startPosition+1+i*2
        ids = lines[bindex].strip('\n').split(' ')
        modules = lines[bindex+1]
#        print(i,ids,modules)
        
        if len(ids)!=3:
            print('wrong IDs',ids)
            sys.exit()
        
        m = re.match(moduleFormat,modules)
        if m is None:
            print('not found pattern')
            print(modules)
            sys.exit()
            
        # Extract data and convert to Module format
        moduleData.append(Module(   str(ids[0]),
                                    str(ids[1]),
                                    str(ids[2]),
                                  float(m.group(dataOffset+0)),
                                  float(m.group(dataOffset+1)),
                                  float(m.group(dataOffset+2)),
                                  float(m.group(dataOffset+3)),
                                    str(m.group(dataOffset+4)),
                                    str(m.group(dataOffset+5))
                                    ))   
        
    oName = startTag.rstrip('\n')
    jhead = ''
#    jhead = '{'
    jhead += '"'+ oName +'": {'
    for module in moduleData:
        jhead += '"module":   {'
        jhead += '"name": "' + module.name + '",'
        jhead += '"value": "' + module.value + '",'
        jhead += '"ref": "' + module.ref + '",'
        jhead += '"x": ' + str(module.x) + ','
        jhead += '"y": ' + str(module.y) + ','
        jhead += '"z": ' + str(module.z) + ','
        jhead += '"a": ' + str(module.a) + ','
        jhead += '"l": "' + module.l + '",'
        jhead += '"t": "' + module.t + '",'
        jhead +=                          '},'
    jhead +=                 '},'
#    jhead+='}'
    
    return blockLines, jhead, moduleData

def ParseZukenLine(line,i):
    """
    Converts Zuken parentheses line to JSON format
    :param: line a stringn with one line and parentheses
    :param: index of this line in the file
    """
    
    # Define input and output format in regex of different lines
    
    cirForm = r'\s*\(circle (\d+)\n'
    cirJSON = r'"circle": {"r": \1,'

    fspForm = r'\s*\((footprintSpec|string|comment) ""\)\n'
    fspJSON = r'"\1": "",'

    fForm = r'\s*\(footprint (.+)[ ]?\(side B\)\)\n'
    fJSON = r'"footprint": {"name": "\1", "side": "B"},'
    
    psForm = r'\s*\((pad|padstack) (.+) \(side B\) \(flip X\)\)\n'
    psJSON = r'"\1": {"name": "\2", "side": "B", "flip": "X"},'
    
    psForm2 = r'\s*\((pad|padstack|footprint|package|part) (.+)\)\n'
    psJSON2 = r'"\1": "\2",'
    
    fromForm = r'\s*\(fromTo (\d+)\)\n'
    fromJSON = r'"fromTo": {"from": \1},'

    fromForm2 = r'\s*\(fromTo (\d+) (\d+)\)\n'
    fromJSON2 = r'"fromTo": {"from": \1, "To": \2},'
    
    conductForm = r'\s*\(nonConductive "(.+)"\)\n'
    conductJSON = r'"nonConductive": "\1",'

    conductForm2 = r'\s*\(nonConductive (.+)\)\n'
    conductJSON2 = r'"nonConductive": "\1",'

    lconductForm = r'\s*\(layer \((conductive|nonConductive) "(.+)"\)\n'
    lconductJSON = r'"layer": {"type": "\1", "name": "\2",'

    lconductForm2 = r'\s*\(layer \((conductive|nonConductive) (.+)\)\n'
    lconductJSON2 = r'"layer": {"type": "\1", "name": "\2",'

    oneTagForm = r'\s*\((\w+)\s*\n'
    oneTagJSON   = r'"\1": {'
    
    sDateForm = r'\s*\(string (\d\d.\d\d|\d\d)\)\n'
    sDateJSON = r'"string": "\1",'

    tagIntegerForm = r'\s*\((\w+) ([-]?\d+)\)\n'
    tagIntegerJSON = r'"\1": \2,'

    tagDecimalForm = r'\s*\((\w+) ([-]?\d+\.\d+)\)\n'
    tagDecimalJSON = r'"\1": \2,'

    tagValueForm = r'\s*\((\w+) (\w+\d*|\d+\.\d+|[0-9])\)\n'
    tagValueJSON = r'"\1": "\2",'

    ctagDateForm = r'\s*; \((\w+) (\d\d\d\d-\d\d-\d\d-\d\d:\d\d:\d\d)\)\n'
    ctagDateJSON = r'"\1": "\2",' 

    ctagDateForm2 = r'\s+\((\w+) (\d\d\d\d-\d\d-\d\d-\d\d:\d\d:\d\d)\)\n'
    ctagDateJSON2 = r'"\1": "\2",' 
    
    ctagValueForm = r'\s*;\s\((\w+) (\w+|\d+\.\d+|[0-9])\)\n'
    ctagValueJSON = r'"\1": "\2",'
    
    closeParForm = r'\s*\)\n'
    closeParJSON = r'},'
    
    sLayerForm = r'\s*\(layer \(systemLayer \(type (\w+)\)\)\n'
    sLayerJSON = r'"layer": {"systemLayer": {"type": "\1"},'

    sLayerForm2 = r'\s*\(layer \(infoOf \(systemLayer \(type (\w+)\)\)\)\n'
    sLayerJSON2 = r'"layer": {"infoOf": {"systemLayer": {"type": "\1"}},'
    
    sLayerForm3 = r'\s*\(layer \(infoOf \(conductive (\d+)\)\)\n'
    sLayerJSON3 = r'"layer": {"infoOf": {"conductive": \1},'    
        
    layerForm = r'\s*\(layer "(.+)" \(type ([A-Z]+)\)\)\n'#    https://stackoverflow.com/questions/9085558/python-regex-match-text-between-quotes
    layerJSON = r'"layer": {"name": "\1", "type": "\2"},'

    layerForm2 = r'\s*\(layer (.+) \(type ([A-Z]+)\)\)\n'#    https://stackoverflow.com/questions/9085558/python-regex-match-text-between-quotes
    layerJSON2 = r'"layer": {"name": "\1", "type": "\2"},'   

    layerForm3 = r'\s*\((layer|footLayer) "(.+)"\)\n'#    https://stackoverflow.com/questions/9085558/python-regex-match-text-between-quotes
    layerJSON3 = r'"\1": "\2",'
    
    propSForm = r'\s*\(propertyS (\w+) "(.*)"\)\n'
    propSJSON = r'"propertyS": {"name": "\1", "value": "\2"},'
    
    propSForm2 = r'\s*\(propertyS (\w+) (.+)\)\n'
    propSJSON2 = r'"propertyS": {"name": "\1", "value": "\2"},'

    propIForm = r'\s*\(propertyI (\w+) ([-]?\d+)\)\n'
    propIJSON = r'"propertyI": {"name": "\1", "value": \2},'
    
    propFForm = r'\s*\(propertyF (\w+) ([-]?\d+\.\d+)\)\n'
    propFJSON = r'"propertyF": {"name": "\1", "value": \2},'
    
    ptForm = r'\s*\(pt '+ intFormat+' '+ intFormat + '\)\n'
    ptJSON = r'"pt": {"x": \1, "y": \2},'
    
    ptOpenForm = r'\s*\(pt '+ intFormat+' '+ intFormat + '\n'
    ptOpenJSON = r'"pt": {"x": \1, "y": \2,'

    centerForm = r'\s*\(center '+ intFormat+' '+ intFormat + '\)\n'
    centerJSON = r'"center": {"x": \1, "y": \2},'

    tarcForm = r'\s*\(tarc (\w+) \(r '+uintFormat+'\)\)\n'
    tarcJSON = r'"tarc": {"name": "\1", "r": \2},'
    
    padForm = r'\s*\(pad (.+)\n'
    padJSON = r'"pad": {"name": "\1",'
    
    holeForm = r'\s*\(hole \((\w+) HOLE\)\n'
    holeJSON = r'"hole": {"\1": "HOLE",'

    padSetForm = r'\s*\(padSet \((\w+) (.+)\)\n'
    padSetJSON = r'"padSet": {"\1": "\2",'
    
    connectForm = r'\s*\((connect|noconnect|thermal|clearance)\s+\(pad (.+)\)\)\n'
    connectJSON = r'"\1": {"pad": "\2"},'

    padstackForm = r'\s*\(padstack (.+)\n'
    padstackJSON = r'"padstack": {"name": "\1",'

    drLayerForm = r'\s*\(layer \(drawOf \((nonConductive|conductive|systemLayer) \(type (\w+)\)\)\)\n'
    drLayerJSON = r'"layer": {"drawOf": "\1", "type": "\2",'

    drLayerForm2 = r'\s*\(layer \(drawOf \((nonConductive|conductive|systemLayer) "(.+)"\)\)\n'
    drLayerJSON2 = r'"layer": {"drawOf": "\1", "name": "\2",'

    drLayerForm3 = r'\s*\(layer \(drawOf \((nonConductive|conductive|systemLayer) (.+)\)\)\n'
    drLayerJSON3 = r'"layer": {"drawOf": "\1", "name": "\2",'    

    flayerForm = r'\s*\(layer \((\w+) "(.+)"\)\n'
    flayerJSON = r'"layer": {"name": "\2", "type": "\1",'
    
    flayerForm2 = r'\s*\(layer \((\w+) (.+)\)\n'
    flayerJSON2 = r'"layer": {"name": "\2", "type": "\1",'
    
    fprintForm = r'\s*\(footprint (.+)\n'
    fprintJSON = r'"footprint": {"name": "\1",'
    
    fplayerForm = r'\s*\(layer \((.+)\)\n'
    fplayerJSON = r'"layer": {"name": "\1",'
    
    psgForm = r'\s*\(padstackGroup (\w+) \(padstack (.+)\)\)\n'
    psgJSON = r'"padstackGroup": {"type": "\1", "padstack": "\2"},'
    
    pinForm = r'\s*\(pin (.+)\n'
    pinJSON = r'"pin": {"name": "\1",'
    
    stringParsForm = r'\s*\(string "(.+ \(\d/\d\))\n'
    stringParsJSON = r'"string": "\1\\n'
    
    stringForm = r'\s*\(string "(.+)"\)\n'
    stringJSON = r'"string": "\1",'

    breakStringForm = r'\s*\(string "(.+)\n'
    breakStringJSON = r'"string": "\1\\n'
    
    stringForm2 = r'\s*\(string (.+)\)\n'
    stringJSON2 = r'"string": "\1",'
            
    breakStringForm2 = r'\s*(.+)"\)\n'
    breakStringJSON2 = r'\1",'

    pLayerForm = r'\s*\((layer) "(.+)"\)\n'
    pLayerJSON = r'"\1": {"name": "\2"},'
    
    pLayerForm2 = r'\s*\((layer|footLayer) (.+)\)\n'
    pLayerJSON2 = r'"\1": {"name": "\2"},'
    
    referForm = r'\s*\(refer "(.+)"\n'
    referJSON = r'"refer": {"name": "\1",'
    
    referForm2 = r'\s*\(refer (.+)\n'
    referJSON2 = r'"refer": {"name": "\1",'
    
    systemForm = r'\s*\(systemLayer \(type (\w+)\)\)\n'
    systemJSON = r'"systemLayer": {"type": "\1"},'
    
    drawForm = r'\s*\((\w+) \((\w+) "(.+)"\)\)\n'
    drawJSON = r'"\1": {"name": "\2", "type": "\1"},'

    drawForm2 = r'\s*\((\w+) \((\w+) (.+)\)\)\n'
    drawJSON2 = r'"\1": {"name": "\2", "type": "\1"},'
    
    gateForm = r'\s*\(gate (\d+) \n'
    gateJSON = r'"gate": {"n": \1,'
    
    netForm = r'\s*\(net (.+)\)\n'
    netJSON = r'"net": "\1",'

    netForm2 = r'\s*\(net (.+)\n'
    netJSON2 = r'"net": {"name": "\1",'
    
    emptyStringForm = r'\n'
    emptyStringJSON = r'\\n'
    
    endStringForm = r'"\)\n'
    endStringJSON = r'",'
    
    tagValueOpenForm = r'\s*\((\w+) (\w+\d*|\d+\.\d+|[0-9]|\w+\d*-\w*\d*\w*)\n'
    tagValueOpenJSON = r'"\1":{"name":"\2",'
        
    compForm = r'\s*\(component \(reference (.+)\)\n'
    compJSON = r'"component": {"reference": "\1",'

    partForm = r'\s*\((part|package|originalReference|id) (.+)\)\n'
    partJSON = r'"\1": "\2",'

    gatenForm = r'\s*\(gate (\d+) \(name (.+)\)\)\n'
    gatenJSON = r'"gate": {"n": \1, "name": "\2"},'
    
    clForm = r'\s*\(layerNumber (\d+) \(status (\w+)\)\)\n'
    clJSON = r'"layerNumber": {"id": \1, "status": "\2"},'

    cPinForm = r'\s*\(comp (.+)\(pin (.+)\)\)\n'
    cPinJSON = r'"comp": {"name": "\1", "pin": "\2"},'
    
    iParForm = r'\s*\((intParam|distParam|nPin|heightR) (\d+)\s+(\d+)\)\n'
    iParJSON = r'"\1": {"first": \2, "second": \3},'

    sParForm = r'\s*\(stringParam (\d+) \(string "(\w*)"\)\)\n'
    sParJSON = r'"stringParam": {"id": \1, "string": "\2"},'

    sParForm2 = r'\s*\(stringParam (\d+) \(string (\w+.*)\)\)\n'
    sParJSON2 = r'"stringParam": {"id": \1, "string": "\2"},'
    
    sParForm3 = r'\s*\(stringParam (\d+) \(string (.+)\)\)\n'
    sParJSON3 = r'"stringParam": {"id": \1, "string": "\2"},'
    
    expForm = r'\s*\(default \(x ([-]?\d+)\) \(y ([-]?\d+)\)\)\n'
    expJSON = r'"default": {"x": \1, "y": \2},'

    expForm2 = r'\s*\(select (\w+) \(x ([-]?\d+)\) \(y ([-]?\d+)\)\)\n'
    expJSON2 = r'"\1": {"x": \2, "y": \3},'
    
    setForm = r'\s*\(set (.+)\n'
    setJSON = r'"set": {"name": "\1",'

    middleStringForm = r'\s*(\w+.*)\n'
    middleStringJSON = r'\1\\n'

    middleStringForm2 = r'(\(\d+\)\s+.+)\n'
    middleStringJSON2 = r'\1\\n'
    

    inFormats =  [cirForm, fspForm, fForm, psForm, psForm2, fromForm, fromForm2, conductForm, conductForm2, lconductForm, lconductForm2, oneTagForm, sDateForm, tagIntegerForm, tagDecimalForm, tagValueForm, ctagDateForm, ctagDateForm2, ctagValueForm, closeParForm, sLayerForm, sLayerForm2, sLayerForm3, layerForm, layerForm2, layerForm3, propSForm, propSForm2, propIForm, propFForm, ptForm, ptOpenForm, centerForm, tarcForm, padForm, holeForm, padSetForm, connectForm, padstackForm, drLayerForm, drLayerForm2, drLayerForm3, flayerForm, flayerForm2, fprintForm, fplayerForm, psgForm, pinForm, stringParsForm, stringForm, breakStringForm, stringForm2, breakStringForm2, pLayerForm, pLayerForm2, referForm, referForm2, systemForm, drawForm, drawForm2, gateForm, netForm, netForm2, emptyStringForm, endStringForm, tagValueOpenForm, compForm, partForm, gatenForm, clForm, cPinForm, iParForm, sParForm, sParForm2, sParForm3, expForm, expForm2, setForm, middleStringForm, middleStringForm2]
    outFormats = [cirJSON, fspJSON, fJSON, psJSON, psJSON2, fromJSON, fromJSON2, conductJSON, conductJSON2, lconductJSON, lconductJSON2, oneTagJSON, sDateJSON, tagIntegerJSON, tagDecimalJSON, tagValueJSON, ctagDateJSON, ctagDateJSON2, ctagValueJSON, closeParJSON, sLayerJSON, sLayerJSON2, sLayerJSON3, layerJSON, layerJSON2, layerJSON3, propSJSON, propSJSON2, propIJSON, propFJSON, ptJSON, ptOpenJSON, centerJSON, tarcJSON, padJSON, holeJSON, padSetJSON, connectJSON, padstackJSON, drLayerJSON, drLayerJSON2, drLayerJSON3, flayerJSON, flayerJSON2, fprintJSON, fplayerJSON, psgJSON, pinJSON, stringParsJSON, stringJSON, breakStringJSON, stringJSON2, breakStringJSON2, pLayerJSON, pLayerJSON2, referJSON, referJSON2, systemJSON, drawJSON, drawJSON2, gateJSON, netJSON, netJSON2, emptyStringJSON, endStringJSON, tagValueOpenJSON, compJSON, partJSON, gatenJSON, clJSON, cPinJSON, iParJSON, sParJSON, sParJSON2, sParJSON3, expJSON, expJSON2, setJSON, middleStringJSON, middleStringJSON2]
    
    jline=''
    for j, inFormat in enumerate(inFormats):
        if re.match(inFormat,line):
            try:
                jline = re.sub(inFormat,outFormats[j], line)
            except TypeError:
                print(inFormat)
                print(outFormats[j])
                sys.exit()
                
            break
    if jline=='':
        print('Unrecognized Zuken line:', i, line)
        sys.exit()
#    if (line.count('(') - line.count(')')) != (jline.count('{') -jline.count('}')):
#        if line.count('string')==0:
#            print('Unbalanced parentheses', line, jline)
#        jline=''
#        sys.exit()
        
    return jline

def array_on_duplicates(ordered_pairs):
    """Convert duplicate keys to arrays and store order on an extra key."""
#    https://www.semicolonworld.com/question/56998/python-json-parser-allow-duplicate-keys
#    https://stackoverflow.com/questions/14902299/json-loads-allows-duplicate-keys-in-a-dictionary-overwriting-the-first-value
        
    d = {}
    order = 0
    for k, v in ordered_pairs:
        if type(v) is dict:
            v['o'] = order
        if k in d:
            if type(d[k]) is list:
                d[k].append(v)
            else:
                d[k] = [d[k],v]
        else:
           d[k] = v
        order += 1
    return d


def ParseFootprintPCBFile(inFileName):
    """
    Converts Zuken parentheses file to JSON format
    :param: lines an array of strings with each line of the file
    """
    print('Parsing the file',inFileName)
    lines = open(inFileName,'r').readlines()
    
    # Sanity check
    if len(lines)<2:
        print('wrong lines',lines)
        sys.exit()
    jointLines = ''.join(lines)
    if jointLines.count('(')!=jointLines.count(')'):
        print('Unbalanced parentheses in input file')
        sys.exit()
        
        
    # TODO: This is way too complicated. Use instead: https://sexpdata.readthedocs.io/en/latest/
    # For example: sexpdata.loads('\n'.join(open('GR106771-A.ftf').readlines()))
    
    #    https://stackoverflow.com/questions/35155425/turning-nested-curly-braces-into-a-dict-python
    for i, line in enumerate(lines):
        lines[i] = ParseZukenLine(line,i)
#        print(i+1,line)
    
    # Add global braces
    lines[0]='{'+lines[0]
    lines[-1]+='}'
    
    #Strip trailing commas
    linestr = ''.join(lines)
    linestr = re.sub(',}','}',linestr)
    linestr = re.sub('{','{\n',linestr)
    linestr = re.sub('}','}\n',linestr)
    
    # Convert to JSON for nice indent:
    return String2JSON(linestr)
        

def String2JSON(linestr):
    """
    Converts a string to a JSON object
    :param: linestr a string in JSON format
    """
    try:
        final_dictionary = json.loads(linestr, object_pairs_hook=array_on_duplicates) 
    except json.JSONDecodeError as e:
        with open(logFileName,'w') as f:
            print('JSON format corrupt, check',logFileName)
            print(e)
            f.writelines(linestr)
            sys.exit()
    except RecursionError as e:
        with open(logFileName,'w') as f:
            print('JSON format too nested, check',logFileName)
            print(e)
#            sys.setrecursionlimit(10**6)#https://www.geeksforgeeks.org/python-handling-recursion-limit/
            f.writelines(linestr)
            sys.exit()  
            
    return final_dictionary


def ParseBoardLibraryFile(inFileName, name):
    """
    Parses a board or library file to JSONe
    :inFileName: the full path input filename
    :name: the name of the file without extension
    """
    isBoard = '.brd' in inFileName
        
    # Start parsing main board file .brd
    print('Parsing the file',inFileName)
    inFile = open(inFileName,'r').readlines()
    startPosition = 0
    result = '{'
    
    # Parse Header
    blockLines, jhead = ParseHeader(inFile, name, (name+' MM\n') if isBoard else "")
    startPosition += blockLines
    result += jhead
    
    if isBoard:
        # Parse board outline and convert to JSON
        blockLines, jhead, data = ParseOutline(inFile, startPosition, '.BOARD_OUTLINE ECAD\n', '.END_BOARD_OUTLINE\n', decimalFormat + '\n')
        startPosition += blockLines
        result += jhead
        
        # Parse other outline and convert to JSON
        blockLines, jhead, data = ParseOutline(inFile, startPosition, '.OTHER_OUTLINE ECAD\n', '.END_OTHER_OUTLINE\n', '(' + name + ') ' + decimalFormat + ' ' + placementFormat + '\n')
        startPosition += blockLines
        result += jhead
        
        # Parse route outline and convert to JSON
        blockLines, jhead, data = ParseOutline(inFile, startPosition, '.ROUTE_OUTLINE ECAD\n', '.END_ROUTE_OUTLINE\n', placementFormat + '\n')
        startPosition += blockLines
        result += jhead
        
        # Parse place outline and convert to JSON
        blockLines, jhead, data = ParseOutline(inFile, startPosition, '.PLACE_OUTLINE ECAD\n', '.END_PLACE_OUTLINE\n', placementFormat + ' ' + decimalFormat + '\n')
        startPosition += blockLines
        result += jhead
        
        # Iterate over route keepouts and convert to JSON:
        while inFile[startPosition]=='.ROUTE_KEEPOUT ECAD\n':
            blockLines, jhead, data = ParseOutline(inFile, startPosition, '.ROUTE_KEEPOUT ECAD\n', '.END_ROUTE_KEEPOUT\n', placementFormat + '\n')
            startPosition += blockLines
            result += jhead
                        
        # Iterate over route keepouts and convert to JSON:
        while inFile[startPosition]=='.PLACE_KEEPOUT ECAD\n':
            blockLines, jhead, data = ParseOutline(inFile, startPosition, '.PLACE_KEEPOUT ECAD\n', '.END_PLACE_KEEPOUT\n', placementFormat+' (\d+\.\d+)?\n')
            startPosition += blockLines
            result += jhead
        
        # Parse now drilled holes
        blockLines, jhead, data = ParseHoles(inFile, startPosition, '.DRILLED_HOLES\n', '.END_DRILLED_HOLES\n', '')
        startPosition += blockLines
        result += jhead
        
        # Parse module placement
        blockLines, jhead, data = ParseModulePlacement(inFile, startPosition)
        startPosition += blockLines
        result += jhead
    else:
        # Iterate over electrical symbols and convert to JSON:
        while startPosition < len(inFile) and inFile[startPosition]=='.ELECTRICAL\n':
            blockLines, jhead, data = ParseOutline(inFile, startPosition, '.ELECTRICAL\n', '.END_ELECTRICAL\n', symbolFormat+' '+symbolFormat+' (MM) '+decimalFormat+'\n')
            startPosition += blockLines
            result += jhead

    # Finish file parsing
    if startPosition != len(inFile):
        print('Error when parsing, did not finish at end of file')
        sys.exit()
        
    # Convert to JSON object
    result += '}'
    result = re.sub(',}','}',result)
    
    return String2JSON(result)

def WriteJSONFile(result, jsonName):
    """
    Write JSON object to file
    :param: result the JSON string
    :param: jsonName the output filename
    """
    if os.path.exists(jsonName):
        print('Overwriting JSON file', jsonName)
    else:
        print('Writing JSON file to', jsonName)
    with open(jsonName, "w") as f:
        json.dump(result, f, indent=2)

def main(cmd, args):
#if True:
#    cmd, args = (sys.argv[0], sys.argv[1:])
    """
    Main function
    :param: cmd the executed command in terminal
    :param: args the array of arguments
    """
    
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Convert Zuken CR-5000 ASCII PCB files to KiCAD')
    parser.add_argument('--path'  , dest='path'  , type=str  , required=True , help='Input directory where Zuken files are located')
    parser.add_argument('--fname' , dest='fname' , type=str  , required=True , help='Name of the PCB files (without extension)')
    parser.add_argument('--outdir', dest='outdir', type=str  , required=False, help='Output directory (optional)', default="")
    parser.add_argument('--name'  , dest='name'  , type=str  , required=False, help='ID of the PCB file, normally as file name (without extension)', default='')
    args = parser.parse_args(args)
    
    # Sanity check about input path
    if not os.path.exists(args.path):
        print('Non-existing input path',args.path)
        sys.exit()
    if not os.path.isdir(args.path):
        print('Not a directory in input path',args.path)
        sys.exit()
    
    # Sanity check about input files
    extensions = ["brd","ftf","pcf","pro"]
    for extension in extensions:
        filename = os.path.join(args.path,args.fname+"."+extension)
        if not os.path.exists(filename):
            print('Non-existing input path',filename)
            sys.exit()
        if not os.path.isdir(args.path):
            print('Not a file in ',filename)
            sys.exit()
    if args.name == '':
        args.name = args.fname
        
    # Sanity check about output directory
    if args.outdir == "":
        args.outdir = args.path
    if not os.path.exists(args.outdir):
        print('Non-existing output directory',args.outdir)
        sys.exit()
    
    # Start parsing the board file
    extension = extensions[0]
    result = ParseBoardLibraryFile(os.path.join(args.path,args.fname+"."+extension),args.name)
    WriteJSONFile(result,os.path.join(args.outdir, args.fname+"."+extension+".json"))
    
    # Start parsing footprint .ftf and PCB .pcf files and convert these to JSON
    for extension in extensions[1:3]:
        fileName = os.path.join(args.path,args.fname+"."+extension)
        result = ParseFootprintPCBFile(fileName)
        WriteJSONFile(result,os.path.join(args.outdir, args.fname+"."+extension+".json"))

    # Start parsing the library file
    extension = extensions[3]
    result = ParseBoardLibraryFile(os.path.join(args.path,args.fname+"."+extension),args.name)
    WriteJSONFile(result,os.path.join(args.outdir, args.fname+"."+extension+".json"))

    print('Finished parsing all files!')

    
if __name__ == '__main__':
    main(sys.argv[0], sys.argv[1:])
#    print('Main')
