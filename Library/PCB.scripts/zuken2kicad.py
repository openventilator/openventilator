#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#__author__ = Fernando Hueso-González'

"""
Script to convert Zuken CR-5000 JSON PCB files to KiCAD (previously converted with separate script from native format to JSON)
    Board file (.brd.json)
    Footprint ASCII file (.ftf.json)
    PC Board ASCII file (.pcf.json)
    Library file (.pro.json)
    http://www.mweda.com/cst/cst2013/mergedProjects/CST_PARTICLE_STUDIO/common_tools/import_zcr5000.htm
Developed in the context of the KiCAD OpenVentilator Group http://openventilator.gitlab.io/
"""

import argparse
import sys
import os
import re
from typing import NamedTuple
import math
import json
import numpy as np
#import tracemalloc
#tracemalloc.start(10)

import pcbnew
#sys.path.append('kicad-python')
#from kicad.pcbnew import Arc, Circle, Line, Polygon
#from kicad.util import Point2D
#from kicad.pcbnew import Via

sys.path.append('kicad-footprint-generator/')
import KicadModTree as KMT

import kifield
from kifield import sch

global scaleY
scaleY = 1 # Global scaling factor for Y coordinate

global zunit
zunit = 1 # Zuken conversion from mm to pt

global fusion
fusion = True # If pads are to be fused

layerDict = {
        'Placement-FC': 'F.CrtYd',
        'Placement-FS': 'B.CrtYd',
        'CompArea-A': 'F.CrtYd',
        'CompArea-B': 'B.CrtYd',
        'Symbol-A': 'F.SilkS',
        'Symbol-B': 'B.SilkS',
        'Symbol-A-1': 'F.Fab',
        'Symbol-B-1': 'B.Fab',
        'Assem_(MISC)': ['F.Cu', 'F.SilkS', 'F.Mask', 'F.Paste', 'F.Fab'],
        'Assem_(MISC)_M': ['B.Cu', 'B.SilkS', 'B.Mask', 'B.Paste', 'B.Fab'],
        'comp_equip': 'F.Fab',# before: Silk
        'solder_equip': 'B.Fab',
        'Component': 'F.Cu',
        'Solder': 'B.Cu',
        'Comp_resist': 'F.Mask',
        'Solder_resist': 'B.Mask',
        'Resist-A': 'F.Mask',
        'Resist-B': 'B.Mask',
        'masque_comp': 'F.Paste',
        'masque_sold': 'B.Paste',
        'MetalMask-A': 'F.Paste',
        'MetalMask-B': 'B.Paste',
        'Serigraphie-FC': 'F.SilkS',# before: Fab
        'Serigraphie-FS': 'B.SilkS',# before: Fab
        'interne2': 'In1.Cu',
        'interne3': 'In2.Cu',
        'interne4': 'In3.Cu',
        'interne5': 'In4.Cu',
        #comp_equip1, comp_equip2, format1, format1_MIR, etc.
}

layerDict2 = {
        'F.Mask' : pcbnew.F_Mask,
        'B.Mask' : pcbnew.B_Mask,
        'F.SilkS' : pcbnew.F_SilkS,
        'B.SilkS' : pcbnew.B_SilkS,
        'F.Paste' : pcbnew.F_Paste,
        'B.Paste' : pcbnew.B_Paste,
        'F.Fab': pcbnew.F_Fab,
        'B.Fab': pcbnew.B_Fab,
        'F.Cu': pcbnew.F_Cu,
        'B.Cu': pcbnew.B_Cu,
        'In1.Cu': pcbnew.In1_Cu,
        'In2.Cu': pcbnew.In2_Cu,
        'In3.Cu': pcbnew.In3_Cu,
        'In4.Cu': pcbnew.In4_Cu,
}

padMap = {
    'Component': 'F.Cu',
    'Comp_resist': 'F.Mask',
    'Solder': 'B.Cu',
    'Solder_resist': 'B.Mask',
    'all_inner': 'In*.Cu',
    'masque_comp': 'F.Paste',
    'masque_sold': 'B.Paste'
}

fontMap = {
    1: [1.06,0.75],
    9: [1.1,0.75],
    10: [1.25,0.8]
}

reconnectDict = {
}
innerReconnectDict = {
}

allfps = set()

def GenerateGerbers(board, fname, path, conductorLayers):
    """
    Generates PCB gerbers
    :param: board the pcb object
    :param: fname the board file name
    :param: path the output directory
    :param: conductorLayers number of conductor layers
    """
    print('Plotting Gerbers...')

    plot_controller = pcbnew.PLOT_CONTROLLER(board)
    plot_options = plot_controller.GetPlotOptions()

    # Set General Options:
    plot_options.SetOutputDirectory(path)
    plot_options.SetPlotFrameRef(False)
    plot_options.SetPlotValue(True)
    plot_options.SetPlotReference(True)
        
#    plot_options.SetPlotInvisibleText(True)
#    plot_options.SetPlotViaOnMaskLayer(True)
    plot_options.SetExcludeEdgeLayer(True)
    plot_options.SetPlotPadsOnSilkLayer(False)
    #plot_options.SetUseAuxOrigin(PLOT_USE_AUX_ORIGIN)
    plot_options.SetMirror(False)
    #plot_options.SetNegative(PLOT_NEGATIVE)
    plot_options.SetDrillMarksType(pcbnew.PCB_PLOT_PARAMS.NO_DRILL_SHAPE)
    plot_options.SetAutoScale(False)
    plot_options.SetScale(1)
    #plot_options.SetPlotMode(PLOT_MODE)
    plot_options.SetLineWidth(pcbnew.FromMM(0.15))
    plot_options.SetUseGerberX2format(True)

    # Set Gerber Options
    plot_options.SetUseGerberAttributes(True)
    #plot_options.SetUseGerberProtelExtensions(GERBER_USE_GERBER_PROTEL_EXTENSIONS)
    #plot_options.SetCreateGerberJobFile(GERBER_CREATE_GERBER_JOB_FILE)
    plot_options.SetSubtractMaskFromSilk(False)
    plot_options.SetIncludeGerberNetlistInfo(True)
#    popt.SetCreateGerberJobFile(gen_job_file)
#    popt.SetUseGerberProtelExtensions(False)

    plot_plan = [
        ( 'F.Cu', pcbnew.F_Cu, 'Front Copper' ),
        ( 'B.Cu', pcbnew.B_Cu, 'Back Copper' ),
        ( 'F.Mask', pcbnew.F_Mask, 'Front Mask' ),
        ( 'B.Mask', pcbnew.B_Mask, 'Back Mask' ),
        ( 'F.Paste', pcbnew.F_Paste, 'Front Paste' ),
        ( 'B.Paste', pcbnew.B_Paste, 'Back Paste' ),
        ( 'F.SilkS', pcbnew.F_SilkS, 'Front SilkScreen' ),
        ( 'B.SilkS', pcbnew.B_SilkS, 'Back SilkScreen' ),
        ( 'F.Fab', pcbnew.F_Fab, 'Front Fabrication' ),
        ( 'B.Fab', pcbnew.B_Fab, 'Back Fabrication' ),
        ( 'Edge.Cuts', pcbnew.Edge_Cuts, 'Edges' ),
    ]

    for i in range(conductorLayers-2):
        plot_plan.append(('In'+str(i+1)+'.Cu',pcbnew.F_Cu+i+1, 'Inner'+str(i+1)+' Copper'))

    for layer_info in plot_plan:
        plot_options.SetSkipPlotNPTH_Pads( layer_info[1] <= pcbnew.B_Cu )#https://gitlab.com/kicad/code/kicad/-/blob/master/demos/python_scripts_examples/gen_gerber_and_drill_files_board.py
        plot_controller.SetLayer(layer_info[1])
        plot_controller.OpenPlotfile(layer_info[0], pcbnew.PLOT_FORMAT_GERBER, layer_info[2])

        plot_controller.PlotLayer()

    plot_controller.ClosePlot()

    drill_writer = pcbnew.GERBER_WRITER(board)
#    drill_writer.SetFormat(METRIC, ZERO_FORMAT, INTEGER_DIGITS, MANTISSA_DIGITS)
#    drill_writer.SetOptions(MIRROR_Y_AXIS, HEADER, OFFSET, MERGE_PTH_NPTH)
    drill_writer.CreateDrillandMapFilesSet(path,True,False)


# https://stackoverflow.com/questions/35988/c-like-structures-in-python/31062667
class Point5(NamedTuple):
    """
    Class describing a Zuken Point with an index ID, a 2D point x and y in mm, and an angle a in degrees, and width in mm
    """
    ID: int
    x: float
    y: float
    a: float
    w: float = -1

class Hole6(NamedTuple):
    """
    Class describing a Zuken Hole with hole diameter in mm, x and y in mm, and 3 string tags (electrical, reference, type)
    """
    d: float
    x: float
    y: float
    e: str
    r: str
    t: str

class Pad6(NamedTuple):
    """
    Class describing a Zuken Pad with pad names, layer, connect / noconnect flags
    """
    psname: str
    lay: float
    connect: str
    noconnect: str
    thermal: str
    clearance: str


class Module(NamedTuple):
    """
    Class describing a Zuken Module with footprint name, value, symbol reference, x y z position in mm, and rotation angle in degrees, layer and fixation type
    """
    name: str
    value: str
    ref: str
    x: float
    y: float
    z: float
    a: float
    l: str
    t: str

class Track3(NamedTuple):
    """
    Class describing a Zuken track point with a 2D point x and y in pt, and a width in pt
    """
    x: int
    y: int
    width: int

class Point2(NamedTuple):
    """
    Class describing a Zuken outline point with a 2D point x and y in pt
    """
    x: int
    y: int

class Point4(NamedTuple):
    """
    Class describing a Zuken outline point with a 2D point x and y in pt, and an order id within sequence, and width in pt
    """
    x: int
    y: int
    o: int
    w: int = -1
    tarc: bool = False

class Box(NamedTuple):
    """
    Class describing a Zuken box with two corner 2D point x and y in pt, and an order id within sequence
    """
    b0: Point4
    b1: Point4
    isOblong: bool = False
    angle: float = 0

class Donut(NamedTuple):
    """
    Class describing a Zuken donut with center point x and y in pt, and an inner and outer radius in pt
    """
    pos: Point4
    inr: int
    outr: int
class Arc(NamedTuple):
    """
    Class describing a Zuken arc with center point and start point in pt, and the subtended angle in deg
    """
    cpt: Point4
    spt: Point4
    dalpha: float
    ept: Point4
    o: int

class Line(NamedTuple):
    """
    Class describing a Zuken line as an array of Point4
    """
    pts: list

class Rectangle(NamedTuple):
    """
    Class describing a Zuken rectangle in pt and deg
    """
    pos: Point4
    w: int
    h: int
    a: float
    ow: int
    fw: int
    fa: float

class Surface(NamedTuple):
    """
    Class describing a Zuken surface in pt and deg
    """
    ow: int
    fw: int
    fa: float
    outl: Line

class Circle(NamedTuple):
    """
    Class describing a Zuken circle with position and radius in pt
    """
    pos: Point4
    r: int

class Oval(NamedTuple):
    """
    Class describing a Zuken oval with position, width, height in pt and angle in deg
    """
    pos: Point4
    width: int
    height: int
    angle: float


class Thermal(NamedTuple):
    """
    Class describing a Zuken round or square thermal with position and other ids in pt
    """
    rou: bool
    pos: Point4
    tin: int
    tout: int
    n: int
    w: int
    a: int


class Pad(NamedTuple):
    """
    Class describing a full Zuken Pad with several subclasses
    """
    name: str
    box: Box
    lines: list
    rects: list
    circs: list
    therm: list
    oblon: list
    dons: list
    surfs: list


class PadStack(NamedTuple):
    """
    Class describing a Zuken Padstack
    """
    psname: str
    allPads: list
    pth: str
    drillRadius: int
    oval: Oval
    ovalSize: list


class Component(NamedTuple):
    """
    Class describing a Zuken Component with footprint name, symbol reference, x y position in mm, and rotation angle in degrees, layer and fixation type
    """
    reference: str
    part: str
    package: str
    footprint: str
    outOfBoard: bool
    position: Point4
    outposition: Point4
    side: str
    placed: bool
    lock: bool
    angle: float


def AddText(board, layer, t, locked=False):
    """
    Add a Zuken text field to the PCB
    :param: board the PCB object
    :param: layer the layer it is added to
    :param: t the Text object
    :param: locked bool if it is locked
    :param: shrinkFactor to scale the text
    """
    mlays = layer
    if not type(mlays) is list:
        mlays = [mlays]
    for mlay in mlays:
        txt = pcbnew.TEXTE_PCB(board)
        ModifyText(mlay, txt, t, locked, 0)
        board.Add(txt)


def ModifyText(layer, txt, t, locked=False, angleOffset = 0):
    """
    Modify an existing KiCad text based on t object
    :param: txt TEXTE_PCB from pcbnew object
    :param: layer the layer it is added to
    :param: t a class Text of Zuken
    :param: locked if locked in layer
    :param: angleOffset angle offset of module in deg
    """
    # TODO for the future check if this changes something https://gitlab.com/kicad/code/kicad/-/commit/9cf1e61d8a4180de28c727510d4dca6bbfc8167d https://gitlab.com/kicad/code/kicad/-/issues/4478#note_344736492
    txt.SetText(t.string)
    if t.size!=-1:
        if not t.font in fontMap.keys():
            print('Missing font',t.font,t.string)
            shrinkFactor = [1,1]
            sys.exit()
        else:
            shrinkFactor = fontMap[t.font]
        txt.SetTextSize(pcbnew.wxSizeMM(shrinkFactor[0]*t.size.x/zunit, shrinkFactor[1]*t.size.y/zunit))
    if t.thickness!=-1:
        txt.SetThickness(pcbnew.FromMM(t.thickness/zunit))
    txt.SetTextAngle(t.angle*10+angleOffset*10)
    flipMap = {'NONE': False, 'X': True} # around X axis
    if flipMap[t.flip]:
        txt.Flip(txt.GetCenter())
    txt.SetLayer(layer) # redundant with flip, must be after it!!
    if layer == pcbnew.B_SilkS:
        txt.SetMirrored(True)
    txt.SetPosition(pcbnew.wxPointMM(t.position.x/zunit, scaleY*t.position.y/zunit))# Has to be at the end because Flips and mirrors change stuff
    if t.justify != '':# has to be at the end also to avoid problems when flipping
        hjustifyMap = {'LO_L': pcbnew.GR_TEXT_HJUSTIFY_LEFT, 'LO_C': pcbnew.GR_TEXT_HJUSTIFY_CENTER, 'LO_R': pcbnew.GR_TEXT_HJUSTIFY_RIGHT, 'CE_L': pcbnew.GR_TEXT_HJUSTIFY_LEFT, 'CE_C': pcbnew.GR_TEXT_HJUSTIFY_CENTER, 'CE_R': pcbnew.GR_TEXT_HJUSTIFY_RIGHT}
        vjustifyMap = {'LO_L': pcbnew.GR_TEXT_VJUSTIFY_BOTTOM, 'LO_C': pcbnew.GR_TEXT_VJUSTIFY_BOTTOM, 'LO_R': pcbnew.GR_TEXT_VJUSTIFY_BOTTOM, 'CE_L': pcbnew.GR_TEXT_VJUSTIFY_CENTER, 'CE_C': pcbnew.GR_TEXT_VJUSTIFY_CENTER, 'CE_R': pcbnew.GR_TEXT_VJUSTIFY_CENTER}
        txt.SetHorizJustify(hjustifyMap[t.justify])
        txt.SetVertJustify(vjustifyMap[t.justify])
    txt.SetLocked(locked)


def AddDimension(board, layer, p0, p1, p2, p3):
    """
    Add a Zuken dimension field to the PCB
    :param: board the PCB object
    :param: layer the layer it is added to
    :param: p0 arrow start point
    :param: p1 arrow end point
    :param: p2 assist point at start point
    :param: p3 assist point at end point
    """
    dim = pcbnew.DIMENSION(board)
    dim.SetLayer(layer)
    dim.SetOrigin(pcbnew.wxPointMM(p0.x/zunit,scaleY*p0.y/zunit))
    dim.SetEnd(pcbnew.wxPointMM(p1.x/zunit,scaleY*p1.y/zunit))
    dim.m_featureLineGF = pcbnew.wxPointMM(p2.x/zunit,scaleY*p2.y/zunit)
    dim.m_featureLineDF = pcbnew.wxPointMM(p3.x/zunit,scaleY*p3.y/zunit)
    dim.SetText(' ')
    board.Add(dim)

def AddBox(board, layer, p0, p1, width):
    """
    Add a Zuken box field to the PCB
    :param: board the PCB object
    :param: layer the layer it is added to
    :param: p0 lower left corner of box in pt
    :param: p1 upper right corner of box in pt
    :param: width the line thickness
    """
    p00 = Track3(p0.x,p0.y,width)
    p01 = Track3(p0.x,p1.y,width)
    p11 = Track3(p1.x,p1.y,width)
    p10 = Track3(p1.x,p0.y,width)
    AddGraphicLines([p00,p01,p11,p10,p00],board,layer)


def AddHole(hole, board, viaWidth = 0.9144):
    """
    Add drilled hole to the board
    :param: a hole with XY coordinates in mm, diameter in mm and ids
    :param: board the pcb object
    :param: viaWidth the width of the via in mm
    """
    if hole.t == 'VIA':
        v = pcbnew.VIA(board)
        v.SetDrill(pcbnew.FromMM(hole.d))
        v.SetWidth(pcbnew.FromMM(viaWidth))
        v.SetPosition(pcbnew.wxPointMM(hole.x,scaleY*hole.y))
        board.Add(v)
#    else:
        #To be done by footprint
        # See https://forum.kicad.info/t/anti-pads-in-inner-layers/20308/5


def AddVia(points, board, layer, netcode, drill, width):
    """
    Add a via to the board
    :param: point a Point2 in pt at the via center
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    :param: netcode the net it belongs to
    :param: drill the drill diameter of the via in pt
    :param: width the width of the via in pt
    """
    v = pcbnew.VIA(board)
    v.SetDrill(pcbnew.FromMM(drill/zunit))
    v.SetWidth(pcbnew.FromMM(width/zunit))
    v.SetPosition(pcbnew.wxPointMM(points.x/zunit,scaleY*points.y/zunit))
    v.SetNetCode(netcode)
    board.Add(v)

    #For more than 2 conductor layers, to be done by footprint
    # See https://forum.kicad.info/t/anti-pads-in-inner-layers/20308/5


def AddGraphicHole(hole, board, layer):
    """
    Add drilled hole to the board
    :param: a hole with XY coordinates, diameter and ids
    :param: board the pcb object
    """
    p0 = Point5(0,hole.x,hole.y,0)
    p1 = Point5(0,hole.x+hole.d/2,hole.y,360)
    AddArc(p1, p0, board, layer)
#    else:
        #To be done by footprint

def AddHoles(data, board):
    """
    Add drilled holes to the board
    :param: data a set of holes with different coordinates, diameters and ids
    :param: board the pcb object
    """

    # Sanity check
    if len(data) < 1:
        print('Inconsistent data', data)
        sys.exit()

    # Split data type by index and get unique ids
#    ids = [p.ID for p in data]
#    uids = list(set(ids))

    # Loop over each segment ID
#    for uid in uids:
#        points = [d for d in data if d.ID == uid]
#        AddSegment(points, board, layer, isKeepout)
    for d in data:
        AddHole(d, board)


def AddGraphicHoles(data, board, layer):
    """
    Add drilled holes to the board
    :param: data a set of holes with different coordinates, diameters and ids
    :param: board the pcb object
    :param: layer the PCB layer to be added to
    """

    # Sanity check
    if len(data) < 1:
        print('Inconsistent data', data)
        sys.exit()

#        AddSegment(points, board, layer, isKeepout)
    for d in data:
        AddGraphicHole(Hole6(d['d'],d['x'],d['y'],d['e'],d['r'],d['t']), board, layer)


def AddModule(mod, board):
    """
    Adds an empty module to the board
    :param: mod a Module with all coordinates and ids
    :param: board the pcb object
    """
    m = pcbnew.MODULE(board)

    fpid = pcbnew.LIB_ID("", mod.name)# https://docs.kicad-pcb.org/doxygen-python/FootprintWizardBase_8py_source.html
    m.SetFPID(fpid)
    m.SetValue(mod.value)
    m.Value().SetVisible(False)
    m.SetReference(mod.ref)
    m.SetPosition(pcbnew.wxPointMM(mod.x,scaleY*mod.y))
    m.SetOrientationDegrees(scaleY*mod.a)
    m.SetIsPlaced(mod.t=='PLACED')
    m.SetLocked(mod.t=='FIXED')
    if mod.l == "BOTTOM":
        m.Flip(m.GetPosition())
    allfps.add(mod.name)
    board.Add(m)


def AddComponent(libPath, libName, comp, board, subname='', removeCopper={}, conductorLayers=0, layerMap = []):
    """
    Adds a full component to the board
    :param: libPath the footprint library path
    :param: libName the footprint library name
    :param: comp a Component with all coordinates and ids
    :param: board the pcb object
    :param: subname a suffix for footprint variations depending on removed copper
    :param: removeCopper layers from which to remove copper from, dict for each pin
    :param: conductorLayers number of copper layers
    :param: layermap Zuken to KiCad layer mapping
    """

    fpName = comp.footprint
    footprint = pcbnew.FootprintLoad(libPath, fpName + subname)
    #https://www.reddit.com/r/KiCad/comments/cfejdz/place_a_component_with_a_python_script/
    if footprint is None:
        if subname == '':
            print('footprint',fpName,'not found')
            sys.exit()
        else:
            if len(subname)>100:
                subname = '_'+hex(int(subname[1:],2))#https://stackoverflow.com/questions/16203874/convert-string-of-0s-and-1s-to-byte-in-python
            CreateComponentFootprintVariation(libPath, fpName, subname, removeCopper, conductorLayers, layerMap)
            footprint = pcbnew.FootprintLoad(libPath, fpName+subname)
    elif subname != '':
        if os.path.getctime(os.path.join(libPath,fpName+subname+'.kicad_mod')) < os.path.getctime(os.path.join(libPath,fpName+'.kicad_mod')):#https://stackoverflow.com/questions/12817041/check-if-the-file-is-newer-then-some-other-file
            # update it because master footprint has been changed
            if len(subname)>100:
                subname = '_'+hex(int(subname[1:],2))#https://stackoverflow.com/questions/16203874/convert-string-of-0s-and-1s-to-byte-in-python
            CreateComponentFootprintVariation(libPath, fpName, subname, removeCopper, conductorLayers, layerMap)
            footprint = pcbnew.FootprintLoad(libPath, fpName+subname)

    m = pcbnew.MODULE(footprint)
    fpid = pcbnew.LIB_ID(libName, fpName+subname)
    m.SetFPID(fpid)
    m.SetReference(comp.reference)
    if 'cam' in comp.reference:
        m.Reference().SetVisible(False)
    m.SetValue(comp.part)
    p = comp.outposition if comp.outOfBoard else comp.position
    m.SetPosition(pcbnew.wxPointMM(p.x/zunit,scaleY*p.y/zunit))
    angle = comp.angle*10
    if comp.side == 'B':
        angle += 180*10
        m.Flip(m.GetPosition())
#        m.SetLayer(layer) SetLayer is redundant, if done, always after Flip, otherwise flip will change the layer
    m.SetOrientation(angle)
    m.SetIsPlaced(comp.placed)
    m.SetLocked(comp.lock)
    board.Add(m)
    allfps.add(fpName+subname)
    return m


def AddViaFootprint(libPath, libName, fpName, subname, position, board, layer, netcode, conductorLayers, removeCopper, viaRef):
    """
    Add module with a footprint to the board
    :param: libPath the footprint library path
    :param: libName the footprint library nickname
    :param: fpName the footprint name
    :param: subName a suffix for footprint variations depending on removed Copper
    :param: position a Point2 in pt of the via position
    :param: board the pcb object
    :param: layer the PCB layer to be added to
    :param: netcode the net the via is connected to
    :param: conductorLayers number of copper layers
    :param: removeCopper layers from which to remove copper from
    :param: viaRef the number for this via reference, if < 1 omit.
    """
    footprint = pcbnew.FootprintLoad(libPath, fpName+subname)
    #https://www.reddit.com/r/KiCad/comments/cfejdz/place_a_component_with_a_python_script/
    if footprint is None:
        if subname == '':
            print('footprint',fpName,'not found')
            sys.exit()
        else:
            CreateViaFootprintVariation(libPath, fpName, subname, removeCopper, conductorLayers)
            footprint = pcbnew.FootprintLoad(libPath, fpName+subname)
    elif subname != '':
        if os.path.getctime(os.path.join(libPath,fpName+subname+'.kicad_mod')) < os.path.getctime(os.path.join(libPath,fpName+'.kicad_mod')):#https://stackoverflow.com/questions/12817041/check-if-the-file-is-newer-then-some-other-file
            # update it because master footprint has been changed
            CreateViaFootprintVariation(libPath, fpName, subname, removeCopper, conductorLayers)
            footprint = pcbnew.FootprintLoad(libPath, fpName+subname)

    m = pcbnew.MODULE(footprint)
    fpid = pcbnew.LIB_ID(libName, fpName+subname)
    m.SetFPID(fpid)
    if viaRef>0:
        m.SetReference('VI'+str(viaRef))
    m.SetPosition(pcbnew.wxPointMM(position.x/zunit,scaleY*position.y/zunit))
    m.SetLocked(True)
    m.SetLayer(layer)
    board.Add(m)
    allfps.add(fpName+subname)
    for p in m.Pads():
        if p.GetName()!='':
            # HAS to be after board.Add https://forum.kicad.info/t/issue-with-assigning-net-to-pad-from-a-footprint-python-scripting-solved/20343
            p.SetNetCode(netcode)#https://forum.kicad.info/t/copper-and-mask-in-different-layers/22717/10
    return m

def AddGraphicModule(mod, outline, board, layer):
    """
    Add graphic module outline to the board
    :param: mod a Module with all coordinates and ids
    :outline: set of Point5 defining closed outline
    :param: board the pcb object
    :param: layer the PCB layer to add the graphic lines to
    """
    flip = 'X' if (mod.l == "BOTTOM") else 'NONE'
    locked = (mod.l == "FIXED")
    if False:#disable, too much stuff
        AddText(board,layer,Text(mod.name,Point2(zunit*mod.x,zunit*(mod.y  )),Point2(0.75*zunit,0.75*zunit),-1,'', mod.a, flip), locked) # disable, too much stuff
        AddText(board,layer,Text(mod.ref ,Point2(zunit*mod.x,zunit*(mod.y+1)),Point2(0.75*zunit,0.75*zunit),-1,'', mod.a, flip), locked)
#        AddSegment(outline,board,layer,False) # Disable for now, footprint points had to be rotated/flipped (before translation in AddGraphicModules function)


def AddModules(data, board):
    """
    Add modules to the board
    :param: data a set of modules with different coordinates and ids
    :param: board the pcb object
    """

    # Sanity check
    if len(data) < 1:
        print('Inconsistent data', data)
        sys.exit()

    for m in data:
        AddModule(Module(m['name'],m['value'],m['ref'],m['x'],m['y'],m['z'],m['a'],m['l'],m['t']), board)

def AddGraphicModules(placement, outlines, board, layer):
    """
    Add module outlines to the board
    :param: placement a set of modules in JSON format with different coordinates and ids
    :param: outlines a set of library outlines in JSON format of different footprints
    :param: board the pcb object
    :param: layer
    """

    # Sanity check
    if len(placement) < 1:
        print('Inconsistent placement', placement)
        sys.exit()
    if len(outlines) < 1:
        print('Inconsistent outlines', outlines)
        sys.exit()
    for m in placement:
        if m['value']!=m['name']:
            print('Warning: mismatch module name - value',m)
    for o in outlines:
        if o['units']!='MM':
            print('wrong units',o)
            sys.exit()
#        if o['value']!=o['name']:
#            print('Warning: mismatch outline name - value',o)
#        if not 'outline_0' in o.keys():
#            print('Warning: empty outline',o)
        if 'outline_1' in o.keys():
            print('unsupported multioutline',o)
            sys.exit()

    # Create map of module name to index
    outlineMap = [o['name'] for o in outlines]

    for m in placement:
        mName = m['name']
        if not mName in outlineMap:
            # try substitutions
            inp =  ['-KEMET-','-KEMET-','-MURATA-']
            outp = ['--'     ,'-'      ,'-'       ]
            for k in range(len(inp)):
                newName = re.sub(inp[k],outp[k],mName)
                if newName in outlineMap:
                    mName = newName
                    break
            if not mName in outlineMap:
                print('Warning: not found name in outlineList', mName)
                print(outlineMap)
                sys.exit()
                mName = ''
        if mName!='':
            oidx = outlineMap.index(mName)
            outline = [Point5(0, p['x'] + m['x'], p['y'] + m['y'], p['a']) for p in outlines[oidx]['outline_0']] if 'outline_0' in outlines[oidx] else []
        else:
            outline = []
        AddGraphicModule(Module(m['name'],m['value'],m['ref'],m['x'],m['y'],m['z'],m['a'],m['l'],m['t']), outline, board, layer)

def AddCircularArea(p0, p1, board, layer, isKeepout, resolution = 0.1):
    """
    Add a circular closed zone to the board
    :param: p0 First Point5 defining the arc start
    :param: p1 Second Point5 defining the arc center
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    :param: isKeepout marks if it is a keepout area
    :param: resolution the step size to sample on the circle perimeter, in mm
    """

    #Calculate circle features
    radius = math.sqrt(math.pow(p0.x - p1.x,2) + math.pow(p0.y - p1.y,2))
    perimeter = 2*math.pi*radius
    dalpha = p1.a-p0.a
    subdivisions = int(perimeter/resolution*abs(dalpha)/360)
    subdivisions = max(subdivisions,3) # Avoid division by zero later on

    # Sample perimeter and add zone to board
    ze = pcbnew.ZONE_CONTAINER(board)
    ze.SetLayer(layer)
    bOutline = pcbnew.wxPoint_Vector()

    for i in range(subdivisions+1):
        alpha = (p0.a + i/subdivisions*dalpha)*math.pi/180
        bOutline.append(pcbnew.wxPointMM(p1.x+radius*math.cos(alpha),scaleY*(p1.y+radius*math.sin(alpha))))
    ze.AddPolygon(bOutline)
    board.Add(ze)


def AddRoundedRectangleArea(points, board, layer, isKeepout, resolution = 0.1):
    """
    Add a rounded rectangle closed zone to the board
    :param: points Set of nine Point5 defining the rounded rectangle
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    :param: isKeepout marks if it is a keepout area
    :param: resolution the step size to sample on the circle perimeter, in mm
    """
    p0, p1, p2, p3, p4, p5, p6, p7, p8 = points

    # Calculate arc centers
    p01 = Point5(p1.ID,p0.x,p1.y,p1.a)
    p23 = Point5(p3.ID,p3.x,p2.y,p3.a)
    p45 = Point5(p5.ID,p4.x,p5.y,p5.a)
    p67 = Point5(p7.ID,p7.x,p6.y,p7.a)

    #Calculate rounded features
    radius = abs(p1.y-p0.y)
    perimeter = 2*math.pi*radius
    dalpha = 90
    subdivisions = int(perimeter/resolution*dalpha/360)
    subdivisions = max(subdivisions,3) # Avoid division by zero later on

    # Start adding points to polygon vector
    bOutline = pcbnew.wxPoint_Vector()

    pc = p01
    for i in range(subdivisions+1):
        alpha = (-90 + i/subdivisions*dalpha)*math.pi/180
        bOutline.append(pcbnew.wxPointMM(pc.x+radius*math.cos(alpha),scaleY*(pc.y+radius*math.sin(alpha))))
    pn = p2
    bOutline.append(pcbnew.wxPointMM(pn.x,scaleY*pn.y))

    pc = p23
    for i in range(subdivisions+1):
        alpha = (0 + i/subdivisions*dalpha)*math.pi/180
        bOutline.append(pcbnew.wxPointMM(pc.x+radius*math.cos(alpha),scaleY*(pc.y+radius*math.sin(alpha))))
    pn = p4
    bOutline.append(pcbnew.wxPointMM(pn.x,scaleY*pn.y))

    pc = p45
    for i in range(subdivisions+1):
        alpha = (90 + i/subdivisions*dalpha)*math.pi/180
        bOutline.append(pcbnew.wxPointMM(pc.x+radius*math.cos(alpha),scaleY*(pc.y+radius*math.sin(alpha))))
    pn = p6
    bOutline.append(pcbnew.wxPointMM(pn.x,scaleY*pn.y))

    pc = p67
    for i in range(subdivisions+1):
        alpha = (180 + i/subdivisions*dalpha)*math.pi/180
        bOutline.append(pcbnew.wxPointMM(pc.x+radius*math.cos(alpha),scaleY*(pc.y+radius*math.sin(alpha))))
    pn = p8
    bOutline.append(pcbnew.wxPointMM(pn.x,scaleY*pn.y))

    # Add zone to board
    ze = pcbnew.ZONE_CONTAINER(board)
    ze.SetLayer(layer)
    ze.AddPolygon(bOutline)
    board.Add(ze)


def AddSlotArea(points, board, layer, isKeepout, resolution = 0.1):
    """
    Add a slot closed zone to the board
    :param: points Set of five Point5 defining the rounded rectangle
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    :param: isKeepout marks if it is a keepout area
    :param: resolution the step size to sample on the circle perimeter, in mm
    """
    p0, p1, p2, p3, p4 = points

    # Calculate arc centers
    p01 = Point5(p1.ID,(p0.x + p1.x)/2,(p0.y + p1.y)/2,p1.a)
    p23 = Point5(p3.ID,(p2.x + p3.x)/2,(p2.y + p3.y)/2,p3.a)

    #Calculate rounded features
    radius = math.sqrt(math.pow(p01.x-p0.x,2)+math.pow(p01.y-p0.y,2))
    perimeter = 2*math.pi*radius
    dalpha = 180
    subdivisions = int(perimeter/resolution*dalpha/360)
    subdivisions = max(subdivisions,2) # Avoid division by zero later on

    # Start adding points to polygon vector
    bOutline = pcbnew.wxPoint_Vector()

    pc = p01
    for i in range(subdivisions+1):
        alpha = (-180 + i/subdivisions*dalpha)*math.pi/180
        bOutline.append(pcbnew.wxPointMM(pc.x+radius*math.cos(alpha),scaleY*(pc.y+radius*math.sin(alpha))))
    pn = p2
    bOutline.append(pcbnew.wxPointMM(pn.x,scaleY*pn.y))

    pc = p23
    for i in range(subdivisions+1):
        alpha = (0 + i/subdivisions*dalpha)*math.pi/180
        bOutline.append(pcbnew.wxPointMM(pc.x+radius*math.cos(alpha),scaleY*(pc.y+radius*math.sin(alpha))))
    pn = p4
    bOutline.append(pcbnew.wxPointMM(pn.x,scaleY*pn.y))

    # Add zone to board
    ze = pcbnew.ZONE_CONTAINER(board)
    ze.SetLayer(layer)
    ze.AddPolygon(bOutline)
    board.Add(ze)


def AddArc(p0, p1, board, layer):
    """
    Add an arc or circle to the board
    :param: p0 First Point5 defining the arc start
    :param: p1 Second Point5 defining the arc center
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    """
    segm = pcbnew.DRAWSEGMENT(board)
    segm.SetLayer(layer)
    segm.SetArcStart(pcbnew.wxPointMM(p0.x,scaleY*p0.y))

    # Find center of arc https://math.stackexchange.com/questions/2308430/find-center-of-a-circle-using-2-points-and-arc

    segm.SetCenter  (pcbnew.wxPointMM(p1.x,scaleY*p1.y))

    if p0.w != -1 and p0.w == p1.w:
        segm.SetWidth(pcbnew.FromMM(p0.w))

    if abs(p1.a - p0.a) == 360:
        segm.SetShape(pcbnew.S_CIRCLE)
    else:
        segm.SetShape(pcbnew.S_ARC)
        segm.SetAngle( scaleY * (p1.a - p0.a) * 10 ) # from degrees to internal units

    board.Add(segm)


def AddArcObj(arc, board, layer):
    """
    Add an arc or circle to the board
    :param: arc an Arc object
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    """

    spt = Point5(0,arc.spt.x/zunit,arc.spt.y/zunit, 0, arc.spt.w/zunit)
    cpt = Point5(0,arc.cpt.x/zunit,arc.cpt.y/zunit, arc.dalpha, arc.cpt.w/zunit)

    AddArc(spt, cpt, board, layer)

def AddLine(p0, p1, board, layer):
    """
    Add a straight line to the board
    :param: p0 First Point5 defining the line start
    :param: p1 Second Point5 defining the line end
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    """
    segm = pcbnew.DRAWSEGMENT(board)
    segm.SetLayer(layer)
    segm.SetStart(pcbnew.wxPointMM(p0.x,scaleY*p0.y))
    segm.SetEnd(  pcbnew.wxPointMM(p1.x,scaleY*p1.y))
    if p0.w != -1:
        segm.SetWidth(pcbnew.FromMM(p0.w))

    board.Add(segm)


def AddPolygon(points, board, layer):
    """
    Add a polygon to the board
    :param: points a set of points in mm already in KiCad CS
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    """
    if len(points)<2:
        print('Invalid polygon',points)
        sys.exit()

    segm = pcbnew.DRAWSEGMENT(board)
    segm.SetLayer(layer)
    segm.SetShape(pcbnew.S_POLYGON)
    segm.SetPolyPoints(points)
    segm.SetWidth(0)

    board.Add(segm)

def AddGraphicZone(points, board, layer, isKeepout):
    """
    Add a graphic closed zone to the board
    :param: points array of Point5 defining the closed polygon
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    :param: isKeepout marks if it is a keepout area
    """
    if len(points)<2:
        print('Invalid graphic zone',points)
        sys.exit()

    ze = pcbnew.ZONE_CONTAINER(board)
    ze.SetLayer(layer)
    bOutline = pcbnew.wxPoint_Vector()
    for point in points:
        bOutline.append(pcbnew.wxPointMM(point.x,scaleY*point.y))
    ze.AddPolygon(bOutline)
    board.Add(ze)


def RemoveDuplicates(points):
    """
    Remove duplicate points in polygon, in case they are found
    :param: points an array of Point2 or Point4 building a polygon
    """
    # Sanity check
    if len(points) < 2:
        print('Warning: wrong polygon')
        return points
    uniquePoints = [points[0]]
    for pt in range(1,len(points)):
        point = points[pt]
        if point.x != uniquePoints[-1].x or point.y != uniquePoints[-1].y:
            uniquePoints.append(point)
    return uniquePoints


def AddZone(points, board, layers, netcode, isKeepout, blockPour, blockTracks, blockVias, clearance = -1, locked = True, minThick = -1, ow = -1):
    """
    Add a closed zone to the board
    :param: points a set of Point2 or Point4 in pt building a track
    :param: board the pcb object
    :param: layers the PCB layers to add it to
    :param: netcode the net it belongs to
    :param: isKeepout bool if zone is keepout
    :param: blockPour do not allow copper pour
    :param: blockTracks do not allow tracks
    :param: blockVias do not allow vias
    :param: clearance default clearance in mm
    :param: locked bool if zone is locked
    :param: minThickness of the pour
    :param: ow outline width
    """
    if len(points)<2:
        print('Invalid zone',points)
        sys.exit()
    if not type(layers) is list:
        layers = [layers]
    for layer in layers:
        ze = pcbnew.ZONE_CONTAINER(board)
        ze.SetLayer(layer)
        ze.SetNetCode(netcode)
        bOutline = pcbnew.wxPoint_Vector()
        for point in points:
            bOutline.append(pcbnew.wxPointMM(point.x/zunit,scaleY*point.y/zunit))
        ze.AddPolygon(bOutline)
        if isKeepout:
            ze.SetIsKeepout(isKeepout)
            ze.SetDoNotAllowCopperPour(blockPour)
            ze.SetDoNotAllowTracks(blockTracks)
            ze.SetDoNotAllowVias(blockVias)
        if clearance != -1:
            ze.SetZoneClearance(pcbnew.FromMM(clearance))
        if minThick != -1:
            ze.SetMinThickness(pcbnew.FromMM(minThick))
        ze.SetPadConnection(2)#pcbnew.ZONE_CONNECTION_FULL
        ze.SetLocked(locked)
        if ow != -1:# Remove half-width of the line
            ze.Outline().Inflate(pcbnew.FromMM(-ow/zunit/2),5)
        board.Add(ze)


def AddTrack(points, board, layer, netcode, locked = True):
    """
    Add a track to the board
    :param: points a set of Point3 in pt building a track
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    :param: netcode the net it belongs to
    :param: locked if track is locked
    """

    #Sanity check
    if len(points) < 2:
        print('Warning: Wrong track points',points)
        return
#    if not all(p.width == points[0].width for p in points):
#        print('Warning: wrong track points width',points) THIS IS OK, extra thermal spokes

    for i in range(1,len(points)):
        t = pcbnew.TRACK(board)
        p0 = points[i-1]
        p1 = points[i]
        t.SetStart(pcbnew.wxPointMM(p0.x/zunit, scaleY*p0.y/zunit))
        t.SetEnd  (pcbnew.wxPointMM(p1.x/zunit, scaleY*p1.y/zunit))
        t.SetWidth(pcbnew.FromMM(p0.width/zunit))
        t.SetLayer(layer)
        t.SetNetCode(netcode)
        t.SetLocked(locked)
        board.Add(t)


def AddTrackRaw(points, board, layer, netcode, locked = True):
    """
    Add a track to the board based on raw wxPoints in mm
    :param: points a set of wxPoint in mm building a track
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    :param: netcode the net it belongs to
    :param: locked if track is locked
    """

    #Sanity check
    if len(points) < 2:
        print('Warning: Wrong track points',points)
        return
#    if not all(p.width == points[0].width for p in points):
#        print('Warning: wrong track points width',points) THIS IS OK, extra thermal spokes

    for i in range(1,len(points)):
        t = pcbnew.TRACK(board)
        p0 = points[i-1]
        p1 = points[i]
        t.SetStart(pcbnew.wxPointMM(p0.x, p0.y))
        t.SetEnd  (pcbnew.wxPointMM(p1.x, p1.y))
        t.SetWidth(pcbnew.FromMM(p0.width))
        t.SetLayer(layer)
        t.SetNetCode(netcode)
        t.SetLocked(locked)
        board.Add(t)


def AddGraphicLines(points, board, layer):
    """
    Add a track to the board
    :param: points a set of Track3 in pt building several lines
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    """

    #Sanity check
    if len(points) < 2:
        print('Warning: Wrong points',points)
#    if not all(p.width == points[0].width for p in points):
#        print('Warning: wrong points width',points)

    mmPoints = []
    for p in points:
        mmPoints.append(Point5(0,p.x/zunit,p.y/zunit,0,p.width/zunit))

    AddSegment(mmPoints, board, layer, False)


def AddSegment(points, board, layer, isKeepout):
    """
    Add a segment with same unique id to the board
    :param: points a set of Points5 in mm with same id
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    :param: isKeepout marks if it is a keepout area
    """
    # Sanity-check
    if len(points) == 0:
        return

    # Check segment type by looking at angles
    if all(p.a == 0 for p in points):
        if points[0] == points[-1]:
            # Closed polyline
            if isKeepout:
                AddGraphicZone(points,board,layer,isKeepout) # Do not use this shortcut because it will create a Zone with a connected net. But the board outline should be just graphic lines
            else:
                for i in range(1,len(points)):
                    AddLine(points[i-1],points[i],board,layer)
        else:
            if isKeepout:
                print('Non closed area',points)
                sys.exit()
            # Set of lines
            for i in range(1,len(points)):
                AddLine(points[i-1],points[i],board,layer)
    else:
        if len(points)==2:
            # Circle or arc. First point is center, so it has to be on second place in function call
            p0, p1 = points

            if isKeepout:
                AddCircularArea(p1, p0, board, layer, isKeepout)
            else:
                AddArc(p1, p0, board, layer)

        elif len(points)==5:
            # Slot
            p0, p1, p2, p3, p4 = points

            # Sanity check:
            for i in range(1,len(points)):
                if abs(points[i-1].a - points[i].a) != 180:
                    print('Wrong slot',points)
                    sys.exit()
                if abs(points[i-1].a)%90!=0 or abs(points[i].a)%90!=0:
                    print('Wrong slot',points)
                    sys.exit()

            if not isKeepout:
                p01 = Point5(p1.ID,(p0.x + p1.x)/2,(p0.y + p1.y)/2,p1.a)
                AddArc (p0,p01,board,layer)
                AddLine(p1,p2,board,layer)
                p23 = Point5(p3.ID,(p2.x + p3.x)/2,(p2.y + p3.y)/2,p3.a)
                AddArc (p2,p23,board,layer)
                AddLine(p3,p4,board,layer)
            else:
                AddSlotArea(points, board, layer, isKeepout)

        elif len(points)==9:
            # Rounded rectangle
            p0, p1, p2, p3, p4, p5, p6, p7, p8 = points

            # Sanity check:
            for i in range(1,len(points)):
                if abs(points[i-1].a - points[i].a) != 90:
                    print('Wrong rounded rectangle',points)
                    sys.exit()
            for i in range(1,len(points)):
                if abs(points[i-1].a)%90!=0 or points[i].a%90!=0:
                    print('Wrong rounded rectangle',points)
                    sys.exit()

            if not isKeepout:
                p01 = Point5(p1.ID,p0.x,p1.y,p1.a)
                AddArc(p0,p01,board,layer)
                AddLine(p1,p2,board,layer)
                p23 = Point5(p3.ID,p3.x,p2.y,p3.a)
                AddArc(p2,p23,board,layer)
                AddLine(p3,p4,board,layer)
                p45 = Point5(p5.ID,p4.x,p5.y,p5.a)
                AddArc(p4,p45,board,layer)
                AddLine(p5,p6,board,layer)
                p67 = Point5(p7.ID,p7.x,p6.y,p7.a)
                AddArc(p6,p67,board,layer)
                AddLine(p7,p8,board,layer)
            else:
                AddRoundedRectangleArea(points, board, layer, isKeepout)

        else:
            print('Unexpected segment length',len(points))
            sys.exit()


def AddGraphicOutline(data, board, layer, isKeepout=False):
    """
    Add an outline expressed in JSON format to the board
    :param: data a set of JSON outlines with different lenghts and ids
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    :param: isKeepout marks if it is a keepout area
    """

    # If is list, call AddGraphicOutline recursively for each of them
    if type(data) is list:
        for d in data:
            AddGraphicOutline(d, board, layer, isKeepout)
        return

    # Sanity check
    if len(data) < 2:
        print('Inconsistent data', data)
        sys.exit()

    # Get only outlines
    outlines = {key: value for key, value in data.items() if "outline_" in key}

    # Loop over each segment ID
    for key, outline in outlines.items():
        oid = re.match(r'outline_(\d+)', key).group(1)
        points = [Point5(oid, d['x'], d['y'], d['a']) for d in outline]
        AddSegment(points, board, layer, isKeepout)


def AddOutline(data, board, layer, isKeepout, width):
    """
    Add an outline expressed as Point2 or Point5 objects to the board
    :param: data a set of Point 2 or Point5 objects in pt
    :param: board the pcb object
    :param: layer the PCB layer to add it to
    :param: isKeepout marks if it is a keepout area
    :param: the outline Width in pt
    """
    if type(data[0]) is Point2:
        points = [Point5(0,p.x/zunit,p.y/zunit,0,width/zunit) for p in data]
    else:
        points = [Point5(0,p.x/zunit,p.y/zunit,scaleY*p.a,width/zunit) for p in data]
    AddSegment(points, board, layer, isKeepout)


def ProcessBox(bstr):
    """
    Process a Zuken JSON box to a Box object
    :param: bstr the json object
    """
    # Sanity-check
    if list(bstr.keys()) != ['pt','o']:
        print('wrong box',bstr)
        sys.exit()
    box = bstr['pt']
    if len(box) != 2:
        print('wrong box length',box)
        sys.exit()
    for b in box:
        if list(b.keys()) != ['x','y','o']:
            print('wrong box keys',box)
            sys.exit()
    b0 = Point4(box[0]['x'],box[0]['y'],box[0]['o'])
    b1 = Point4(box[1]['x'],box[1]['y'],box[1]['o'])
    return Box(b0,b1)


def ProcessPoint(pstr):
    """
    Process a Zuken JSON point to a Point4 object
    :param: pt the json object
    """
    # Sanity-check
    if not 'pt' in pstr.keys():
        print('wrong pstr',pstr)
        sys.exit()
    pt = pstr['pt']
    if list(pt.keys()) != ['x','y','o']:
        print('wrong pt keys',pt)
        sys.exit()

    return Point4(pt['x'],pt['y'],pt['o'])


def ProcessRectangles(rects):
    """
    Process Zuken rectangles by converting those into an array of Rectangle objects
    :param: rects an array of Zuken rectangles in JSON, pt
    """

    if not type(rects) is list:
        rects = [rects]

    # Append consecutive rectangles to same array
    rpoints = []

    for rt in rects:
        if list(rt.keys())!=['pt','width','height','rAngle','outlineWidth','fillWidth','fillAngle','o']:
            print('wrong rect keys',rt.keys())
            sys.exit()
        pt = rt['pt']
        if list(pt.keys())!=['x','y','o']:
            print('wrong rect pt keys',pt.keys())
            sys.exit()

        rpoints.append(Rectangle(Point4(pt['x'],pt['y'],pt['o']),rt['width'],rt['height'],rt['rAngle'],rt['outlineWidth'],rt['fillWidth'],rt['fillAngle']))

    return rpoints


def ProcessSurfaces(surfs):
    """
    Process Zuken surfaces by converting those into an array of Surface objects
    :param: rects an array of Zuken rectangles in JSON, pt
    """

    if not type(surfs) is list:
        surfs = [surfs]

    # Append consecutive rectangles to same array
    spoints = []

    for su in surfs:
        if list(su.keys())!=['outlineWidth','fillWidth','fillAngle','vertex','o']:
            print('wrong surface keys',su.keys())
            sys.exit()
        if 'pt' in su['vertex'].keys():
            pt = su['vertex']['pt']
            if not type(pt) is list:
                pt = [pt]
            allp = []
            for p in pt:
                possibleKeys = ['x','y','tarc','o']
                for k in p.keys():
                    if not k in possibleKeys:
                        print('Wrong p keys',p)
                        sys.exit()
                if 'tarc' in p.keys():
                    allp.append(Point4(p['x'],p['y'],p['o'],p['tarc']['r'],True))
                else:
                    allp.append(Point4(p['x'],p['y'],p['o']))
            spoints.append(Surface(su['outlineWidth'],su['fillWidth'],su['fillAngle'],[Line(allp)]))
        if 'arc' in su['vertex'].keys():
            arcs = ProcessArcs(su['vertex']['arc'],-1)
            spoints.append(Surface(su['outlineWidth'],su['fillWidth'],su['fillAngle'],arcs))

    return spoints


def ProcessCircles(circs):
    """
    Process Zuken circles by converting those into an array of Circle objects
    :param: circs an array of Zuken circles in JSON, pt
    """

    if not type(circs) is list:
        circs = [circs]

    # Append consecutive rectangles to same array
    cpoints = []

    for ci in circs:
        if list(ci.keys())!=['r','pt','o']:
            print('wrong circle keys',ci.keys())
            sys.exit()
        pt = ci['pt']
        if list(pt.keys())!=['x','y','o']:
            print('wrong ci pt keys',pt.keys())
            sys.exit()

        cpoints.append(Circle(Point4(pt['x'],pt['y'],pt['o']),ci['r']))

    return cpoints


def ProcessOvals(ovals):
    """
    Process Zuken ovals by converting those into an array of Oval objects
    :param: ovals an array of Zuken ovals in JSON, pt
    """

    if not type(ovals) is list:
        ovals = [ovals]

    # Append consecutive rectangles to same array
    cpoints = []

    for ov in ovals:
        if list(ov.keys())!=['pt','width','height','ovalAngle','o']:
            print('wrong oval keys',ov.keys())
            sys.exit()
        pt = ov['pt']
        if list(pt.keys())!=['x','y','o']:
            print('wrong ov pt keys',pt.keys())
            sys.exit()

        cpoints.append(Oval(Point4(pt['x'],pt['y'],pt['o']),ov['width'],ov['height'],ov['ovalAngle']))

    return cpoints


def rotate_via_numpy(xy, radians):
    """Use numpy to build a rotation matrix and take the dot product. https://gist.github.com/LyleScott/e36e08bfb23b1f87af68c9051f985302"""
    x, y = xy
    c, s = np.cos(radians), np.sin(radians)
    j = np.matrix([[c, s], [-s, c]])
    m = np.dot(j, [x, y])

    return float(m.T[0]), float(m.T[1])

def ProcessOblongs(bstr):
    """
    Process a Zuken JSON oblong to a Box object
    :param: bstr the json object
    """
    # Sanity-check
    if not type(bstr) is list:
        bstr = [bstr]

    obs = []
    for bs in bstr:
        if list(bs.keys()) != ['width','pt','o']:
            print('wrong oblong',bs)
            sys.exit()
        box = bs['pt']
        if len(box) != 2:
            print('wrong oblong length',box)
            sys.exit()
        for b in box:
            if list(b.keys()) != ['x','y','o']:
                print('wrong oblong keys',box)
                sys.exit()
        b0 = Point4(box[0]['x'],box[0]['y'],box[0]['o'],bs['width'])
        b1 = Point4(box[1]['x'],box[1]['y'],box[1]['o'],bs['width'])

        # Sanity checks
        angle = 0
        if b0.y == b1.y:
            ob0 = Point4(min(b0.x,b1.x)-b0.w/2,b0.y,b0.o,b0.w)
            ob1 = Point4(max(b0.x,b1.x)+b1.w/2,b1.y,b1.o,b1.w)
        elif b0.x == b1.x:
            ob0 = Point4(b0.x,min(b0.y,b1.y)-b0.w/2,b0.o,b0.w)
            ob1 = Point4(b1.x,max(b0.y,b1.y)+b1.w/2,b1.o,b1.w)
        else:
            angle = math.atan2(b1.y-b0.y,b1.x-b0.x)
            r0 = rotate_via_numpy([b0.x,b0.y],angle)
            r1 = rotate_via_numpy([b1.x,b1.y],angle)
            if abs(r0[1] - r1[1]) > 1e-9:
                print('Rotation failed',b0,b1,r0,r1,angle*180/math.pi)
                sys.exit()
            ob0 = Point4(min(r0[0],r1[0])-b0.w/2,r0[1],b0.o,b0.w)
            ob1 = Point4(max(r0[0],r1[0])+b1.w/2,r1[1],b1.o,b1.w)
            angle *= 180/math.pi

        obs.append(Box(ob0,ob1,True,angle))

    return obs


def ProcessDonuts(bstr):
    """
    Process a Zuken JSON donut to a Donut object
    :param: bstr the json object
    """
    # Sanity-check
    if not type(bstr) is list:
        bstr = [bstr]

    don = []
    for bs in bstr:
        if list(bs.keys()) != ['out','in','pt','o']:
            print('wrong donut',bs)
            sys.exit()
        pt = bs['pt']
        pos = Point4(pt['x'],pt['y'],pt['o'])
        inr = bs['in']
        outr = bs['out']
        don.append(Donut(pos,inr,outr))

    return don

def ProcessThermals(isRound,ths):
    """
    Process Zuken round or square thermals by converting those into an array of RoundThermal objects
    :param: isRound
    :param: rths an array of Zuken round or square thermals in JSON, pt
    """

    if not type(ths) is list:
        rths = [ths]

    # Append consecutive rectangles to same array
    tpoints = []

    for th in rths:
        if list(th.keys())!=['out','in','pt','nBridge','bridgeWidth','bridgeAngle','o']:
            print('wrong thermal keys',th.keys())
            sys.exit()
        pt = th['pt']
        if list(pt.keys())!=['x','y','o']:
            print('wrong th pt keys',pt.keys())
            sys.exit()

        tpoints.append(Thermal(isRound,Point4(pt['x'],pt['y'],pt['o']),th['in'],th['out'],th['nBridge'],th['bridgeWidth'],th['bridgeAngle']))

    return tpoints


def ProcessLines(lines):
    """
    Process Zuken lines by converting those into a Line object
    :param: lines an array of Zuken lines
    """

    # Append consecutive points to same line
    points = []

    if not type(lines) is list:
        lines = [lines]

#    # Sanity-check
#    if len(lines) < 2:
#        print('wrong line',lines)
#        sys.exit()

    for pt in lines:
        possibleKeys = ['x','y','tarc','width','o']
        for k in pt.keys():
            if not k in possibleKeys:
                print('Wrong pt keys',pt)
                sys.exit()
        if 'tarc' in pt.keys():
            points.append(Point4(pt['x'],pt['y'],pt['o'],pt['tarc']['r'],True))
        else:
            points.append(Point4(pt['x'],pt['y'],pt['o'],pt['width'] if 'width' in pt.keys() else -1))

    return Line(points)


def DisentangleArcsLines(points, fillBetweenArcs = True, closeOutline = False):
    """
    Disentangle a heterogeneous set of lines and arc objects from a continuous line by adding points into the start and end of arcs
    :param: points set of points and arc objects
    :param: fillBetweenArcs also add line if consecutive arcs do not connect
    :param: closeOutline make outline close
    """
    arcs = [point for point in points if type(point) is Arc]

    lines = []
    currLine = None
    i = 0
    while i < len(points):
        if type(points[i]) is Point4:
            if not currLine is None:
                print('wrong currline')
                sys.exit()
            currLine = []
            if i > 0:
                if type(points[i-1]) is Arc:
                    # add startpoint to current line
                    if points[i].x != points[i-1].ept.x or points[i].y != points[i-1].ept.y:
                        currLine.append(Point4(points[i-1].ept.x,points[i-1].ept.y,points[i].o,points[i].w))
            currLine.append(points[i])
            j = i+1
            while j < len(points):
                if type(points[j]) is Arc:
                    # add endpoint to current line
                    if points[j].spt.x != points[j-1].x or points[j].spt.y != points[j-1].y:
                        currLine.append(Point4(points[j].spt.x,points[j].spt.y, points[j-1].o,points[j-1].w))
                    break
                else:
                    currLine.append(points[j])
                    j+=1
            lines.append(Line(currLine))
            currLine = None
            i = j
        else:
            j = i + 1
            if fillBetweenArcs:
                while j < len(points) and type(points[j]) is Arc:
                    if points[j-1].ept.x != points[j].spt.x or points[j-1].ept.y != points[j].spt.y:
                        lines.append(Line([points[j-1].ept,points[j].spt]))
                    j+=1
            i = j

    if closeOutline:
        firstPoint = points[0] if type(points[0]) is Point4 else points[0].spt
        lastPoint = points[-1] if type(points[-1]) is Point4 else points[-1].ept
        if firstPoint.x != lastPoint.x or firstPoint.y != lastPoint.y:
            lines.append(Line([lastPoint,firstPoint]))

    return arcs, lines

def ProcessArcs(arcs, resolution = 0.1, trace = ''):
    """
    Process Zuken arcs by sampling those into a polygon of Point4
    :param: arcs an array of Zuken arcs
    :param: resolution the sampling resolution along the arc in mm. If negative, just center, startpoint, radius and dalpha are returned
    :param: trace some debug message
    """

    # Convert mm resolution to Zuken units
    if resolution!= -1:
        resolution*=zunit

#    # Sanity check array
#    if len(arcs)<2:
#        print('Bad arc',arcs)
#        sys.exit()

    # Append consecutive arcs to same profile
    points = []

    # Loop over arcs
    if not type(arcs) is list:
        arcs = [arcs]

    for arc in arcs:
        if list(arc.keys())!= ['name','r','begin','center','end','o']:
            print('Wrong arc',arc.keys())
            sys.exit()

        # Sanity check
        radius = arc['r']
        if radius <= 0:
            print('Wrong radius',radius)
            sys.exit()
        direction = arc['name']
        if direction == 'CCW':
            dsign = 1
        elif direction == 'CW':
            dsign = -1
        else:
            print('Wrong direction',direction)
            sys.exit()

        if 'width' in arc[ 'begin']['pt'].keys():
            width = arc[ 'begin']['pt']['width']
            if width != arc[ 'end']['pt']['width']:
                print('wrong width',arc)
                sys.exit()
        else:
            width = -1

        spt =    Point4(arc[ 'begin']['pt']['x'],arc['begin' ]['pt']['y'],arc['o'],width)
        center = Point4(arc['center']['x']      ,arc['center']['y']      ,arc['o'],   -1)
        ept =    Point4(arc[   'end']['pt']['x'],arc['end'   ]['pt']['y'],arc['o'],width)

        if radius == 1796153472: # Either a flag or a bug in gr106772-A, convert to line
            points.append(spt)
            points.append(ept)
            continue

        if spt == ept:
            # Add a circle. Appears in openShape
            if len(arcs) > 1:
                print('Warning, this could lead to a self-intersecting polygon')
            if center.x== 0 and center.y == 0:
                print('Wrong circle',center,spt,ept)
                sys.exit()

            int32Limit = 2147483648/2 # apparently used as a flag divided by two

            if abs(center.x)-abs(int32Limit)>1 and abs(center.y) - abs(int32Limit) > 1:
                print('Missing flag',center)
                sys.exit()

            if center.x == 0 or center.y == 0:
                shiftX = int(np.sign(center.x))*radius
                shiftY = int(np.sign(center.y))*radius
            else:
                norm = math.sqrt(math.pow(center.x,2)+math.pow(center.y,2))
                ux = center.x/norm
                uy = center.y/norm
                # It might be a rounding error:
                slope = abs(uy/ux)
#                print('slope',trace,slope,spt,center,radius)
                if slope < 1e-5:
                    ux = int(np.sign(center.x))
                    uy = 0
                elif slope > 1e-5:
                    ux = 0
                    uy = int(np.sign(uy))
                shiftX = ux*radius
                shiftY = uy*radius
                # Alternative way would be to use just the flag as shift, and make the other zero. But we leave it in case sometimes both values are significant

            cpt = Point4(spt.x + shiftX, spt.y + shiftY, spt.o, spt.w)
            start_angle = 0
            dalpha = 2*math.pi

            if cpt == ept or cpt == spt:
                print('Wrong arc',cpt,spt,ept)
                sys.exit()

            if resolution == -1:
                points.append(Arc(cpt, spt, dalpha/math.pi*180,ept,arc['o']))
            else:
                arclength = radius*abs(dalpha)
                subdivisions = int(arclength/resolution)
                subdivisions = max(subdivisions,3) # Avoid division by zero later on

                # Sample points on arc
                arcpoints = []
        #        print('Starting arc with radius',radius,'dalpha',dalpha*180/math.pi,'subdivisions',subdivisions)
                for i in range(0,subdivisions+1):
                    alpha = (start_angle + i/subdivisions*dalpha)
                    arcpoints.append(Point4(cpt.x+radius*math.cos(alpha), cpt.y+radius*math.sin(alpha), arc['o'],width))
                points += arcpoints
        else:
            # https://math.stackexchange.com/questions/1781438/finding-the-center-of-a-circle-given-two-points-and-a-radius-algebraically
            # https://stackoverflow.com/questions/57769249/how-to-calculate-start-end-angle-of-an-arc-by-given-2-points

            # Calculate the two possible centers of this arc
            mpt = Point4((ept.x + spt.x)/2,(ept.y + spt.y)/2,spt.o,spt.w)
            hpt = Point4((ept.x - spt.x)/2,(ept.y - spt.y)/2,spt.o,spt.w)
            a = math.sqrt(math.pow(hpt.x,2)+math.pow(hpt.y,2))
            if a > radius:
                if abs(a-radius)<1:
                    a = radius # was a rounding error
                else:
                    print('Wrong a,radius',a,radius,arc)
                    continue
            b = math.sqrt(math.pow(radius,2)-math.pow(a,2))
            cpt1 = Point4(mpt.x + b/a*hpt.y, mpt.y - b/a*hpt.x,spt.o,spt.w)
            cpt2 = Point4(mpt.x - b/a*hpt.y, mpt.y + b/a*hpt.x,spt.o,spt.w)

            if cpt1 == cpt2:
                cpt = cpt1
            else:
                # Choose which one is the center based on the sign of 'center' point
                sign1 = ((ept.x - spt.x)*(cpt1.y - spt.y) - (ept.y - spt.y)*(cpt1.x - spt.x))
                sign2 = ((ept.x - spt.x)*(cpt2.y - spt.y) - (ept.y - spt.y)*(cpt2.x - spt.x))
                signc = ((ept.x - spt.x)*(center.y - spt.y) - (ept.y - spt.y)*(center.x - spt.x))

                # https://stackoverflow.com/questions/1560492/how-to-tell-whether-a-point-is-to-the-right-or-left-side-of-a-line
                if np.sign(sign1) == np.sign(sign2) or np.sign(sign1)==0 or np.sign(sign2)==0 or np.sign(signc)==0:
                    print('Wrong signs', sign1, sign2, signc, spt, ept, radius, center, cpt1, cpt2)
                    sys.exit()
                if np.sign(signc) == np.sign(sign1):
                    cpt = cpt1
                else:
                    cpt = cpt2

            if cpt == ept or cpt == spt:
                print('Wrong arc',cpt,spt,ept)
                sys.exit()


            # Calculate start and end angles
            start_angle = math.atan2(spt.y-cpt.y, spt.x-cpt.x) % (2*math.pi)
            stop_angle  = math.atan2(ept.y-cpt.y, ept.x-cpt.x) % (2*math.pi)
            dalpha = stop_angle - start_angle

            # Sanity-check
            if dalpha == 0:
                print('Wrong zero dalpha')
                sys.exit()

            # Correct dalpha
            if np.sign(dalpha) != dsign:
                dalpha = 2*math.pi - abs(dalpha) # Change circle section
                dalpha *= dsign

            if resolution == -1:
                points.append(Arc(cpt, spt, dalpha/math.pi*180,ept,arc['o']))
#                if spt.x == 16070340 and spt.y == 11267265:
#                    print(arc,cpt,spt,ept,dalpha/math.pi*180)
#                    sys.exit()
            else:
                arclength = radius*abs(dalpha)
                subdivisions = int(arclength/resolution)
                subdivisions = max(subdivisions,3) # Avoid division by zero later on

                # Sample points on arc
                arcpoints = []
                arcpoints.append(spt)
        #        print('Starting arc with radius',radius,'dalpha',dalpha*180/math.pi,'subdivisions',subdivisions)
                for i in range(1,subdivisions):
                    alpha = (start_angle + i/subdivisions*dalpha)
                    arcpoints.append(Point4(cpt.x+radius*math.cos(alpha), cpt.y+radius*math.sin(alpha), arc['o'], width))
                arcpoints.append(ept)
                points += arcpoints

    return points


def AddPadstackToModel(footprint_name,pinName,padDict,offset,angle,padstack,km,customSurfPad=[]):
    """
    Adds a padstack to an open Kicad Mod Tree
    :param: footprint_name the name of the footprint
    :param: pinName the pin name
    :param: padDict a dict of full pads
    :param: offset 2D point of location
    :param: angle rotation angle
    :param: padstack the padstack object
    :param: km the KMT tree object
    :param: customSurfPad an extra surface to anchor to this pad
    """
    # Sanity-check no duplicates
    laynames = [p.lay for p in padstack.allPads]
    if len(laynames) != len(set(laynames)):
        print('Repeated layers')
        sys.exit()
    for lay in laynames:
        if not lay in padMap.keys():
            print('Unexpected pad layer',lay)
            sys.exit()

    # Verify no-connect symmetry
    noconns = [p.lay for p in padstack.allPads if p.noconnect != '']
    if len(noconns)>0:
        if any(lay != 'Component' for lay in noconns):
            if lay != 'masque_sold':
                print('Unsupported reconnect for',lay)
#            sys.exit()

    if not fusion:
        for pad in padstack.allPads:
            klay = [padMap[pad.lay]]
            AddPadToModel(footprint_name, pinName, padDict, offset, angle, pad, klay, km, KMT.Pad.TYPE_SMT, 0, 0, 0, 0, 0, customSurfPad)
    else:
        klay = []
        for pad in padstack.allPads:
            klay.append(padMap[pad.lay])

        if padstack.allPads == []:
            return padstack
        psd = {}
        allPads = padstack.allPads
        for p in allPads:
            psd[p.lay] = p
        if not 'Component' in psd.keys() and not 'Solder' in psd.keys():
            print('missing component/solder',allPads)
            sys.exit()
        possibleKeys = ['Component','Solder', 'Comp_resist','Solder_resist','masque_comp', 'masque_sold']
        for k in psd.keys():
            if not k in possibleKeys:
                print('Unexpected psd key',psd.keys())
                sys.exit()

        padFp = psd['Component'].connect if 'Component' in psd.keys() else psd['Solder'].connect if 'Solder' in psd.keys() else ''
        smFp = psd['Comp_resist'].connect if 'Comp_resist' in psd.keys() else psd['Solder_resist'].connect if 'Solder_resist' in psd.keys() else ''
        clFp = psd['Component'].clearance if 'Component' in psd.keys() else ''
        thFp = psd['Component'].thermal if 'Component' in psd.keys() else ''
        if clFp == '' and 'all_inner' in psd.keys():
            clFp = psd['all_inner'].clearance
        if thFp == '' and 'all_inner' in psd.keys():
            thFp = psd['all_inner'].thermal

        lnames = [p.lay for p in allPads]
#        psname = allPads[0].psname

        pad = allPads[lnames.index('Component') if 'Component' in lnames else lnames.index('Solder')]
        pas = GetPadSizes(padDict[padFp])
        if smFp != '':
            sms = GetPadSizes(padDict[ smFp])

            diffs = []
            if len(sms) != len(pas):
                print('Bad sms pas sizes',sms,pas)
                sys.exit()
            for i in range(len(pas)):
                if len(pas[i]) == 1 and len(sms[i])==1:
                    diff = max(sms[i][0][0]-pas[i][0][0],sms[i][0][1]-pas[i][0][1])
                    if diff < 0:
                        print('wrong diff',diff)
                        sys.exit()
                    if diff > 0:
                        diffs.append(diff)
            smm = min(diffs)/zunit/2 if len(diffs)>0 else 0
        else:
            smm = 0

        AddPadToModel(footprint_name, pinName, padDict, offset, angle, pad, klay, km, KMT.Pad.TYPE_SMT, 0, smm, 0,0,0, customSurfPad)


    return padstack

def GetPadSignature(pad):
    """
    Gets the pad internal class shape
    :param: pad the pad object
    """
    return [len(pad.circs), len(pad.therm), len(pad.oblon), len(pad.rects), len(pad.dons), len(pad.surfs), len(pad.lines)]

def GetPadSizes(pad):
    """
    Calculates pad sizes of an SM pad
    :param: pad the Pad class
    """
    sizecircs = []
    for i, circle in enumerate(pad.circs):
        diam = circle.r * 2
        sizecircs.append([diam, diam])
    sizetherm = []
    for therm in pad.therm:
        if therm.rou:
            diam = therm.tin
            sizetherm.append([diam, diam])
        else:
            diam = therm.tin
            sizetherm.append([diam, diam])
    sizeoblon = []
    for obl in pad.oblon:
    #        print('Warning: skipping oblong shape for now',footprint_name)
        b0 = obl.b0
        b1 = obl.b1
        leftx=b0.x
        rightx=b1.x
        lefty=b0.y
        righty=b1.y
        size=[abs(rightx-leftx) if rightx != leftx else b0.w,abs(righty-lefty) if righty != lefty else b0.w]
        sizeoblon.append(size)
    sizerects = []
    for rect in pad.rects:
        sizerects.append([rect.w,rect.h])
    sizedons = []
    for don in pad.dons:
        diam = don.outr*2
        sizedons.append([diam, diam])
    sizesurfs = []
    if all(type(surf.outl[0]) is Arc for surf in pad.surfs) or all(type(surf.outl[0]) is Line for surf in pad.surfs):
        for i, surf in enumerate(pad.surfs):
            if len(surf.outl) == 0:
                print('empty size outl',surf)
                sys.exit()
            if type(surf.outl[0]) is Arc:
                if len(surf.outl) != 1:
                    print('Multiarc pad  size not yet implemented') #            for pt in surf.outl:
                    print(surf.outl)
                    sys.exit()
                pt = surf.outl[0]
                spt = pt.spt
                cpt = pt.cpt
                radius = math.sqrt(math.pow(spt.x - cpt.x, 2) + math.pow(spt.y - cpt.y,2))
                sizesurfs.append([radius*2,radius*2])
            elif type(surf.outl[0]) is Line:
                for pt in surf.outl:
                    for pp in pt:
                        if not all(p.w == pp[0].w for p in pp):
                            print('Warning: wrong size spoints width',pt,pad.name)

                        if len(pp)==4 and PolygonToBox(pp):
                            box = PolygonToBox(pp)
                            b0 = box.b0
                            b1 = box.b1
                            p0x = b0.x
                            p1x = b1.x
                            p0y = b0.y
                            p1y = b1.y
                            w = abs(p1x - p0x)
                            h = abs(p1y - p0y)
                            sizesurfs.append([w,h])
                        elif len(pp)==4:
                            left = pp[1]
                            right = pp[2]
                            if left.x == -right.x and left.y == -right.y:
                                w = abs(right.x - left.x)
                                h = abs(right.y - left.y)
                                sizesurfs.append([w,h])
                            else:
                                #Rombo
                                p0 = pp[0]
                                p1 = pp[1]
                                p2 = pp[2]
                                p3 = pp[3]
                                if p0.x == 0 and p2.x == 0 and p0.y == -p2.y and p1.y == 0 and p3.y == 0 and p1.x == -p3.x:
                                    w = abs(p3.x - p1.x)
                                    h = abs(p2.y - p0.y)
                                    sizesurfs.append([w,h])
                                else:
                                    print('Warning: Bad triangle',pad.name)
                                    print(pp)
#                                    sys.exit()
                                    sizesurfs.append([0,0])
                        elif len(pp)==3:
                            up = pp[0]
                            ce = pp[1]
                            bo = pp[2]
                            if up.x == bo.x and up.y == -bo.y and ce.y == 0:# Triangle
                                w = abs(up.x - ce.x)
                                h = abs(up.y - bo.y)
                                sizesurfs.append([w,h])
                            else:
                                print('Warning: Bad triangle',pad.name)
                                print(pp)
                                sys.exit()
                                sizesurfs.append([0,0])
                        else:
                            minx = min([p.x for p in pp])
                            maxx = max([p.x for p in pp])
                            miny = min([p.y for p in pp])
                            maxy = max([p.y for p in pp])
                            w = maxx - minx
                            h = maxy - miny
                            sizesurfs.append([w,h])
            else:
#                print('Surf arcs size not yet implemented',pad.name)
                sizesurfs.append([0,0])
    sizelines = []
    for i, line in enumerate(pad.lines):
        if len(line)!=1:
#            print('Multiline not supported yet',line)
            sizelines.append([0,0])
            continue
        if type(line[0]) is Arc:
#            print('Line arcs size not yet implemented',pad.name)
            sizelines.append([0,0])
            continue
        else:
            if type(line) is list:
#                print('Multi Line size not yet implemented',pad.name)
                sizelines.append([0,0])
                continue
            else:
                pts = line.pts
                if len(pts)==2 and (pts[0].x == pts[1].x or pts[0].y == pts[1].y) and pts[0].w == pts[1].w and pts[0].w > 0:
                    p0 = pts[0]
                    p1 = pts[1]
                    th = p0.w
                    if p0.x == p1.x:
                        w = th
                        h = abs(p1.y - p0.y) + th
                    else:
                        w = abs(p1.x - p0.x) + th
                        h = th
                    sizelines.append([w, h])
                else:
#                    print('loong lines not yet implemented',line, pad.name)
                    sizelines.append([0,0])

    return sizecircs, sizetherm, sizeoblon, sizerects, sizedons, sizesurfs, sizelines


def AddPadToModel(footprint_name, pinName, padDict, offset, angle, pad, klay, km, pth, drill=0, smm2=0, clm2=0, thWidth2=0, thGap2=0, customSurfPad = []):
    """
    Adds a Pad object to an open kicad mod tree
    :param: footprint_name the name of the parent footprint
    :param: pinName the pad name
    :param: padDict a dict of full pads
    :param: offset 2D point of location
    :param: angle rotation angle
    :param: pad a Pad6 object
    :param: klay the layer to add the pad to
    :param: km the KMT tree object
    :param: pth the pad type
    :param: drill the drill geometry, or 0 if no hole
    :param: smm2 solder mask margin from padstack selection
    :param: clm2 clearance margin from padstack selection
    :param: thWidth2 thermal width from padstack selection
    :param: thGap2 thermal gap from padstack selection
    :param: customSurfPad an extra surface to anchor to this pad
    """
    pName = pinName
    zc = KMT.Pad.CONNECT_SOLID #if drill != 0 else KMT.Pad.CONNECT_PARENT
#    slay = 'F.SilkS' if 'F.Cu' in klay else 'F.SilkS' if 'F.Mask' in klay else 'F.SilkS' if 'F.Paste' in klay else 'B.SilkS' # Silk layer

    if pad is None:
        if (type(drill) is list and (drill[0]<=0 or drill[1]<=0)) or (drill <= 0):
            print('bad pad',pth)
            sys.exit()
        else:
            km.append(KMT.Pad(type=pth,\
                             shape=KMT.Pad.SHAPE_CIRCLE,\
                             at=[offset[0],offset[1]],\
                             size=drill,\
                             layers=klay,\
                             number=pName, rotation=angle,
                             clearance = clm2, solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
                             drill = drill, zone_connect = zc
                             ))
            return
    else:
        if any('Mask' in k for k in klay) and not fusion:
            pName = ''#https://forum.kicad.info/t/copper-and-mask-in-different-layers/22717/4

    p = padDict[pad.connect]

    # Initialize clearances to zero by default
    psizecircs , psizetherm , psizeoblon , psizerects , psizedons , psizesurfs, psizelines  = GetPadSizes(p)
    clsizecircs, clsizetherm, clsizeoblon, clsizerects, clsizedons, clsizesurfs, clsizelines = GetPadSizes(p)
#    https://forum.kicad.info/t/how-do-i-create-a-soldermask-on-a-pad-different-to-the-pad-itself/10395

    if pad.clearance != '':
        c = padDict[pad.clearance]
        if GetPadSignature(p) != GetPadSignature(c):
            print('Non-matching cl signature',p,c)
        clsizecircs, clsizetherm, clsizeoblon, clsizerects, clsizedons, clsizesurfs, clsizelines = GetPadSizes(c)

    if pad.thermal != '':
        t = padDict[pad.thermal]
        if GetPadSignature(t) == [0,1,0,0,0,0,0]:
            th = t.therm[0]
            thWidth2 = max(th.w/zunit,thWidth2)
            thGap2 = max((th.tout - th.tin)/zunit, thGap2)
        elif GetPadSignature(t) == [0,0,0,0,0,2,0]:
            t = t # TODO!!
        elif GetPadSignature(t) == [0,0,0,0,0,0,0]:
            t = t # TODO!!
        elif GetPadSignature(t) == [0,0,0,0,0,0,4]:
            t = t # TODO!!
        else:
            print('Warning: non-matching th signature', pad.thermal, GetPadSignature(t))
            sys.exit()

    # create pads
    for i, circle in enumerate(p.circs):
        clm = max(clsizecircs[i][0] - psizecircs[i][0],clsizecircs[i][1] - psizecircs[i][1])/2/zunit
        pos = circle.pos
        diam = circle.r * 2 / zunit
        if customSurfPad == []:
            km.append(KMT.Pad(type=pth,\
                             shape=KMT.Pad.SHAPE_CIRCLE,\
                             at=[pos.x/zunit+offset[0],scaleY*pos.y/zunit+offset[1]],\
                             size=[diam,diam],\
                             layers=klay,\
                             number=pName, rotation=angle,
                             clearance = max(clm,clm2), solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
                             drill = drill, zone_connect = zc
                             ))
        elif pinName!='':
            roundingIssue = True
            if not roundingIssue:#https://gitlab.com/kicad/code/kicad/-/issues/4386
    #            print(customSurfPad[i]['geometry']['surface'])
                surfs = ProcessSurfaces(customSurfPad[int(pinName)-1]['geometry']['surface'])
                print(surfs)
                linepts = [pt for surf in surfs for outl in surf.outl if type(outl) is Line for pt in outl.pts ]
                arcs = [outl  for surf in surfs for outl in surf.outl if type(outl) is Arc]
                joutl = sorted(linepts+arcs, key=lambda p: p.o)
                prims = []
                lastPoint = None
                for ele in joutl:
                    if type(ele) is Point4 and lastPoint is None:
                        lastPoint = ele
                        continue
                    if type(ele) is Point4:
                        p0 = [lastPoint.x/zunit-offset[0], scaleY*lastPoint.y/zunit-offset[1]]
                        p1 = [ele.x/zunit-offset[0], scaleY*ele.y/zunit-offset[1]]
                        w = lastPoint.w/zunit if (lastPoint is not None and lastPoint.w != -1) else ele.w if ele.w != -1 else 0
                        kl = KMT.Line(start=p0, end=p1, width=w)
                        prims.append(kl)
                        lastPoint = ele
                    else:
                        if lastPoint != None:
                            ept = lastPoint
                            spt= ele.spt
                            p0 = [ept.x/zunit-offset[0], scaleY*ept.y/zunit-offset[1]]
                            p1 = [spt.x/zunit-offset[0], scaleY*spt.y/zunit-offset[1]]
                            w = ept.w/zunit if (ept is not None and ept.w != -1) else spt.w if spt.w != -1 else 0
                            kl = KMT.Line(start=p0, end=p1, width=w)
                            prims.append(kl)
                        cpt = [ele.cpt.x/zunit-offset[0], scaleY*ele.cpt.y/zunit-offset[1]]
                        spt = [ele.spt.x/zunit-offset[0], scaleY*ele.spt.y/zunit-offset[1]]
                        w = lastPoint.w/zunit if (lastPoint is not None and lastPoint.w != -1) else 0
                        kl = KMT.Arc(center=cpt, start=spt, angle=scaleY*ele.dalpha, width = w)
                        prims.append(kl)
                        lastPoint = ele.ept
                # Ensure it is closed
                spt = joutl[ 0].spt if type(joutl[ 0]) is Arc else joutl[ 0]
                ept = joutl[-1].ept if type(joutl[-1]) is Arc else joutl[-1]
                if spt.x != ept.x or spt.y != ept.y:
                    p0 = [ept.x/zunit-offset[0], scaleY*ept.y/zunit-offset[1]]
                    p1 = [spt.x/zunit-offset[0], scaleY*spt.y/zunit-offset[1]]
                    w = ept.w/zunit if (ept is not None and ept.w != -1) else spt.w if spt.w != -1 else 0
                    kl = KMT.Line(start=p0, end=p1, width=w)
                    prims.append(kl)
                #            TODO: km.append(KMT.Arc(center=[cx + offset[0], cy + offset[1]], start=[sx + offset[0], sy + offset[1]], angle=a, layers='F.SilkS', width=ow, hide=True, rotation=angle))
                km.append(KMT.Pad(type=pth,\
                     shape=KMT.Pad.SHAPE_CUSTOM,\
                     at=[pos.x/zunit+offset[0],pos.y/zunit+offset[1]],\
                     size = [diam,diam],
                     layers=klay,
                     primitives=prims,
                     number=pName, rotation=angle,
                     clearance = max(clm,clm2), solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
                     drill = drill, zone_connect = zc
                ))
            else:
                vertex = customSurfPad[int(pinName)-1]['geometry']['surface']['vertex']
                linepts = []
                if 'arc' in vertex.keys():
                    linepts += ProcessArcs(vertex['arc'],0.1,footprint_name)
                if 'pt' in vertex.keys():
                    linepts += ProcessLines(vertex['pt']).pts

                clay = klay
                if footprint_name == 'TB-CMS-AIROX-2':
                    clay.pop(klay.index('F.Paste'))# does not have it, hack because it was in heelprint in both Component and Comp_resist, but not in masque_comp

                linepts = sorted(linepts, key=lambda p: p.o)
                points = RemoveDuplicates(linepts)
                points = [[p.x/zunit, scaleY*p.y/zunit] for p in points]
#                km.append(KMT.Polygon(polygone = points, width = ow, layer = klay))
                prims = [KMT.Polygon(nodes=[(p[0] - offset[0], p[1] - offset[1]) for p in points])]

                km.append(KMT.Pad(type=pth,\
                     shape=KMT.Pad.SHAPE_CUSTOM,\
                     at=[pos.x/zunit+offset[0],pos.y/zunit+offset[1]],\
                     size = [diam,diam],
                     layers=clay,
                     primitives=prims,
                     number=pName, rotation=angle,
                     clearance = max(clm,clm2), solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
                     drill = drill, zone_connect = zc
                ))
    for i, therm in enumerate(p.therm):
        clm = max(clsizetherm[i][0] - psizetherm[i][0],clsizetherm[i][1] - psizetherm[i][1])/2/zunit
        width = therm.w/zunit
        gap = (therm.tout-therm.tin)/zunit
        if thWidth2 != 0 and width != thWidth2:
            print('Clash thwidth',width,thWidth2)
            sys.exit()
        if thGap2 != 0 and gap != thGap2:
            print('Clash thgap',width,thGap2)
            sys.exit()
        if therm.rou:
            diam = therm.tin/zunit
            km.append(KMT.Pad(type=pth,\
                             shape=KMT.Pad.SHAPE_CIRCLE,\
                             at=[offset[0],offset[1]],\
                             size=[diam,diam],\
                             radius=diam,\
                             layers=klay,\
                             zone_connect = KMT.Pad.CONNECT_THERMAL,\
                             number=pName, rotation=angle,\
                             clearance = max(clm,clm2), solder_mask_margin = smm2, thermal_width = width, thermal_gap = gap,
                             drill = drill
                            ))
        else:
            diam = therm.tin/zunit
            km.append(KMT.Pad(type=KMT.Pad.TYPE_SMT,\
                             shape=KMT.Pad.SHAPE_RECT,\
                             at=[offset[0],offset[1]],\
                             size=[diam,diam],\
                             layers=klay,\
                             zone_connect = KMT.Pad.CONNECT_THERMAL,\
                             number=pName, rotation=angle,
                             clearance = max(clm,clm2), solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
                             drill = drill
                            ))
    for i, obl in enumerate(p.oblon):
        clm = max(clsizeoblon[i][0] - psizeoblon[i][0],clsizeoblon[i][1] - psizeoblon[i][1])/2/zunit
    #        print('Warning: skipping oblong shape for now',footprint_name)
        b0 = obl.b0
        b1 = obl.b1
        # Convert to mm
        leftx=b0.x/zunit
        rightx=b1.x/zunit
        lefty=scaleY*b0.y/zunit
        righty=scaleY*b1.y/zunit
        km.append(KMT.Pad(type=pth,\
                             shape=KMT.Pad.SHAPE_OVAL,\
                             at=[offset[0],offset[1]],\
                             offset = [(leftx+rightx)/2,(lefty+righty)/2],\
                             size=[abs(rightx-leftx) if rightx != leftx else b0.w/zunit,abs(righty-lefty) if righty != lefty else b0.w/zunit],\
                             layers=klay,\
                             number=pName, rotation=angle + obl.angle,
                             clearance = max(clm,clm2), solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
                             drill = drill, zone_connect = zc
                            ))
    if GetPadSignature(p) == [0,0,0,0,0,0,1] and psizelines[0] != [0,0]:
#        print(footprint_name,p.lines)
        w = psizelines[0][0]/zunit
        h = psizelines[0][1]/zunit
        clm = max(clsizelines[0][0] - psizelines[0][0],clsizelines[0][1] - psizelines[0][1])/2/zunit
        km.append(KMT.Pad(type=pth,\
                             shape=KMT.Pad.SHAPE_OVAL,\
                             at=[offset[0],offset[1]],\
                             size=[w,h],\
                             layers=klay,\
                             number=pName, rotation=angle,
                             clearance = max(clm,clm2), solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
                             drill = drill, zone_connect = zc
                            ))
    for i, rect in enumerate(p.rects):
        clm = max(clsizerects[i][0] - psizerects[i][0],clsizerects[i][1] - psizerects[i][1])/2/zunit
        w = rect.w/zunit
        h = rect.h/zunit
        o = rect.ow/zunit
        px = rect.pos.x/zunit
        py = scaleY*rect.pos.y/zunit
        if rect.a != 0 or rect.fa != 0 or rect.fw != 0:
            print('Warning angles not yet done',rect)
#        km.append(KMT.RectLine(start=[px-w/2+offset[0],py-h/2+offset[1]], end=[px+w/2+offset[0],py+h/2+offset[1]], layer=slay, width=o))#, hide=True Only works for text, not lines
         # todo use KMT.FilledRect instead?
        km.append(KMT.Pad(type=pth,\
                     shape=KMT.Pad.SHAPE_RECT,\
                     at=[px+offset[0],py+offset[1]],\
                     size=[w,h],\
                     layers=klay,
                     number=pName, rotation=angle,
                     clearance = max(clm,clm2), solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
                     drill = drill, zone_connect = zc
                    ))
    for i, don in enumerate(p.dons):
#        print(footprint_name,klay,pth)
        clm = max(clsizedons[i][0] - psizedons[i][0],clsizedons[i][1] - psizedons[i][1])/2/zunit
        inr = don.inr/zunit
        outr = don.outr/zunit
        px = don.pos.x/zunit
        py = scaleY*don.pos.y/zunit
        if not 'F.Cu' in klay:
            print('Don no cu Unsupported yet',footprint_name)
            sys.exit()
        km.append(KMT.RingPad(type=pth,\
                     shape=KMT.Pad.SHAPE_CIRCLE,\
                     at=[px+offset[0],py+offset[1]],\
                     inner_diameter = 2*inr,\
                     size = 2*outr,\
                     number=pName, rotation=angle,
                     clearance = max(clm,clm2), solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
                     drill = drill, zone_connect = zc,
                     skip_mask = not 'F.Mask' in klay,
                     skip_paste = not 'F.Paste' in klay
                    ))
    if all(type(surf.outl[0]) is Arc for surf in p.surfs) or all(type(surf.outl[0]) is Line for surf in p.surfs):
        for i, surf in enumerate(p.surfs):
            clm = max(clsizesurfs[i][0] - psizesurfs[i][0],clsizesurfs[i][1] - psizesurfs[i][1])/2/zunit
            ow = surf.ow/zunit
            if len(surf.outl) == 0:
                print('empty outl',surf)
                sys.exit()
            if type(surf.outl[0]) is Arc:
                if len(surf.outl) != 1:
                    print('Multiarc pad not yet implemented') #            for pt in surf.outl:
                    print(surf.outl)
                    sys.exit()
                pt = surf.outl[0]
                cx = pt.cpt.x/zunit
                cy = scaleY*pt.cpt.y/zunit
                sx = pt.spt.x/zunit
                sy = scaleY*pt.spt.y/zunit
                a = scaleY*pt.dalpha
#                km.append(KMT.Arc(center=[cx + offset[0], cy + offset[1]], start=[sx + offset[0], sy + offset[1]], angle=a, layers=slay, width=ow, rotation=angle))# hide=True, only works for text, not lines
                km.append(KMT.Pad(type=pth,\
                             shape=KMT.Pad.SHAPE_CUSTOM,\
                             at=[offset[0],offset[1]],\
                             size = [0.2,0.2],
                             layers=klay,
                             primitives=[KMT.Arc(center=[cx + offset[0], cy + offset[1]], start=[sx + offset[0], sy + offset[1]], angle=a)],
                             number=pName, rotation=angle,
                             clearance = max(clm,clm2), solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
                             drill = drill, zone_connect = zc
                            ))
            elif type(surf.outl[0]) is Line:
                for pt in surf.outl:
                    for pp in pt:
                        if not all(p.w == pp[0].w for p in pp):
                            print('Warning: wrong spoints width',pt,pad.name)
                        rw = pp[0].w/zunit
                        rw = max(rw,0.02)

                        if len(pp)==4 and PolygonToBox(pp):
                            box = PolygonToBox(pp)
                            b0 = box.b0
                            b1 = box.b1
                            p0x = b0.x/zunit
                            p1x = b1.x/zunit
                            p0y = scaleY*b0.y/zunit
                            p1y = scaleY*b1.y/zunit
                            px = (p0x + p1x)/2
                            py = (p0y + p1y)/2
                            w = abs(p1x - p0x)
                            h = abs(p1y - p0y)
#                            km.append(KMT.RectLine(start=[p0x+offset[0],p0y+offset[1]], end=[p1x+offset[0],p1y+offset[1]], layer=slay, width=rw))#, hide=True only works for text, not lines
                             # todo use KMT.FilledRect instead?
                            km.append(KMT.Pad(type=pth,\
                                         shape=KMT.Pad.SHAPE_RECT,\
                                         at=[px+offset[0],py+offset[1]],\
                                         size=[w,h],\
                                         layers=klay,
                                         number=pName, rotation=angle,
                                         clearance = max(clm,clm2), solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
                                         drill = drill, zone_connect = zc
                                        ))
                        else:
                            # Polygonal pad
                            ps = []
                            for sp in pp:
                                ps.append([sp.x/zunit + offset[0],scaleY*sp.y/zunit + offset[1]])
#                            km.append(KMT.PolygoneLine(polygone=ps, layer=slay, width=ow))#, hide=True only works for texts, not lines
                            km.append(KMT.Pad(type=pth,\
                                         shape=KMT.Pad.SHAPE_CUSTOM,\
                                         at=[offset[0],offset[1]],\
                                         size = [0.2,0.2],
                                         layers=klay,
                                         primitives=[KMT.Polygon(nodes=[(sp.x/zunit + offset[0],scaleY*sp.y/zunit + offset[1]) for sp in pp])],
                                         number=pName, rotation=angle,
                                         clearance = max(clm,clm2), solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
                                         drill = drill, zone_connect = zc
                                        ))
            else:
                print('Unrecognized soutl',surf.outl,type(surf.outl[0]))
                sys.exit()
    elif footprint_name in ['GDCAFE-F_FERME','GDC.A', 'GDC.A1', 'GDCAFE_S60A0', 'GDCAFE_S60A180', 'GDCAFE_S76A0', 'GDCAFE_S76A180', 'TH_GDCAFE_S60A0', 'TH_GDCAFE_S60A180', 'CLR_GDCAFE_S60A0', 'CLR_GDCAFE_S60A180'] and pinName!='' and 'F.Cu' in klay:
        linepts = [pt for surf in p.surfs for outl in surf.outl if type(outl) is Line for pt in outl.pts ]
        arcs = [outl for surf in p.surfs for outl in surf.outl if type(outl) is Arc]

        allpts = sorted(linepts+arcs, key=lambda p: p.o)

        polypts = []
        for i in range(len(allpts)):
            if type(allpts[i]) is Arc:
                resolution = 0.1*zunit
                arc = allpts[i]
                spt = arc.spt
                ept = arc.ept
                cpt = arc.cpt
                radius = math.sqrt(math.pow(spt.x - cpt.x, 2) + math.pow(spt.y - cpt.y,2))
                alpha0 = math.atan2(spt.y-cpt.y,spt.x-cpt.x)
                dalpha = arc.dalpha*math.pi/180
                perimeter = abs(dalpha)*radius
                subdivisions = int(perimeter/resolution)
                subdivisions = max(subdivisions,3) # Avoid division by zero later on
#                print(perimeter,subdivisions,alpha0*180/math.pi,dalpha*180/math.pi,cpt,spt,ept)
                for s in range(subdivisions+1):
                    x = cpt.x+radius*math.cos(alpha0+s*dalpha/subdivisions)
                    y = cpt.y+radius*math.sin(alpha0+s*dalpha/subdivisions)
                    polypts.append([x/zunit,scaleY*y/zunit])
            else:
                polypts.append([allpts[i].x/zunit,scaleY*allpts[i].y/zunit])

        if footprint_name in ['GDC.A','GDC.A1', 'GDCAFE-F_FERME'] and pinName!='' and 'F.Mask' in klay:
            smm2 = 0.2038#GDCAFE_S76A180- GDCAFE_S60A180 = 16mil/2

        prims = [KMT.Polygon(nodes=[(p[0], p[1]) for p in polypts])]
        km.append(KMT.Pad(type=pth,\
             shape=KMT.Pad.SHAPE_CUSTOM,\
             at=[offset[0],offset[1]],\
             size = [0.2,0.2],
             layers=klay,
             primitives=prims,
             number=pName, rotation=angle,
             clearance = clm2, solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
             drill = drill, zone_connect = zc
        ))
    else:
        linepts = [pt for surf in p.surfs for outl in surf.outl if type(outl) is Line for pt in outl.pts ]
        arcs = [outl for surf in p.surfs for outl in surf.outl if type(outl) is Arc]
        joutl = sorted(linepts+arcs, key=lambda p: p.o)
        prims = []
        lastPoint = None
        for ele in joutl:
            if type(ele) is Point4:
                if lastPoint == None:
                    lastPoint = ele
                else:
                    p0 = [lastPoint.x/zunit, scaleY*lastPoint.y/zunit]
                    p1 = [ele.x/zunit, scaleY*ele.y/zunit]
                    w = lastPoint.w/zunit if (lastPoint is not None and lastPoint.w != -1) else ele.w if ele.w != -1 else 0
                    kl = KMT.Line(start=p0, end=p1, width=w)
                    prims.append(kl)
                    lastPoint = ele
            else:
                if lastPoint!= None and (ele.spt.x != lastPoint.x or ele.spt.y != lastPoint.y):
                    p0 = [lastPoint.x/zunit, scaleY*lastPoint.y/zunit]
                    p1 = [ele.spt.x/zunit, scaleY*ele.spt.y/zunit]
                    w = lastPoint.w/zunit if (lastPoint is not None and lastPoint.w != -1) else ele.spt.w if ele.spt.w != -1 else 0
                    kl = KMT.Line(start=p0, end=p1, width=w)
                    prims.append(kl)
                cpt = [ele.cpt.x/zunit, scaleY*ele.cpt.y/zunit]
                spt = [ele.spt.x/zunit, scaleY*ele.spt.y/zunit]
                w = lastPoint.w/zunit if (lastPoint is not None and lastPoint.w != -1) else 0
                kl = KMT.Arc(center=cpt, start=spt, angle=scaleY*ele.dalpha, width = w)
                prims.append(kl)
                lastPoint = ele.ept
#            TODO: km.append(KMT.Arc(center=[cx + offset[0], cy + offset[1]], start=[sx + offset[0], sy + offset[1]], angle=a, layers=slay, width=ow, hide=True, rotation=angle))
        km.append(KMT.Pad(type=pth,\
             shape=KMT.Pad.SHAPE_CUSTOM,\
             at=[offset[0],offset[1]],\
             size = [0.2,0.2],
             layers=klay,
             primitives=prims,
             number=pName, rotation=angle,
             clearance = clm2, solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
             drill = drill, zone_connect = zc
        ))

    # Silk-work
    for line in p.lines:
    #        pts = []
#        klay = slay
        for pt in line:
            if type(pt) is Arc:
                cx = pt.cpt.x/zunit
                cy = scaleY*pt.cpt.y/zunit
                sx = pt.spt.x/zunit
                sy = scaleY*pt.spt.y/zunit
                a = scaleY*pt.dalpha
#                km.append(KMT.Arc(center=[cx + offset[0], cy + offset[1]], start=[sx + offset[0], sy + offset[1]], angle=a, layer=klay, width = 0.05, rotation=angle))#, hide = True only works for text, not lines
            else:
                # Line
                ps = []
                if any(p.tarc for p in pt):
                    print('Warning: tarc geometry not yet implemented',footprint_name,pt)
                    ws = [p.w for p in pt if not p.tarc]
                    if len(ws)<1:
                        print('wrong ws width',pt)
                        sys.exit()
                    if not all(w==ws[0] for w in ws):
                        print('Wrong width',ws)
                        sys.exit()
                    w = ws[0]/zunit
                    for p in pt:
                        ps.append([p.x/zunit + offset[0],scaleY*p.y/zunit + offset[1]])
                elif not all(p.w == pt[0].w for p in pt):
                    print('Warning: wrong lpoints width',pad)
                    sys.exit()
                else:
                    w = pt[0].w/zunit
                    for p in pt:
                        ps.append([p.x/zunit + offset[0],scaleY*p.y/zunit + offset[1]])

#                km.append(KMT.PolygoneLine(polygone=ps, layer=klay, width = w, rotation=angle))#, hide=True only works for text, not lines


def SaveFile(board, args):
    """
    Saves KiCad PCB to file
    :param: board the pcbnew board
    :args: the cli parsed arguments
    """
    kicadName = os.path.join(args.outdir,args.fname+".kicad_pcb")
    if os.path.exists(kicadName):
        print('Overwriting KiCAD PCB file',kicadName)
    else:
        print('Writing KiCAD PCB file to',kicadName)
    board.Save(kicadName)

def PolygonToBox(points):
    # Converts 4 vertices into a box
    if len(points)!=4:
        print('bad polybox',points)
        sys.exit()

    p00 = points[0]
    p01 = points[1]
    p11 = points[2]
    p10 = points[3]
    if   p00.x == p01.x and p00.y == p10.y and p01.y == p11.y and p11.x == p10.x:
        return Box(p00,p11)

    p01 = points[0]
    p11 = points[1]
    p10 = points[2]
    p00 = points[3]
    if   p00.x == p01.x and p00.y == p10.y and p01.y == p11.y and p11.x == p10.x:
        return Box(p00,p11)

    return False

def CreatePadFootprint(folder, footprint_name, padDict, padFp):
    """
    Creates a footprint of a pad
    :param: folder the footprint folder library
    :param: footprint_name the footprint name
    :param: padDict a dict with all full pad objects
    :param: padFp the name of the pad
    """
    km = KMT.Footprint(footprint_name)

    # set general values
    km.append(KMT.Text(type='reference', text='REF**', at=[0,0], layer='F.SilkS', hide=True))
    km.append(KMT.Text(type='value', text=footprint_name, at=[0,2], layer='F.Fab', hide=True))

    pad = padDict[padFp]
    b0 = pad.box.b0
    b1 = pad.box.b1
    # Convert to mm
    leftx=b0.x/zunit
    rightx=b1.x/zunit
    lefty=scaleY*b0.y/zunit
    righty=scaleY*b1.y/zunit

    # Create silkscreen
#    km.append(KMT.RectLine(start=[leftx,lefty], end=[rightx,righty], layer='F.SilkS', width=0.15))

    # create courtyard
    km.append(KMT.RectLine(start=[leftx,lefty], end=[rightx,righty], layer='F.CrtYd', width=0.05, offset=0))

    pad6 = Pad6('','',padFp,'','','')
    AddPadToModel(footprint_name,'~',padDict,[0,0],0,pad6,KMT.Pad.LAYERS_SMT,km, KMT.Pad.TYPE_SMT)# we name pin as ~ to match it with EEschema symbol


    # write file
    file_handler = KMT.KicadFileHandler(km)
    file_handler.writeFile(os.path.join(folder,footprint_name+'.kicad_mod'),timestamp=0)


def CreatePadstackFootprint(folder, footprint_name, pslay, box, allPads, padDict):
    """
    Creates a footprint of a padstack
    :param: folder the footprint folder library
    :param: footprint_name the footprint name
    :param: pslay the footlayer
    :param: box area of the footprint
    :param: allPads array of Pad6 in this padstack
    :param: padDict map between pad name and full Pad class
    """
    km = KMT.Footprint(footprint_name)

    # set general values
    km.append(KMT.Text(type='reference', text='REF**', at=[0,0], layer='F.SilkS', hide=True))
    km.append(KMT.Text(type='value', text=pslay, at=[0,2], layer='F.Fab', hide=True))

    b0 = box.b0
    b1 = box.b1
    # Convert to mm
    leftx=b0.x/zunit
    rightx=b1.x/zunit
    lefty=scaleY*b0.y/zunit
    righty=scaleY*b1.y/zunit

    # create courtyard
    km.append(KMT.RectLine(start=[leftx,lefty], end=[rightx,righty], layer='F.CrtYd', width=0.05, offset=0))

    pth = KMT.Pad.TYPE_SMT
    padstack = PadStack(footprint_name, allPads,pth,0,None,0)
    pcname = AddPadstackToModel(footprint_name,'~',padDict,[0,0],0,padstack,km)

    # write file
    file_handler = KMT.KicadFileHandler(km)
    file_handler.writeFile(os.path.join(folder,footprint_name+'.kicad_mod'),timestamp=0)

    return pcname


def CreateViaFootprint(folder, footprint_name, pslay, box, circle, oval, allPads, padDict, pstype):
    """
    Creates a footprint that mimicks a via
    :param: folder the footprint folder library
    :param: footprint_name the footprint name
    :param: pslay the footlayer
    :param: box area of the footprint
    :param: circle the circular hole shape
    :param: oval the oval hole shape
    :param: allPads array of Pad6 in this padstack
    :param: padDict map between pad name and full Pad class
    :param: psType if plated or non-plated
    """
#    https://github.com/pointhi/kicad-footprint-generator/blob/880c84ad508978a2d4bd26564c56f1a251edc62a/scripts/example_kicadmodtree_script.py
    km = KMT.Footprint(footprint_name)
#    km.setDescription("")
#    km.setTags("example")

    # Convert to mm
#    b0 = box.b0
#    b1 = box.b1
#    leftx=b0.x/zunit
#    rightx=b1.x/zunit
#    lefty=scaleY*b0.y/zunit
#    righty=scaleY*b1.y/zunit

    # set general values
    km.append(KMT.Text(type='reference', text='REF**', at=[0,0], layer='F.SilkS', hide=True))
    km.append(KMT.Text(type='value', text=footprint_name, at=[0,2], layer='F.Fab', hide=True))

    # create silscreen
#    km.append(KMT.RectLine(start=[leftx,lefty], end=[rightx,righty], layer='F.SilkS', width=0.05))

    # create courtyard
#    km.append(KMT.RectLine(start=[leftx,lefty], end=[rightx,righty], layer='F.CrtYd', width=0.05, offset=0))

    if pstype!='PLATED' and pstype!='NONPLATED':
        print('unknown pstype',footprint_name)
        sys.exit()

    pth = KMT.Pad.TYPE_THT if pstype=='PLATED' else KMT.Pad.TYPE_NPTH

#    pcname = filPads[0].connect
#    if not pcname in padDict.keys():
#        print('Missing reference pad')
#        sys.exit()
#    for p in allPads:
#        print(p)
    ovalSize = [0,0]
    if circle is None and oval is None:
        print('Error, Missing circle or oval')
        sys.exit()
    if not circle is None and not oval is None:
        print('Error, Double circle or oval')
        sys.exit()

    if not circle is None:
        drillRadius=circle.r
    else:
        drillRadius = max(oval.width,oval.height)
        if oval.angle == 90:
            ovalSize = [oval.width,oval.height]
        else:
            print('Unsupported oval angle',oval)
            sys.exit()

    padstack = PadStack(footprint_name, allPads,pth,drillRadius,oval,ovalSize)
    AddViaToModel(footprint_name,'~',padDict,[0,0],0,padstack,km)# we name pin as ~ to match it with Eeschema symbol

    # write file
    file_handler = KMT.KicadFileHandler(km)
    file_handler.writeFile(os.path.join(folder,footprint_name+'.kicad_mod'),timestamp=0)

    return padstack


def AddViaToModel(footprint_name,pinName,padDict,offset,angle,padstack,km):
    """
    Adds a via to an open Kicad Mod Tree
    :param: footprint_name the name of the footprint
    :param: pinName the pin name
    :param: padDict a dict of full pads
    :param: offset 2D point of location
    :param: angle rotation angle
    :param: padstack the padstack object
    :param: km the KMT tree object
    """
    drillRadius = padstack.drillRadius/zunit
    pth = padstack.pth
    oval = padstack.oval
    ovalSize = [o/zunit for o in padstack.ovalSize]
    drill=2*drillRadius if oval is None else ovalSize
    psname = padstack.psname

    # Sanity-check no duplicates
    laynames = [p.lay for p in padstack.allPads]
    if len(laynames) != len(set(laynames)):
        print('Repeated layers')
        sys.exit()
    for lay in laynames:
        if not lay in padMap.keys():
            print('Unexpected pad layer',lay)
            sys.exit()

    if not fusion:
        for pad in padstack.allPads:
            klay = [padMap[pad.lay]]
            AddPadToModel(footprint_name, pinName, padDict, offset, angle, pad, klay, km, KMT.Pad.TYPE_SMT)
        # Add now the hole
        AddPadToModel(footprint_name, pinName, padDict, offset, angle, None, KMT.Pad.LAYERS_THT, km, pth, drill)
    else:
        allPads = padstack.allPads
        padFp = ''
        smFp = ''
        clFp = ''
        thFp = ''
        lnames = [p.lay for p in allPads]
        if psname in ['DF3_P.V', 'DF3_P.V-1', 'DF3_VIA', 'MEL26.5M', 'MEL29M-2', 'MEL35M', 'P.G', 'P.K', 'P.K1', 'P.K1-1', 'P.V', 'P.V-1', 'VIA-091X03-VE', 'VIA-100X04-VE', 'DF3_DIP', 'DF4_DIP', 'DIP', 'DIP-1', 'P.H', 'P.H-1', 'P.H-2MM67', 'P.H-2MM67-1', 'P.N', 'P.N-1', 'P.V-2MM', 'PRP0.7-1.4224', 'PRP0.7-2.0.32' , 'FGP1.1X3.8-2.1X4.8']:
            padFp = allPads[lnames.index('Component')].connect
            if 'Comp_resist' in lnames:
                smFp = allPads[lnames.index('Comp_resist')].connect
            elif 'Solder_resist' in lnames:
                smFp = allPads[lnames.index('Solder_resist')].connect
            clFp = allPads[lnames.index('Component')].clearance
            thFp = allPads[lnames.index('Component')].thermal
        elif psname in ['VIA-08X03','VIA-08X03-VE', 'MC3004_VIA', 'P.Q', 'P.Q-1','VIA-091X03-VS','FGP0.8-1.1X1.8', 'FGP.G-P1.1X2', 'FNG1.65X1.1-ORIY0.275', 'MEL25M', 'MEL32M-60', 'P.E', 'P.E-1', 'P.F', 'P.H2', 'P.E-1', 'P.I', 'P.M', 'P.M-1', 'P.M1', 'P.M1-1', 'P.N1', 'P.N3', 'P.O', 'VIA-091X03', 'DF3_DIP-1', 'DF3_P.N', 'DF3_P.N-1','P.F-1', 'PCP0.95-1.6', 'PRP0.95-1.6', 'PRP1.0-1.7272', 'MEL08', 'MEL12', 'MEL13', 'MEL15', 'MEL20', 'MEL25', 'MEL30', 'MEL32', 'MEL35', 'MEL36', 'MEL40', 'MEL45', 'MEL46', 'MEL60', 'DF3_MEL18', 'FNG2.7X1.7']:
            padFp = allPads[lnames.index('Component')].connect
            if 'Comp_resist' in lnames:
                smFp = allPads[lnames.index('Comp_resist')].connect
            elif 'Solder_resist' in lnames:
                smFp = allPads[lnames.index('Solder_resist')].connect
            if 'all_inner' in lnames:
                clFp = allPads[lnames.index('all_inner')].clearance
                thFp = allPads[lnames.index('all_inner')].thermal
        elif psname in ['PRP0.95-1.6-FS-NM']:
            padFp = allPads[lnames.index('Solder')].connect
            smFp = allPads[lnames.index('Solder_resist')].connect
        else:
            print('Via footprint not yet valid')
            print(footprint_name,allPads)
            sys.exit()

        if pinName == '~' or pinName == '':
            pad = allPads[lnames.index('Component') if 'Component' in lnames else lnames.index('Solder')]
                # Store noconnect info in dictionary just for padstack-via footprints
            if pad.noconnect != '':
                if pad.noconnect != pad.connect:
                    print('Unsupported noconnect w different pad names',pad)
                    sys.exit()
                if not psname in reconnectDict.keys():
                    reconnectDict[psname] = pad.noconnect
                elif reconnectDict[psname]!=pad.noconnect:
                    print('Pin name conflict in dict, overwriting dict',pad.noconnect,reconnectDict[psname],psname)
                    reconnectDict[psname] = pad.noconnect
            else:
                if psname in reconnectDict.keys() and reconnectDict[psname] != '':
                    print('Pin name conflict in dict, overwriting dict',pad.noconnect,reconnectDict[psname],psname)
                    reconnectDict[psname] = pad.noconnect

            if 'all_inner' in lnames:
                if fusion and allPads[lnames.index('Component') if 'Component' in lnames else lnames.index('Solder')].connect != allPads[lnames.index('all_inner')].connect:
                    print('Warning: unsupported via with fusion',psname,allPads[lnames.index('Component') if 'Component' in lnames else lnames.index('Solder')].connect,allPads[lnames.index('all_inner')].connect)
    #                sys.exit()
                if fusion and allPads[lnames.index('Component') if 'Component' in lnames else lnames.index('Solder')].noconnect != allPads[lnames.index('all_inner')].noconnect and allPads[lnames.index('Component') if 'Component' in lnames else lnames.index('Solder')].noconnect != '' and allPads[lnames.index('all_inner')].noconnect != '':
                    print('Warning: Unsupported ncon with fusion',psname,allPads[lnames.index('Component') if 'Component' in lnames else lnames.index('Solder')].noconnect,allPads[lnames.index('all_inner')].noconnect)

    #            if fusion and allPads[lnames.index('Component')].clearance != allPads[lnames.index('all_inner')].clearance:
    #                print('Unsupported cl with fusion',psname,allPads[lnames.index('Component')].clearance,allPads[lnames.index('all_inner')].clearance)
    #                sys.exit()
    #            if fusion and allPads[lnames.index('Component')].thermal != allPads[lnames.index('all_inner')].thermal:
    #                print('Unsupported th with fusion',psname,allPads[lnames.index('Component')].thermal,allPads[lnames.index('all_inner')].thermal)
    #                sys.exit()
                if allPads[lnames.index('all_inner')].noconnect != '':
                    if not psname in innerReconnectDict.keys():
                        innerReconnectDict[psname] = allPads[lnames.index('all_inner')].noconnect
                    elif innerReconnectDict[psname]!=allPads[lnames.index('all_inner')].noconnect:
                        print('Pin name conflict in inn dict, overwriting dict',pad.noconnect,innerReconnectDict[psname],psname)
                        innerReconnectDict[psname] = allPads[lnames.index('all_inner')].noconnect

        # Sanity-checks
        if padFp == '':
            print(footprint_name,allPads)
            sys.exit()
        if not padFp in padDict.keys():
            print('Cannot create via, pad',padFp,'missing in dict',footprint_name)
            return
        if len(padDict[padFp].circs)+len(padDict[padFp].rects)+len(padDict[padFp].oblon)+len(padDict[padFp].lines)!=1:
            print('Cannot have this pad',padFp,padDict[padFp])
            sys.exit()

        pad = allPads[lnames.index('Component') if 'Component' in lnames else lnames.index('Solder')]
        pas = GetPadSizes(padDict[padFp])
        if smFp != '':
            sms = GetPadSizes(padDict[ smFp])

            diffs = []
            for i in range(len(pas)):
                if len(pas[i]) == 1 and len(sms[i]) == 1:
                    diff = max(sms[i][0][0]-pas[i][0][0],sms[i][0][1]-pas[i][0][1])
                    if diff < 0:
                        print('wrong diff',diff)
                        sys.exit()
                    if diff > 0:
                        diffs.append(diff)
            smm = min(diffs)/zunit/2 if len(diffs)>0 else 0
        else:
            smm = 0
        if clFp != '':
            cls = GetPadSizes(padDict[ clFp])

            diffs = []
            for i in range(len(pas)):
                if len(pas[i]) and len(cls[i]) == 1:
                    diff = max(cls[i][0][0]-pas[i][0][0],cls[i][0][1]-pas[i][0][1])
                    if diff < 0:
                        print('wrong diff',diff)
                        sys.exit()
                    if diff > 0:
                        diffs.append(diff)
            clm = min(diffs)/zunit/2 if len(diffs)>0 else 0
        else:
            clm = 0

        klay = KMT.Pad.LAYERS_THT if smFp != '' else ['*.Cu']
        AddPadToModel(footprint_name, pinName, padDict, offset, angle, pad, klay, km, pth, drill,smm,clm)



def CreateViaFootprintVariation(libPath, footprint_name, subname, removeCopper, conductorLayers):
    """
    Creates a footprint that mimicks a non-standard via
    :param: libPath the path to the footprint folder library
    :param: footprint_name the original footprint name
    :param: subname the suffix to add for the variation
    :param: removeCopper the layers to remove copper from
    :param: conductorLayers number of conductor layers
    """
#    print('Creating s',footprint_name+subname)

    footprint = pcbnew.FootprintLoad(libPath, footprint_name)
    if footprint is None:
        print('Footprint template not found',footprint_name,libPath)
        sys.exit()

    m = pcbnew.MODULE(footprint)
    fpid = pcbnew.LIB_ID("", footprint_name+subname)
    m.SetFPID(fpid)
    m.SetValue(footprint_name+subname)
    lset = m.Pads().GetFirst().GetLayerSet()
    for rc in removeCopper:
        lset.RemoveLayer(rc)
    for rc in range(pcbnew.F_Cu+conductorLayers-1,pcbnew.B_Cu):#remove extra inner ones past In4.Cu
        lset.RemoveLayer(rc)
    m.Pads().SetLayerSet(lset)
    pcbnew.FootprintSave(libPath,m)


def CreateComponentFootprintVariation(libPath, footprint_name, subname, removeCopper, conductorLayers, layerMap):
    """
    Creates a footprint of a component with non standard via pins
    :param: libPath the path to the footprint folder library
    :param: footprint_name the original footprint name
    :param: subname the suffix to add for the variation
    :param: removeCopper the layers to remove copper from
    :param: conductorLayers number of conductor layers
    :param: layermap Zuken to KiCad layer mapping
    """
#    print('Creating s',footprint_name+subname)

    footprint = pcbnew.FootprintLoad(libPath, footprint_name)
    if footprint is None:
        print('Footprint template not found',footprint_name,libPath)
        sys.exit()

#    print('Creating component variation',footprint_name+subname)
    m = pcbnew.MODULE(footprint)
    fpid = pcbnew.LIB_ID("", footprint_name+subname)
    m.SetFPID(fpid)
    m.SetValue(footprint_name+subname)
    for j,p in enumerate(m.Pads()):
#        pname = p.GetName()
        lset = p.GetLayerSet()
#        if pname == '':
#            continue # This is a NPTH mechanical hole
#        if not pname in removeCopper.keys():
#            print('Missing pname in rmCopp dict',pname)
#            sys.exit()
        for rc in removeCopper[j]:
            klay = layerMap[rc-1]
            lset.RemoveLayer(klay)
        for rc in range(pcbnew.F_Cu+conductorLayers-1,pcbnew.B_Cu):#remove extra inner ones
            lset.RemoveLayer(rc)
        p.SetLayerSet(lset)
    pcbnew.FootprintSave(libPath,m)

def BoxToMM(box):
    """
    Converts Zuken box to mm and kicad coordinates
    :param: box the Box object
    Returns array of start and end coordinates
    """
    b0 = box.b0
    b1 = box.b1
    # Convert to mm
    leftx=b0.x/zunit
    rightx=b1.x/zunit
    lefty=scaleY*b0.y/zunit
    righty=scaleY*b1.y/zunit

    return [leftx,lefty], [rightx, righty]


def ProcessFootprint(libPath, fp, padDict, padstackDict, scaling=1, map3d = None, conductorLayers=2):
    """
    Creates a KiCAD footprint based on the Zuken JSON fp
    :param: libPath the footprint folder library
    :param: fp the Zuken JSON footprint object
    :param: padDict the dict of fullPads
    :param: padstackDict the map of padstack to pad
    :param: scaling a global scaling factor for logos
    :param: map3d a dict of footprint name to 3d model
    :param: conductorLayers number of conductor layers
    """

    # Sanity-check
    possibleKeys = ['name', 'uver', 'cuser', 'uuser', 'ctime', 'utime', 'area', 'grid', 'minRect', 'polarity', 'panelUse', 'heelprint', 'toeprint', 'propertyS', 'o']
    for k in fp.keys():
        if not k in possibleKeys:
            print('Unexpected fp key',fp.keys())
            sys.exit()
    if fp['polarity']!='YES':
        print('Unexpected polarity',fp['polarity'])
        sys.exit()
    if fp['panelUse']!='NO':
        print('Unexpected panelUse',fp['panelUse'])
        sys.exit()

    global zunit
    oldzunit = zunit
    footprint_name = fp['name']
    if scaling != 1:

        subname = "_" + str(scaling)
        fpName = footprint_name
        if os.path.isfile(os.path.join(libPath,fpName+subname+'.kicad_mod')):
            if os.path.getctime(os.path.join(libPath,fpName+subname+'.kicad_mod')) < os.path.getctime(os.path.join(libPath,fpName+'.kicad_mod')):#https://stackoverflow.com/questions/12817041/check-if-the-file-is-newer-then-some-other-file
                # update it because master footprint has been changed

                footprint_name += subname
                print('Recreating footprint scaled variation',footprint_name)
                zunit /= scaling
            else:
                # already generated in this session, no need to redo
                return
        else:
            footprint_name += subname
            print('Creating footprint scaled variation',footprint_name)
            zunit /= scaling

    abox = ProcessBox(fp['area']['box'])
    mbox = ProcessBox(fp['minRect']['box'])

    km = KMT.Footprint(footprint_name)

    # set general values
    km.append(KMT.Text(type='reference', text='REF**', at=[0,0], layer='F.SilkS', hide=False))
    km.append(KMT.Text(type='value', text=footprint_name, at=[0,2], layer='F.Fab', hide=True))

    # Create drawing
    bstart, bend = BoxToMM(mbox)
    km.append(KMT.RectLine(start=bstart, end=bend, layer='Dwgs.User', width=0.05))

    # create courtyard
#    bstart, bend = BoxToMM(abox)
#    km.append(KMT.RectLine(start=bstart, end=bend, layer='F.CrtYd', width=0.05))

    # Analyze heelprint
    hp = fp['heelprint']
    hbox = ProcessBox(hp['minRect']['box'])
    bstart, bend = BoxToMM(hbox)
#    km.append(KMT.RectLine(start=bstart, end=bend, layer='B.SilkS', width=0.1))

    customSurfPad = []
    extraPads = []

    if 'layer' in hp['layout']:
        layers = hp['layout']['layer']
        if not type(layers) is list:
            layers = [layers]
        for layer in layers:
            lname = layer['name']
            if not 'type' in layer.keys() and not 'fpadstack' in layer.keys():
                print('warning no type or fpadstack',layer,footprint_name)
                sys.exit()
            if 'type' in layer.keys():
                ltype = layer['type']
                # Sanity-check
                if ltype!='footLayer':
                    print('nonsupported ltype',ltype)
                    sys.exit()
            possibleKeys = ['name','type','line', 'area', 'surface', 'text', 'o', 'fpadstack']
            lkeys = layer.keys()
            for lk in lkeys:
                if not lk in possibleKeys:
                    print('Unsupported lk',lk)
                    sys.exit()
            klay = layerDict[lname] if lname in layerDict.keys() else 'Dwgs.User'

            if 'fpadstack' in lkeys:
                 if list(layer.keys()) != ['name','fpadstack', 'o']:
                     print('unrecognized hp fpadst layer keys',layer)
                     sys.exit()
                 if layer['name'] != 'FPADSTACK':
                    print('unrecognized hp fp stack',layer)
                    sys.exit()
                 fpads = layer['fpadstack']
                 if not type(fpads) is list:
                     fpads = [fpads]
                 for fpad in fpads:
                     possibleKeys = ['pt','padstackGroup','angle','o']
                     for k in fpad.keys():
                         if not k in possibleKeys:
                             print('wrong fpad keys',k)
                             print(sys.exit())
                     psg = fpad['padstackGroup']
                     if list(psg.keys())!= ['type','padstack','o']:
                         print('wrong pstg keys',psg)
                         sys.exit()
                     if psg['type'] != 'default':
                         print('wrong psg type',psg)
                         sys.exit()
    #                 pName = pin['name']
                     angle = fpad['angle'] if 'angle' in fpad.keys() else 0
    #                 pos = ProcessPoint(pin)
                     pos2 = ProcessPoint(fpad)
                     psname = psg['padstack']
                     pos = pos2
    #                     if pos.x != pos2.x or pos.y != pos2.y:
    #                         print('mismatch pinpos, using pos2',pos,pos2)
    #                         pos = pos2
                     if not os.path.exists(os.path.join(libPath,psname+".kicad_mod")):
                         print('Non found padstack',psname)
                         sys.exit()

                     origin = [pos.x/zunit, pos.y*scaleY/zunit]
                     # Load padstack FP
                     pstdict = padstackDict[psname]
                     if type(pstdict) is PadStack:
                         if pstdict.drillRadius > 0:
                             AddViaToModel     (footprint_name, '', padDict, origin, angle, pstdict, km)
                         else:
                             AddPadstackToModel(footprint_name, '', padDict, origin, angle, pstdict, km, customSurfPad)
                     else:
                         print('Missing pstdict',psname, pstdict)
                         sys.exit()

            if 'text' in lkeys:
                hjustifyMap = {'LO_L': KMT.Text.HJUSTIFY_LEFT, 'LO_C': KMT.Text.HJUSTIFY_CENTER, 'LO_R': KMT.Text.HJUSTIFY_RIGHT, 'CE_L': KMT.Text.HJUSTIFY_LEFT, 'CE_C': KMT.Text.HJUSTIFY_CENTER, 'CE_R': KMT.Text.HJUSTIFY_RIGHT}
#                vjustifyMap = {'LO_L': pcbnew.GR_TEXT_VJUSTIFY_BOTTOM, 'LO_C': pcbnew.GR_TEXT_VJUSTIFY_BOTTOM, 'LO_R': pcbnew.GR_TEXT_VJUSTIFY_BOTTOM, 'CE_L': pcbnew.GR_TEXT_VJUSTIFY_CENTER, 'CE_C': pcbnew.GR_TEXT_VJUSTIFY_CENTER, 'CE_R': pcbnew.GR_TEXT_VJUSTIFY_CENTER}

                texts = layer['text']
                if not type(texts) is list:
                    texts = [texts]
                for text in texts:
                    t = ProcessText(text)
                    tstring = str(t.string)
                    tstring = re.sub('\\n','\\\\n',tstring)#escape newlines
                    mlays = klay
                    if not type(mlays) is list:
                        mlays = [mlays]
                    for mlay in mlays:
                        km.append(KMT.Text(type=KMT.Text.TYPE_USER, text=tstring,
                                       at=[t.position.x/zunit,scaleY*t.position.y/zunit],
                                       thickness=t.thickness/zunit,
                                       rotation=scaleY*t.angle,
                                       mirror = (t.flip=='X'),
                                       justify = hjustifyMap[t.justify],
                                       layer = mlay
                                       ))
            if 'line' in lkeys:
                lines = layer['line']
                if not type(lines) is list:
                    lines = [lines]
                epcounter = 0
                for line in lines:
                    if not type(line) is dict:
                        print('empty line segl',line)
                        print(layer['line'])
                        sys.exit()

                    # Analyze vertex
                    arcs, lines = ProcessArcsLines(line['geometry']['line'],footprint_name)
                    for line in lines:
                        for subline in line:
                            points = RemoveDuplicates(subline.pts)
                            w = points[0].w/zunit
                            points = [ [pt.x/zunit,scaleY*pt.y/zunit] for pt in points]
                            if footprint_name in ['MOL52610-22'] and klay == 'F.Cu':
                                if len(points)!=2:
                                    print('Unexpected line length',footprint_name)
                                    sys.exit()
                                p0x = points[0][0]
                                p1x = points[1][0]
                                p0y = points[0][1]
                                p1y = points[1][1]
                                pc = [(p0x + p1x)/2, (p0y + p1y)/2]
                                if p0x != p1x:
                                    print('Unexpected shape',footprint_name)
                                    sys.exit()
                                h = (p1y - p0y) + w

                                epcounter +=1
                                pName = 'EP' + str(epcounter)
#                                km.append(KMT.PolygoneLine(polygone=points, layer='F.SilkS', width=w))#, hide = True not working with lines
                                extraPads.append(KMT.Pad(type=KMT.Pad.TYPE_SMT,
                                             shape=KMT.Pad.SHAPE_OVAL,
                                             at=pc,
                                             size = [w,h],
                                             layers=[klay],
                                             number=pName,
                                             zone_connect = KMT.Pad.CONNECT_SOLID
                                            ))
                            else:
                                mlays = klay
                                if not type(mlays) is list:
                                    mlays = [mlays]
                                for mlay in mlays:
                                    km.append(KMT.PolygoneLine(polygone = points, width = w, layer = mlay))

                    for arc in arcs:
                        for subarc in arc:
                            cpt = subarc.cpt
                            spt = subarc.spt
                            w = cpt.w/zunit
                            mlays = klay
                            if not type(mlays) is list:
                                mlays = [mlays]
                            for mlay in mlays:
                                km.append(KMT.Arc(center = [cpt.x/zunit, scaleY*cpt.y/zunit], start = [spt.x/zunit, scaleY*spt.y/zunit], angle = scaleY*subarc.dalpha, width = w, layer = mlay))

            for tag in ['area', 'surface']:
                if tag in lkeys:
                    surfaces = layer[tag]
                    if not type(surfaces) is list:
                        surfaces = [surfaces]
                    for surface in surfaces:
                        ow = surface['geometry']['surface']['outlineWidth']/zunit
                        if footprint_name in ['TB-CMS-AIROX-2'] and klay == 'F.Cu':
                            customSurfPad.append(surface)
                            continue
                        if not 'FEE_COMP' in footprint_name and not 'FILM18_HORIZONTAL' in footprint_name and not 'FILM13_HORIZONTAL' in footprint_name and not 'FILM30_HORIZONTAL' in footprint_name and not 'TB-CMS-AIROX-2' in footprint_name:
                            ow = max(ow,0.02)
                            arcs, lines = ProcessArcsLines(surface['geometry']['surface'],footprint_name,klay in ['F.CrtYd','B.CrtYd'])

                            for line in lines:
                                for subline in line:
                                    points = RemoveDuplicates(subline.pts)
                                    points = [ [pt.x/zunit,scaleY*pt.y/zunit] for pt in points]

                                    if len(arcs[0]) == 0:
                                        if footprint_name in ['SO08-PPAD_COMPOSANT','SO08-PPAD_SI7884'] and klay == 'F.Cu':
                                            pName = '9' if footprint_name == 'SO08-PPAD_COMPOSANT' else 'D4' if footprint_name == 'SO08-PPAD_SI7884' else '?'
#                                            km.append(KMT.PolygoneLine(polygone=points, layer='F.SilkS', width=ow, hide=True))# hide= True not working with lines
                                            extraPads.append(KMT.Pad(type=KMT.Pad.TYPE_SMT,
                                                         shape=KMT.Pad.SHAPE_CUSTOM,
                                                         anchor_shape = KMT.Pad.SHAPE_RECT,
                                                         at=[0,0],
                                                         size = [2,2],
                                                         layers=[klay],
                                                         primitives=[KMT.Polygon(polygone=points)],
                                                         number=pName,
                                                         zone_connect = KMT.Pad.CONNECT_SOLID
                                                        ))
                                        elif footprint_name == 'ARF7151A' and len(subline.pts)==4 and klay in ['F.Mask','B.Mask']:
                                            if klay == 'B.Mask':
                                                continue
                                            klay = '*.Mask'
                                            pName = ''
                                            if not PolygonToBox(subline.pts):
                                                print('Failed2box',subline.pts)
                                                sys.exit()
                                            print(subline.pts)
                                            box = PolygonToBox(subline.pts)
                                            print(box)
                                            r = subline.pts[0].w/zunit
                                            b0 = box.b0
                                            b1 = box.b1
                                            p0x = b0.x
                                            p1x = b1.x
                                            p0y = b0.y
                                            p1y = b1.y
                                            cx = (p0x + p1x)/2
                                            cy = (p0y + p1y)/2
                                            w = abs(p1x - p0x)
                                            h = abs(p1y - p0y)
#                                            km.append(KMT.PolygoneLine(polygone=points, layer='F.SilkS', width=ow))#,hide = True not working with lines
                                            extraPads.append(KMT.Pad(type=KMT.Pad.TYPE_NPTH,
                                                         shape=KMT.Pad.SHAPE_ROUNDRECT,
                                                         at=[cx/zunit,scaleY*cy/zunit],
                                                         size = [w/zunit,h/zunit],
                                                         layers=[klay],
                                                         round_radius_exact=r,
                                                         number=pName,
                                                         drill = [w/zunit,h/zunit]
#                                                         zone_connect = KMT.Pad.CONNECT_SOLID
                                                        ))
                                            #Use tarc corner geometry
                                        elif klay in ['F.CrtYd','B.CrtYd']:
                                            km.append(KMT.PolygoneLine(polygone = points, width = ow, layer = klay))
                                        else:
                                            mlays = klay
                                            if not type(mlays) is list:
                                                mlays = [mlays]
                                            for mlay in mlays:
                                                km.append(KMT.Polygon(polygone = points, width = ow, layer = mlay))
                                    else:
                                        km.append(KMT.PolygoneLine(polygone = points, width = ow, layer = klay))

                            for arc in arcs:
                                for subarc in arc:
                                    if not 'MIRECMS' in footprint_name:
                                        cpt = subarc.cpt
                                        spt = subarc.spt
                                        km.append(KMT.Arc(center = [cpt.x/zunit, scaleY*cpt.y/zunit], start = [spt.x/zunit, scaleY*spt.y/zunit], angle = scaleY*subarc.dalpha, width = ow, layer = klay))
                                    else:
                                        resolution = 0.1*zunit
                                        cpt = subarc.cpt
                                        spt = subarc.spt
                                        radius = math.sqrt(math.pow(spt.x - cpt.x, 2) + math.pow(spt.y - cpt.y,2))
                                        subdivisions = int(2*math.pi*radius/resolution)
                                        subdivisions = max(subdivisions,3)
                                        points = []
                                        dalpha = subarc.dalpha*math.pi/180
                                        for s in range(subdivisions+1):
                                            x = cpt.x+radius*math.cos(s*dalpha/subdivisions)
                                            y = cpt.y+radius*math.sin(s*dalpha/subdivisions)
                                            points.append([x/zunit,scaleY*y/zunit])
                                        if klay in ['F.CrtYd','B.CrtYd']:
                                            km.append(KMT.PolygoneLine(polygone = points, width = ow, layer = klay))
                                        else:
                                            km.append(KMT.Polygon(polygone = points, width = ow, layer = klay))
#                                        print(klay,lname)
                        else:
                            vertex = surface['geometry']['surface']['vertex']
                            linepts = []
                            if 'arc' in vertex.keys():
                                linepts += ProcessArcs(vertex['arc'],0.1,footprint_name)
                            if 'pt' in vertex.keys():
                                linepts += ProcessLines(vertex['pt']).pts

                            linepts = sorted(linepts, key=lambda p: p.o)
                            points = RemoveDuplicates(linepts)
                            points = [[p.x/zunit, scaleY*p.y/zunit] for p in points]
                            if klay in ['F.CrtYd','B.CrtYd']:
                                km.append(KMT.PolygoneLine(polygone = points, width = ow, layer = klay))
                            else:
                                mlays = klay
                                if not type(mlays) is list:
                                    mlays = [mlays]
                                for mlay in mlays:
                                    km.append(KMT.Polygon(polygone = points, width = ow, layer = mlay))

    # Analyze toeprint
    tp = fp['toeprint']
    if 'pin' in tp.keys():
        isSMD = True
        pins = tp['pin']
        if not type(pins) is list:
            pins = [pins]
        for pin in pins:
            # Sanity check
            if list(pin.keys()) != ['name','pt','minRect','layout', 'o']:
                print('unrecognized pin keys',pin)
                sys.exit()
            if list(pin['layout'].keys()) != ['layer', 'o']:
                print('unrecognized pin layout keys',pin)
                sys.exit()
            if list(pin['layout']['layer'].keys()) != ['name','fpadstack', 'o']:
                print('unrecognized pin layer keys',pin)
                sys.exit()
            if pin['layout']['layer']['name'] != 'FPADSTACK':
                print('unrecognized pin stack',pin)
                sys.exit()
            fpad = pin['layout']['layer']['fpadstack']
            possibleKeys = ['pt','padstackGroup','angle','o']
            for k in fpad.keys():
                if not k in possibleKeys:
                    print('wrong fpad keys',k)
                    print(sys.exit())
            psg = fpad['padstackGroup']
            if list(psg.keys())!= ['type','padstack','o']:
                print('wrong pstg keys',psg)
                sys.exit()
            if psg['type'] != 'default':
                print('wrong psg type',psg)
                sys.exit()
            pName = pin['name']
            if footprint_name == 'AWM3300V_BT-BLINDAGE' and pName in ['4','5','6','7']:
                continue # We skip it because it should go in BL1 and BL3 caps footprints

            angle = fpad['angle'] if 'angle' in fpad.keys() else 0
            pos = ProcessPoint(pin)
            pos2 = ProcessPoint(fpad)
            psname = psg['padstack']
            if pos.x != pos2.x or pos.y != pos2.y:
                print('mismatch pinpos, using pos2',pos,pos2)
                pos = pos2
            if not os.path.exists(os.path.join(libPath,psname+".kicad_mod")):
                print('Non found padstack',psname)
                sys.exit()

            origin = [pos.x/zunit, pos.y*scaleY/zunit]
            # Load padstack FP
            pstdict = padstackDict[psname]
            if type(pstdict) is PadStack:
                if pstdict.drillRadius > 0:
                    AddViaToModel     (footprint_name, pName, padDict, origin, angle, pstdict, km)
                    isSMD = False
                else:
                    AddPadstackToModel(footprint_name, pName, padDict, origin, angle, pstdict, km, customSurfPad)
            else:
                print('Missing pstdict',psname, pstdict)
                sys.exit()
            if footprint_name == 'TB-CMS-AIROX' and pName == '1' and fusion:# padstack added manually as an extra pad
                ap = pstdict.allPads
                al = [a.lay for a in ap]
                sp = ap[al.index('Comp_resist')]
                pad = padDict[sp.connect]
                radius = pad.circs[0].r
                diam = radius * 2 / zunit
                extraPads.append(KMT.Pad(type=KMT.Pad.TYPE_SMT,\
                             shape=KMT.Pad.SHAPE_CIRCLE,\
                             at=[pos.x/zunit,scaleY*pos.y/zunit],\
                             size=[diam,diam],\
                             layers=['F.Mask'],\
                             number='', rotation=angle,
#                             clearance = max(clm,clm2), solder_mask_margin = smm2, thermal_width = thWidth2, thermal_gap = thGap2,
#                             drill = drill, zone_connect = zc
                             ))
        if isSMD:
            # Mark part as SMD so that it appears in the PNP files
            km.setAttribute('smd')
    else:
        km.setAttribute('virtual')

    # Add extra pads after! the normal pads, to not mess up with reconnect dicts
    for ep in extraPads:
        km.append(ep)

    # Footprint variations when same footprint but different 3D model
    variations = ['']

    if footprint_name == 'FIX35':
        variations.append('_display')

    for variation in variations:
        # add 3d model
        toRemove = []
        if not map3d is None:
            if footprint_name in map3d:
                models = map3d[footprint_name+variation]
                if not type(models) is list:
                    models = [models]
                for model in models:
                    kmodel = KMT.Model(filename=model['model']
                                      ,offset=model['offset'] if 'offset' in model.keys() else [0,0,0]
                                      ,scale=model['scale'] if 'scale' in model.keys() else [1,1,1]
                                      ,rotate=model['rotate'] if 'rotate' in model.keys() else [0,0,0])
                    km.append(kmodel)
                    toRemove.append(kmodel)

        # write file
        file_handler = KMT.KicadFileHandler(km)
        file_handler.writeFile(os.path.join(libPath,footprint_name+variation+'.kicad_mod'),timestamp=0)

        # remove for next variation
        for rm in toRemove:
            km.remove(rm)

    zunit = oldzunit

class Text(NamedTuple):
    """
    A Zuken Text object
    :param: position the Point2 position in pt
    :param: size the text size (per letter) as Point2 in pt
    :param: thickness the text stroke thickness in pt
    :param: justify the justify settings from Zuken (LO_C, LO_R, LO_L)
    :param: angle the text angle in deg
    :param: flip bool if it is in the back of the board
    """
    string : str
    position: Point2
    size: Point2
    thickness: int
    justify: str
    angle: int
    flip: str
    font: int


def ProcessText(text, field='geometry'):
    """
    Process Zuken JSON text and return Text object
    :param: text the JSON dict
    """
    t = text[field]['text']
    string = t['string']
    position = Point2(t['pt']['x'],t['pt']['y'])
    size = Point2(t['width'],t['height'])
    thickness = t['strokeWidth']
    angle = t['textAngle']
    flip  = t['flip']
    justify = t['justify']
    direction = t['dir']
    reverse = t['reverse']
    if direction != 'LtoR':
        print('Warning: unsupported text direction',direction)
    if reverse != 'OFF':
        print('Warning: unsupported reverse',reverse)
    font = t['font']

    return Text(string,position,size,thickness,justify,angle,flip,font)


def ProcessNetName(name, powerNets):
    """
    We append this "/" to make it match with net labels from EEschema in KiCad
    Previously, if there was a "/" at end of name, we change it to KiCad negative format enclosed with ~
    :param: name the original Zuken net name
    :param: the subset of Zuken net names that are power nets
    """
    if name == '':
        return name # Empty net has netcode zero and means no connection
    # French to English translation
    if name == 'TOUCHE-1':
        name = 'KEY-1'
    elif name == 'TOUCHE-2':
        name = 'KEY-2'
    elif name == 'MES_FIO2':
        name = 'FIO2-MEAS'
    elif name == 'CON-MES-FIO2':
        name = 'CON-FIO2-MEAS'
    elif name == 'FREIN':
        name = 'BRAKE'
    elif name == 'MES-VIT':
        name = 'SPEED_MEASURE'
    elif name == 'VAL-O2':
        name = 'O2_VALVE'
    elif name == 'INHIB-ARRET-INV':
        name = 'INHIB-STOP-INV'
    elif name == 'ARRET-INV':
        name = 'INV-STOP'
    elif name == 'VALVE-BALLONNET':
        name = 'EXHALATION-VALVE'
    elif name == 'LED_ETAT_BAT':
        name = 'LED_BAT'
    elif name == 'INHIB-ALARME':
        name = 'ALARM-INHIB'
    elif name == 'MES-Q-O2':
        name = 'Q-O2-MEAS'
    elif name == 'MES_Q_I':
        name = 'QI-MEAS'
    elif name == 'BUZ-SEC':
        name = 'SEC-BUZ'
    elif name == 'V-PILE':
        name = 'V-BATTERY'
    elif name == 'GND-PUIS':
        name = 'PGND'
    if name.endswith('/'):
        name = '~' + name.rstrip('/') + '~' # Convert to Kicad eeschema style, because gui editor does not accept / in the net name, to keep the Negative meaning
    elif name.startswith('/'):
        name = '~' + name.lstrip('/') + '~' # Convert to Kicad eeschema style, because gui editor does not accept / in the net name, to keep the Negative meaning
    if name.startswith('CE/'): # Chip enable select
        name = '~CE~' + name[3:]
    if not name in powerNets: # we exclude power symbols, because they are written differently in KiCad than net labels
        nname = '/' + name # Add the / in the beginning because it is the net flag
    else:
        nname = name # For GND, KiCAD keeps it without /

    return nname


def ProcessArcsLines(lines, trace='', closeOutline=False):
    """
    Processes a Zuken line JSON object composed of segments and arcs
    :param: lines the lines json object
    :param: trace some debug trace
    :param: closeOutline close outline of the surface
    """

    arcElem = []
    lineElem = []

    if not type(lines) is list:
        lines = [lines]
    # Sanity check
    for line in lines:
        if list(line.keys()) != ['vertex','o'] and list(line.keys()) != ['outlineWidth', 'fillWidth', 'fillAngle', 'vertex', 'o'] and list(line.keys()) != ['outlineWidth', 'fillWidth', 'fillAngle', 'vertex', 'openShape', 'o'] and list(line.keys())!=['type','dashLine','vertex','o']:
            print('Warning: wrong li keys',line.keys())
#            return arcElem, lineElem
            sys.exit()
        if 'openShape' in line.keys():
            print('Warning: openShape not yet supported')

        vKeys = line['vertex'].keys()
        # Sanity check
        possibleKeys = ['arc', 'pt', 'o']
        for k in vKeys:
            if not k in possibleKeys:
                print('wrong line keys',k)
                print(sys.exit())
        if not 'arc' in vKeys and not 'pt' in vKeys:
            print('Bad line',vKeys)
            sys.exit()

        vertex = line['vertex']
        arcs = []
        linepts = []
        if 'arc' in vKeys:
            arcs = ProcessArcs(vertex['arc'],-1,trace)
        if 'pt' in vKeys:
            linepts = ProcessLines(vertex['pt']).pts

        points = arcs + linepts
        points = sorted(points, key=lambda p: p.o)# Sort resulting polygon by original order ID
        arcs, lines = DisentangleArcsLines(points,True,closeOutline)

        arcElem.append(arcs)
        lineElem.append(lines)

    return arcElem, lineElem


def GenerateHoleSheet(viaDict, filename):
    """
    Generates a KiCad schematic scheet with the vias
    :param: viaDict via numbers grouped  by net
    :param: filename where to store it
    """

    # First write dummy schematic file
    dumName = '/tmp/dummy.sch'
    dummy = [
            'EESchema Schematic File Version 4',
            'EELAYER 30 0',
            'EELAYER END',
            '$Descr A2 23386 16535',
            'encoding utf-8',
            'Sheet 1 1',
            'Title ""',
            'Date ""',
            'Rev ""',
            'Comp ""',
            'Comment1 ""',
            'Comment2 ""',
            'Comment3 ""',
            'Comment4 ""',
            '$EndDescr',
            "$EndSCHEMATC"
            ]
    with open(dumName,'w') as dum:
        dum.writelines('\n'.join(dummy))

    # Open dummy file and start adding vias
    sh = sch.Schematic(dumName)

    oX = 750
    oY = 750
    nX = 1000
    dX = 500
    sep = 250
    maxCols = 5
    sRows = 2
    posX = 0
    posY = 0
    maxY = 15000
    for net, vias in viaDict.items():

        net = net.lstrip('/')

        # ~ # Add net label
        # ~ desc = 'Text Label ' + str(oX + dX + posX) + ' ' + str(oY + posY) + ' 0 50 ~ 0'
        # ~ lab = {'desc': desc, 'data': net}
        # ~ sh.texts.append(lab)

        # Add hierarchical net label
        desc = 'Text HLabel ' + str(oX + dX + posX) + ' ' + str(oY + posY) + ' 2 50 Input ~ 0'
        lab = {'desc': desc, 'data': net}
        sh.texts.append(lab)

        # Add global label as link
        desc = 'Text GLabel ' + str(oX + dX + posX) + ' ' + str(oY + posY) + ' 0 50 Input ~ 0'
        lab = {'desc': desc, 'data': net}
        sh.texts.append(lab)

        # Add vias
        for i, v in enumerate(vias):

            # Add via
            via = sch.Component([])
            via.labels = {'name': 'V_Connector:Via', 'ref': 'VI?'}
            via.unit = {'unit': '1', 'convert': '1', 'time_stamp':hex(int(v[2:]))}
            vX = oX + (i %maxCols)*sep + posX + nX
            vY = oY + (i//maxCols)*sep + posY
            via.position = {'posx': str(vX), 'posy': str(vY)}
            via.addField({'id': '0', 'ref': '"'+v+'"','name': '', 'posx': str(vX), 'posy': str(vY-50), 'attributes':'0000'})
            via.addField({'ref': '"Via"','name': '', 'posx': str(vX), 'posy': str(vY)})
            sh.components.append(via)

            # Add connection
            desc = 'Connection ~ ' + str(vX) + ' ' + str(vY)
            conn = {'desc': desc}
            sh.conns.append(conn)

        # Add horizontal wire from labels to first via
        desc = 'Wire Wire Line'
        startend = str(oX + dX + posX) + ' ' + str(oY + posY) + ' ' + str(oX + posX + nX) + ' ' + str(oY + posY)
        wire = {'desc': desc, 'data': startend}
        sh.wires.append(wire)

        # Add vertical wire
        rows = (len(vias) - 1)//maxCols + 1
        if rows > 1:
            desc = 'Wire Wire Line'
            startend = str(oX + posX + nX) + ' ' + str(oY + posY) + ' ' + str(oX + posX + nX) + ' ' + str(oY + posY + (rows-1)*sep)
            wire = {'desc': desc, 'data': startend}
            sh.wires.append(wire)

        # Add horizontal wires
        for row in range(rows):
            if row == rows - 1:
                vX = oX + ((len(vias)-1)%maxCols)*sep + posX + nX
            else:
                vX = oX + (maxCols-1)*sep + posX + nX
            vY = oY + (row*sep) + posY
            desc = 'Wire Wire Line'
            startend = str(oX + posX + nX) + ' ' + str(vY) + ' ' + str(vX) + ' ' + str(vY)
            wire = {'desc': desc, 'data': startend}
            sh.wires.append(wire)

        posY += (rows-1)*sep + sRows*sep

        if posY > maxY:
            posX += (maxCols-1)*sep + nX + sRows*sep
            posY = 0

    # Save hole sheet
    sh.save(filename)


def CreateThermalKeepouts(vpos,psname,cond,removeCopper,padDict,padstackDict,board,powerNets,netname):
    """
    Workaround to create thermal reliefs in negative Zuken Gerber layers
    :param: vpos the 2D point of the via where to create the keepouts and spokes
    :param: psname the padstack name
    :param: cond the conductive status
    :param: removeCopper thelayers there to remove copper
    :param: padDict a dict of full pads
    :param: padstackDict a dict of padstacks
    :param: board the PCBNew board object
    :param: powerNets the list of powerNets
    :param: netname the name of the net of this negative plane
    """
    cids = [con['id'] for con in cond]
    cidx = cids.index(2)
    con = cond[cidx]
    if 2 in removeCopper:
        if con['status'] != 'CLEARANCE':
            print('Wrong status cl con',con)
            sys.exit()
        clFp = [ps for ps in padstackDict[psname].allPads if ps.lay == 'all_inner'][0].clearance
        pad = padDict[clFp]
        radius = pad.circs[0].r
        points = []
        perimeter = 2*math.pi*radius
        resolution = 0.1*zunit
        cptx = vpos.x
        cpty = vpos.y
        subdivisions = int(perimeter/resolution)
        subdivisions = max(subdivisions,3) # Avoid division by zero later on
        dalpha = 360*math.pi/180
        start_angle = 0
        for s in range(subdivisions+1):
            alpha = start_angle + s/subdivisions*dalpha
            points.append(Point4(cptx+radius*math.cos(alpha),cpty+radius*math.sin(alpha),0,0))
        AddZone(points,board,pcbnew.In1_Cu,board.GetNetcodeFromNetname(ProcessNetName('GND',powerNets)),True,True,True,False, 0, True, 0.0254)
    else:
        if con['status'] != 'THERMAL':
            print('Wrong status th con',con)
            sys.exit()
        thFp = [ps for ps in padstackDict[psname].allPads if ps.lay == 'all_inner'][0].thermal
        pad = padDict[thFp]
        thP = pad.therm[0]
        thWidth = thP.w
        tin     = thP.tin
        tout    = thP.tout
        if thP.n != 4:
            print('Wrong th n',thP.n)
            sys.exit()
        if thP.a != 0:
            print('Wrong th a',thP.n)
            sys.exit()

        # Add keepout that allows tracks but not pour
        radius = tout
        points = []
        perimeter = 2*math.pi*radius
        resolution = 0.1*zunit
        cptx = vpos.x
        cpty = vpos.y
        subdivisions = int(perimeter/resolution)
        subdivisions = max(subdivisions,3) # Avoid division by zero later on
        dalpha = 360*math.pi/180
        start_angle = 0
        for s in range(subdivisions+1):
            alpha = start_angle + s/subdivisions*dalpha
            points.append(Point4(cptx+radius*math.cos(alpha),cpty+radius*math.sin(alpha),0,0))
        AddZone(points,board,pcbnew.In1_Cu,board.GetNetcodeFromNetname(ProcessNetName('GND',powerNets)),True,True,False,False, 0, True, 0.0254)

        # Add spokes
        netcode = board.GetNetcodeFromNetname(ProcessNetName('GND',powerNets))
        spoke = [ Track3(vpos.x + tin, vpos.y, thWidth), Track3(vpos.x + tout, vpos.y, thWidth) ]
        AddTrack(spoke,board,pcbnew.In1_Cu,netcode)
        spoke = [ Track3(vpos.x - tin, vpos.y, thWidth), Track3(vpos.x - tout, vpos.y, thWidth) ]
        AddTrack(spoke,board,pcbnew.In1_Cu,netcode)
        spoke = [ Track3(vpos.x, vpos.y + tin, thWidth), Track3(vpos.x, vpos.y + tout, thWidth) ]
        AddTrack(spoke,board,pcbnew.In1_Cu,netcode)
        spoke = [ Track3(vpos.x, vpos.y - tin, thWidth), Track3(vpos.x, vpos.y - tout, thWidth) ]
        AddTrack(spoke,board,pcbnew.In1_Cu,netcode)


def main(cmd, args):
#if True:
#    cmd, args = (sys.argv[0], sys.argv[1:])
    """
    Main function
    :param: cmd the executed command in terminal
    :param: args the array of arguments
    """

    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Convert Zuken CR-5000 ASCII PCB files to KiCAD')
    parser.add_argument('--path'  , dest='path'  , type=str  , required=True , help='Input directory where Zuken-JSON files are located')
    parser.add_argument('--fname' , dest='fname' , type=str  , required=True , help='Name of the PCB files (without extension)')
    parser.add_argument('--outdir', dest='outdir', type=str  , required=True , help='Output directory')
    parser.add_argument('--name'  , dest='name'  , type=str  , required=False, help='ID of the PCB file, normally as file name (without extension)', default='')
    parser.add_argument('--remap' , dest='remap' , type=str  , required=False, help='Name of INI (json) file defining a remap of some symbol references incompatible with KiCAD', default='')
    parser.add_argument('--map3d' , dest='map3d' , type=str  , required=False, help='Name of INI (json) file mapping the footprints to 3D models', default='')
    parser.add_argument('--power' , dest='power' , type=str  , required=False, help='Name of INI (json) file mapping the net names that are power nets', default='')
    parser.add_argument('--flipY' , dest='flipY' , type=bool , required=False, help='Bool to flip Y coordinate if needed', default=False)
    parser.add_argument('--nVias' , dest='nVias' , type=bool , required=False, help='Bool to number vias with reference', default=False)
    parser.add_argument('--force' , dest='force' , type=bool , required=False, help='Overwrite footprints with info from PCB', default=False)

    args = parser.parse_args(args)

    # TODO!:
#    remap
#    map3d
    # fprints, pads, vias


    # Sanity check about input path
    if not os.path.exists(args.path):
        print('Non-existing input path',args.path)
        sys.exit()
    if not os.path.isdir(args.path):
        print('Not a directory in input path',args.path)
        sys.exit()

    # Sanity check about input files
    extensions = ["brd.json","ftf.json","pcf.json","pro.json"]
    for extension in extensions:
        filename = os.path.join(args.path,args.fname+"."+extension)
        if not os.path.exists(filename):
            print('Non-existing input path',filename)
            sys.exit()
        if not os.path.isdir(args.path):
            print('Not a file in ',filename)
            sys.exit()
    if args.name == '':
        args.name = args.fname

    # Sanity check about output directory
    if args.outdir == args.path:
        args.outdir = args.path
    if not os.path.exists(args.outdir):
        print('Non-existing output directory',args.outdir)
        sys.exit()

    global scaleY
    if args.flipY:
        scaleY = -1

    # Create KiCAD PCB board object
    board = pcbnew.BOARD()
    board.SetFileName(args.fname)

    # Open .brd.json file
    inFileName = os.path.join(args.path,args.fname+"."+extensions[0])
    print('Processing',inFileName)
    data = json.load(open(inFileName))

    # Set the board thickness
    boardThickness = data['.BOARD_OUTLINE ECAD']['thickness']
    board.GetDesignSettings().SetBoardThickness(pcbnew.FromMM(boardThickness))
    board.GetDesignSettings().m_SolderMaskMinWidth=0 # Do not add mask rounding stuff https://forum.kicad.info/t/solder-mask-too-large-in-gerbers-and-oshpark-but-screen-ok/18798
    if args.fname == 'powersupply-c':
        board.GetNetClasses().GetDefault().SetClearance(pcbnew.FromMM(0.1)) # This is needed, otherwise net tie region does not fill well: https://gitlab.com/kicad/code/kicad/-/issues/4483

    # Add graphic outlines to the PCB
    AddGraphicOutline(data['.BOARD_OUTLINE ECAD'],board,pcbnew.Margin)
    AddGraphicOutline(data['.OTHER_OUTLINE ECAD'],board,pcbnew.Margin)
    AddGraphicOutline(data['.ROUTE_OUTLINE ECAD'],board,pcbnew.Eco1_User)
    AddGraphicOutline(data['.PLACE_OUTLINE ECAD'],board,pcbnew.Eco2_User)
    if '.ROUTE_KEEPOUT ECAD' in data.keys():
        AddGraphicOutline(data['.ROUTE_KEEPOUT ECAD'],board,pcbnew.Dwgs_User,True)
    if '.PLACE_KEEPOUT ECAD' in data.keys():
        AddGraphicOutline(data['.PLACE_KEEPOUT ECAD'],board,pcbnew.Dwgs_User,True)

    # Add drilled holes to the PCB
    AddGraphicHoles(data['.DRILLED_HOLES']['hole'],board,pcbnew.Cmts_User)

    # Get module placement data
    moduleData = data['.PLACEMENT']['module']

    print('Finished processing',inFileName)


    # Open .pro.json file
    inFileName = os.path.join(args.path,args.fname+"."+extensions[3])
    print('Processing',inFileName)
    data = json.load(open(inFileName))

    AddGraphicModules(moduleData,data['.ELECTRICAL'],board,pcbnew.Dwgs_User)

    print('Finished processing',inFileName)


    # Open .ftf.json file
    inFileName = os.path.join(args.path,args.fname+"."+extensions[1])
    print('Processing',inFileName)
    data = json.load(open(inFileName))
    data = data['ftf']

#    dimLineWidth = data['footprintContainer']['commonParameters']['dimensionParameters']['dimLneWidth]

    # Prepare footprint folder
    fpFolder = os.path.join(args.outdir,args.fname+".pretty")
    if not os.path.exists(fpFolder):
        os.makedirs(fpFolder)
        print('Creating footprint folder',fpFolder)
    else:
        print('Using preexisting footprint folder',fpFolder)

    # Get the conversion unit from pcf
    global zunit
    zunit = json.load(open(os.path.join(args.path,args.fname+"."+extensions[2])))['pcf']['parameter']['basePointSize']

    # Analyze pads
    pads = data['footprintContainer']['pads']['pad']
    padDict = {}
    for pad in pads:

        # Sanity check
        mandKeys = ['name', 'uver', 'cuser', 'uuser', 'ctime', 'utime', 'area', 'grid', 'photo', 'panelUse', 'geometry', 'o' ]
        for k in mandKeys:
            if not k in pad:
                print('missing pad key',k)
                print(sys.exit())
        if list(pad['area'].keys()) != ['box','o']:
            print('Wrong area',pad['area'].keys())
            sys.exit()

        box = ProcessBox(pad['area']['box'])

        padname = pad['name']
        geom = pad['geometry']

        # Sanity check
        possibleKeys = ['oblong','line','rectangle','circle','roundThermal','squareThermal','surface','donut','o']
        for k in geom.keys():
            if not k in possibleKeys:
                print('wrong geom keys',k,geom.keys())
                print(sys.exit())

        # Analyze lines
        lineElem = []
        if 'line' in geom.keys():
            lines = geom['line']
            if not type(lines) is list:
                lines = [lines]
            # Sanity check
            for line in lines:
                if list(line.keys()) != ['vertex','o']:
                    print('Wrong li keys',line.keys())
                    sys.exit()
                vKeys = line['vertex'].keys()
                if not 'arc' in vKeys and not 'pt' in vKeys:
                    print('Bad line',vKeys)
                    sys.exit()
                vertex = line['vertex']
                if 'arc' in vKeys:
                    arcs = vertex['arc']
                    lineElem.append(ProcessArcs(arcs,-1))
                if 'pt' in vKeys:
                    pts = vertex['pt']
                    lineElem.append(ProcessLines(pts))

        # Analyze rectangles
        rectElem = []
        if 'rectangle' in geom.keys():
            rectElem = ProcessRectangles(geom['rectangle'])
#            print(padname,rectElem)

        # Analyze circles
        circElem = []
        if 'circle' in geom.keys():
            circElem = ProcessCircles(geom['circle'])
#            print(padname,circElem)

        # Analyze thermals
        thElem = []
        if 'roundThermal' in geom.keys():
            thElem+= ProcessThermals( True,geom['roundThermal' ])
        if 'squareThermal' in geom.keys():
            thElem+= ProcessThermals(False,geom['squareThermal'])

        # Analyze oblongs
        obElem = []
        if 'oblong' in geom.keys():
            obElem = ProcessOblongs(geom['oblong'])

        doElem = []
        if 'donut' in geom.keys():
            doElem = ProcessDonuts(geom['donut'])

        surElem = []
        if 'surface' in geom.keys():
            surElem = ProcessSurfaces(geom['surface'])

        if len(lineElem)==0 and len(rectElem)==0 and len(circElem)==0 and len(thElem)==0 and len(obElem)==0 and len(doElem)==0 and len(surElem)==0:
            print('missing pad elements',geom)
            sys.exit()

        fullPad = Pad(padname,box,lineElem,rectElem,circElem,thElem,obElem,doElem,surElem)
        padDict[padname] = fullPad
        CreatePadFootprint(fpFolder,padname,padDict,padname)

    # Analyze padstacks
    padstacks = data['footprintContainer']['padstacks']['padstack']
    padstackDict = {}
    for ps in padstacks:
        psname = ps['name']
        pstype = ps['type']
        psmode = ps['throughMode']
        box = ProcessBox(ps['area']['box'])
        # Analyze padSets
        if not 'padSet' in ps.keys():
            if not psname in padDict.keys():
                # Sanity-checks
                mandKeys = ['name', 'uver', 'cuser', 'uuser', 'ctime', 'utime', 'area', 'type', 'throughMode', 'panelUse', 'o' ]
                if pstype!='NONPLATED':
                    print('Warning: wrong pstype',pstype)
                    continue
                if psmode!='NONTHROUGH':
                    print('wrong pmode',psmode)
                    sys.exit()
                if list(ps.keys()) != mandKeys:
                    print('missing pad keys',ps.keys())
                    print(sys.exit())
                if list(ps['area'].keys()) != ['box','o']:
                    print('Wrong parea',ps['area'].keys())
                    sys.exit()
                # Create footprint courtyard for it
                pcname = CreatePadstackFootprint(fpFolder,psname,'',box,[],padDict)
                padstackDict[psname] = pcname
            else:
                print('Note: padStack already fp as pad',psname)
            continue
        pads = ps['padSet']
        if not type(pads) is list:
            pads = [pads]

        # Create an array of pads present converted to Pad6
        allPads = []
        for pad in pads:
            play = pad['footLayer']
            passoc = ['','','','']
            for k, ptag in enumerate(['connect','noconnect','thermal','clearance']):
                if ptag in pad.keys():
                    passoc[k] = pad[ptag]['pad']
            allPads.append(Pad6(psname,play,passoc[0],passoc[1],passoc[2],passoc[3]))

        if not 'hole' in ps.keys():
            if pstype!='NONPLATED':
                print('Warning: wrong pstype',pstype)
                continue
            if psmode!='NONTHROUGH':
                print('wrong psmode',psmode)
                sys.exit()
            pcname = CreatePadstackFootprint(fpFolder,psname,play,box,allPads,padDict)
            padstackDict[psname] = pcname
        else:
            # Sanity-check
            if psmode!='THROUGH':
                print('wrong psmode',psmode)
                sys.exit()

            if 'circle' in ps['hole']['geometry'].keys():
                circles = ProcessCircles(ps['hole']['geometry']['circle'])
                if len(circles)!=1:
                    print('Unsupported multiholes circles')
                    sys.exit()
                circle = circles[0]
                pcname = CreateViaFootprint(fpFolder, psname, play, box, circle, None, allPads, padDict, pstype)
                padstackDict[psname] = pcname
            elif 'oval' in ps['hole']['geometry'].keys():
                ovals = ProcessOvals(ps['hole']['geometry']['oval'])
                if len(ovals)!=1:
                    print('Unsupported multiholes ovals')
                    sys.exit()
                oval = ovals[0]
                pcname = CreateViaFootprint(fpFolder, psname, play, box, None, oval, allPads, padDict, pstype)
                padstackDict[psname] = pcname
            else:
                print('Warning: padstack',ps,'not yet implemented')
                sys.exit()

    # Readout 3D map
    if args.map3d != '':
        map3dfile = args.map3d
        if not os.path.exists(map3dfile):
            print('bad map3d file',map3dfile)
            sys.exit()
        map3d = json.load(open(map3dfile))
    else:
        map3d = None

    # Readout power map
    if args.power != '':
        powerfile = args.power
        if not os.path.exists(powerfile):
            print('bad power file',powerfile)
            sys.exit()
        powerNets = json.load(open(powerfile))['nets']
    else:
        powerNets = []
    # Check for additional global labels and append to power map
    if args.remap != '':
        remapName = os.path.join(args.path,args.remap)
        remap = json.load(open(remapName))
        if 'labels' in remap.keys():
            powerNets += remap['labels']

    # Analyze footprints
    footprints = data['footprintContainer']['footprints']['footprint']
    fpDict = {}
    conductorLayers = json.load(open(os.path.join(args.path,args.fname+"."+extensions[2])))['pcf']['technologyContainer']['technology']['numberOfConductorLayer']# Sniff data from pcf file
    # update layerMap
    for i in range((conductorLayers-2)//2):
        layerDict['Assem_(MISC)'].append('In'+str(2*i+1)+'.Cu')
        layerDict['Assem_(MISC)_M'].append('In'+str(2*i+2)+'.Cu')

    for fp in footprints:
        if fp['name'] == 'DPAK' and args.fname == 'GR106772-A':
            fp['name'] += '_cpu' # to avoid conflict with other DPAKs in other boards with different dimensions
        fpDict[fp['name']] = fp
        ProcessFootprint(fpFolder,fp, padDict, padstackDict, 1, map3d, conductorLayers)

    print('Finished processing',inFileName)

    # Open .pcf.json file
    inFileName = os.path.join(args.path,args.fname+"."+extensions[2])
    print('Processing',inFileName)
    data = json.load(open(inFileName))
    data = data['pcf']

    # Get the conversion unit
    zunit = data['parameter']['basePointSize']

    # Set the number of conductor layers
    conductorLayers = data['technologyContainer']['technology']['numberOfConductorLayer']
    board.GetDesignSettings().SetCopperLayerCount(conductorLayers)
    board.GetDesignSettings().SetVisibleLayers(pcbnew.LSET_AllCuMask(conductorLayers))

    if conductorLayers < 2:
        print("error layers")
        sys.exit()
    layerMap = [pcbnew.F_Cu]#0
    for i in range(conductorLayers-2):
        layerMap.append(pcbnew.F_Cu+i+1)
    layerMap.append(pcbnew.B_Cu)
#    print(layerMap)

    # Set the nets
    nets = data['boardContainer']['nets']['net']
    for net in nets:
        nname = ProcessNetName(net['name'],powerNets)
        n = pcbnew.NETINFO_ITEM(board,nname)
        n.SetLocked(net['lockMode']=='FIXED')
        board.Add(n)#https://forum.kicad.info/t/creating-a-net-in-python/2261/3 https://forum.kicad.info/t/loading-netlist-with-python-script/19507/2
#    try:
#        board.GetNetcodeFromNetname(ProcessNetName('',powerNets))
#    except IndexError as e:
#        # Add empty netname for unconnected pads
#        nname = ProcessNetName('',powerNets)
#        n = pcbnew.NETINFO_ITEM(board,nname)
#        n.SetLocked(True)
#        board.Add(n)

    # Check layer types:
    ltypeMap = {}
    flayers = data['technologyContainer']['technology']['footprintLayer']['layer']
    for fl in flayers:
        ltypeMap[fl['name']] = fl['type']

    # Analyze layers
    layers = data['boardContainer']['boardLayout']['layout']['layer']

    # Set the board outline in the Edge.Cuts layer
    layouts = [l for l in layers if 'systemLayer' in l.keys() if l['systemLayer']['type']=='BOARD_FIGURE']
    if len(layouts)!=1 or len(layouts[0].keys())!=3 or not 'surface' in layouts[0].keys():
        print('Bad layouts',layouts)
        sys.exit()
    if not 'openShape' in layouts[0]['surface']['geometry']['surface'].keys() and not 'pt' in layouts[0]['surface']['geometry']['surface']['vertex'].keys():
        print('Missing layout',layouts[0]['surface'])
        sys.exit()
    if 'pt' in layouts[0]['surface']['geometry']['surface']['vertex'].keys():
        layout = layouts[0]['surface']['geometry']['surface']['vertex']['pt']
        outlineWidth = layouts[0]['surface']['geometry']['surface']['outlineWidth']
        if len(layout)<3:
            print('Bad layout',layout)
            sys.exit()
        pts = [Point2(p['x'],p['y']) for p in layout]
        if pts[0] != pts[-1]:
            pts.append(pts[0])#Close polygon
        pts = RemoveDuplicates(pts)
        AddOutline(pts,board,pcbnew.Edge_Cuts,False,outlineWidth)
    if 'openShape' in layouts[0]['surface']['geometry']['surface'].keys():
        layout = layouts[0]['surface']['geometry']['surface']['openShape']['vertex']
        outlineWidth = layouts[0]['surface']['geometry']['surface']['openShape']['outlineWidth']
        pt = layout['pt']
        # Sanity check
        if not all(p['tarc']['name'] == 'ON' for p in pt):
            print('Warning: wrong tarc status',pt)
        if not all(p['tarc']['r'] == pt[0]['tarc']['r'] for p in pt):
            print('Warning: wrong tarc radius',pt)
        radius = pt[0]['tarc']['r']
        for j in range(len(pt)):# we start at 0 to get j-1 from last list element, to close outline
            # Add line and arc
            p0 = Point2(pt[j-1]['x'],pt[j-1]['y'])
            p1 = Point2(pt[j  ]['x'],pt[j  ]['y'])
            dx = p1.x - p0.x
            dy = p1.y - p0.y
            d = math.sqrt(pow(dx,2)+pow(dy,2))
            ux = dx/d
            uy = dy/d
            p0x = p0.x + ux*radius
            p0y = p0.y + uy*radius
            p1x = p1.x - ux*radius
            p1y = p1.y - uy*radius
            pc0 = Point2(p0x,p0y)
            pc1 = Point2(p1x,p1y)
            AddOutline([pc0,pc1],board,pcbnew.Edge_Cuts,False,outlineWidth)
            vx = -uy
            vy = ux
            cptx = p1x + vx*radius
            cpty = p1y + vy*radius
            cpt = Point5(0,cptx,cpty,0)
            spt = Point5(0,p1x,p1y,90)
            AddOutline([cpt,spt],board,pcbnew.Edge_Cuts,False,outlineWidth)

    # Set the conducting zones
    clayers = [l for l in layers if 'type' in l.keys() if l['type']=='conductive']
    clids = [int(l['name']) for l in clayers]
    negatives = []
    if len(clayers)!=conductorLayers:
        if conductorLayers%2 == 0 and abs(conductorLayers-len(clayers))==1:
            for i in range(conductorLayers):
                cid = i+1
                if not cid in clids:
                    if cid>1 and cid<conductorLayers:
                        negatives.append('interne'+str(cid))
                    elif cid == 1:
                        negatives.append('Component')
                    elif cid == conductorLayers:
                        negatives.append('Solder')
            print('Note: one negative GND plane layer detected:',negatives[0],'out of',conductorLayers)
        else:
            print('Error, missing layers',len(clayers),conductorLayers)
            sys.exit()


#    allVertices = {}
    for i, layer in enumerate(clayers):

        lid = int(layer['name'])
#        allVertices[lid] = {}

        # Add tracks
        lines = layer['line']
        if not type(lines) is list:
            lines = [lines]
        for j, line in enumerate(lines):
            klay = layerMap[lid-1]
            netname = ProcessNetName(line['net'] if 'net' in line.keys() else '',powerNets)
            netcode = board.GetNetcodeFromNetname(netname)
#            thermalData = line['thermalData'] if 'thermalData' in line.keys() else ''
#            if not netcode in allVertices[lid].keys():
#                allVertices[lid][netcode] = []

            # Analyze vertex
            vertex = line['geometry']['line']['vertex']

            # Sanity check
            possibleKeys = ['pt', 'arc', 'o']
            for k in vertex.keys():
                if not k in possibleKeys:
                    print('wrong track keys',k,vertex)
                    print(sys.exit())

            if 'pt' in vertex.keys():
                pts = line['geometry']['line']['vertex']['pt']
                if not type(pts) is list:
                    pts = [pts]
                points = [ Track3(pt['x'],pt['y'],pt['width']) for pt in pts ]
                AddTrack(points,board,klay,netcode)
#                for pt in pts:
#                    allVertices[lid][netcode].append(Point2(pt['x'],pt['y']))

#            https://hackaday.io/project/167914-kicad-rf-tools
            if 'arc' in vertex.keys():
                arcs = vertex['arc']
                if not type(arcs) is list:
                    arcs = [arcs]
                pts = ProcessArcs(arcs)# sampling needed because circular tracks are not supported
                points = [ Track3(pt.x,pt.y,pt.w) for pt in pts ]
                AddTrack(points,board,klay,netcode)
#                for pt in pts:
#                    allVertices[lid][netcode].append(Point2(pt.x,pt.y))


        # Add texts
        if 'text' in layer.keys():
            texts = layer['text']
            if not type(texts) is list:
                texts = [texts]
            for text in texts:
                tobj = ProcessText(text)
                AddText(board,klay,tobj)

        # Add zones
        surfaces = layer['surface']
        if not type(surfaces) is list:
            surfaces = [surfaces]
        for surface in surfaces:
            netname = ProcessNetName(surface['net'],powerNets)
            netcode = board.GetNetcodeFromNetname(netname)
            zone = surface['geometry']['surface']

            # Analyze vertex
            vertex = zone['vertex']

            # Sanity check
            possibleKeys = ['arc', 'pt', 'o']
            for k in vertex.keys():
                if not k in possibleKeys:
                    print('wrong zone keys',k)
                    print(sys.exit())

            points = []

            if 'pt' in vertex.keys():
                pts = vertex['pt']
                if not type(pts) is list:
                    pts = [pts]
                points += [ Point4(pt['x'],pt['y'],pt['o']) for pt in pts ]
            if 'arc' in vertex.keys():
                arcs = vertex['arc']
                if not type(arcs) is list:
                    arcs = [arcs]
                points += ProcessArcs(arcs)# sampling needed because circular zones are not supported
            # Sort resulting polygon by original order ID and close polygon before adding to zone
            if len(points) < 2:
                print('Warning: no valid shapes found')
                continue
            points = sorted(points, key=lambda p: p.o)
            if points[0] != points[-1]:
                points.append( Point4(points[0].x, points[0].y, points[-1].o))#Close polygon
            points = RemoveDuplicates(points)
            AddZone(points,board,klay,netcode,False,False,False,False,0,True,0.0254)

            if 'openShape' in zone.keys():
                ops = zone['openShape']
                if not type(ops) is list:
                    ops = [ops]
                for op in ops:
                    vertex = op['vertex']
                    points = []
                    if 'pt' in vertex.keys():
                        pts = vertex['pt']
                        if not type(pts) is list:
                            pts = [pts]
                        points += [ Point4(pt['x'],pt['y'],pt['o']) for pt in pts ]
                    if 'arc' in vertex.keys():
                        arcs = vertex['arc']
                        if not type(arcs) is list:
                            arcs = [arcs]
                        points += ProcessArcs(arcs)# sampling needed because circular zones are not supported
                    # Sort resulting polygon by original order ID and close polygon before adding to zone
                    points = sorted(points, key=lambda p: p.o)
                    if len(points)<3:
                        print('Warning no valid points found in this openShape')
                        continue
                    if points[0] != points[-1]:
                        points.append( Point4(points[0].x, points[0].y, points[-1].o))#Close polygon
                    points = RemoveDuplicates(points)
                    AddZone(points,board,klay,netcode,True,True,False,False,0,True,0.0254)

    # Add the VIAs (padstacks)
    viaRef = 1 if args.nVias else -1
    vlayer = [l for l in layers if 'systemLayer' in l.keys() if 'type' in l['systemLayer'] if l['systemLayer']['type']=='PADSTACK']
    if len(vlayer)==0:
        print('Warning: no vias were found')
    elif len(vlayer)>1:
        print('Too many vias')
        sys.exit()
    else:
        vlayer = vlayer[0]
        padstacks = vlayer['padstack']
        for padstack in padstacks:
            pkeys = padstack.keys()
            psname = padstack['name']
            netname = ProcessNetName(padstack['net'] if 'net' in padstack.keys() else '',powerNets)
            netcode = board.GetNetcodeFromNetname(netname)

            # Sanity-check
            if not 'pt' in pkeys or not 'fromTo' in pkeys or not 'conductive' in pkeys:
                print('vkeys',padstack,pkeys)
                sys.exit()

            pt = padstack['pt']
            point = Point4(pt['x'],pt['y'],pt['o'])

            # Sanity checks:
            fromTo = padstack['fromTo']
            if fromTo['from'] != 1 or fromTo['To']!=conductorLayers:
                print('wrong fromTo',fromTo)
                sys.exit()
            cond = padstack['conductive']['layerNumber']
            if len(cond) != conductorLayers:
                print('wrong cond',cond)
                sys.exit()
            removeCopper = []
            for con in cond:
                if con['id']<1 or con['id']>conductorLayers:
                    print('wrong id',con)
                    sys.exit()
                if con['status']=='NOCONNECT' or con['status']=='CLEARANCE':
                    removeCopper.append(int(con['id']))

            if len(removeCopper) == 0:
                AddViaFootprint(fpFolder, args.fname, psname, '', Point2(point.x,point.y), board, pcbnew.F_Cu, netcode, conductorLayers, [], viaRef)
                print('VI'+str(viaRef))
                if viaRef>0:
                    viaRef+=1
            else:
                vpos = Point2(point.x,point.y)
                # Verify if a track ends on this via, make it stick and add a connection even if NOCONNECT in the pcf, there is a connection in the Gerber
                removeCopper2 = []
                for rm in range(len(removeCopper)):
#                    if removeCopper[rm] in allVertices.keys() and netcode in allVertices[removeCopper[rm]].keys():
#                        if any(v == vpos for v in allVertices[removeCopper[rm]][netcode]):
##                            print('Skipping removal of copper on Layer',removeCopper[rm],'for Via',viaRef,'Net',netname)
#                            continue
                    if removeCopper[rm] in [1, conductorLayers] and psname in reconnectDict.keys():
#                        print('Reconnecting copper on Layer',removeCopper[rm],'for Via',viaRef,'Net',netname)
                        continue
                    elif removeCopper[rm] in list(range(2, conductorLayers)) and psname in innerReconnectDict.keys():
#                        print('Reconnecting copper on Layer',removeCopper[rm],'for Via',viaRef,'Net',netname)
                        continue
                    else:
                        removeCopper2.append(removeCopper[rm])
#                    elif removeCopper[rm] in [1, conductorLayers] and psname in reconnectDict.keys():
##                        print('Reconnecting copper on Layer',removeCopper[rm],'for Via',viaRef,'Net',netname)
#                        continue
#                    else:
#                        removeCopper2.append(removeCopper[rm])
                removeCopper = removeCopper2
                subname = '_'
                for rc in range(conductorLayers):
                    subname+= ('0' if (rc+1) in removeCopper else '1')
                AddViaFootprint(fpFolder, args.fname, psname, subname, vpos, board, pcbnew.F_Cu, netcode, conductorLayers, [layerMap[rc-1] for rc in removeCopper], viaRef)
                print('VI'+str(viaRef))
                if viaRef>0:
                    viaRef+=1

            # manual clearance with keepouts because diff clearances in diff layers padstack not supported by KiCad
            if 'interne2' in negatives and args.fname == 'GR106772-A':#dirty hack:
                CreateThermalKeepouts(vpos, psname,cond,removeCopper,padDict,padstackDict,board,powerNets,'GND')

    print('Done with vias')
#    SaveFile(board,args)
#    sys.exit()

    # Add the modules
    components = data['boardContainer']['components']['component']
    for comp in components:
        reference = comp['reference']
        part = comp['part']
        package = comp['package'] if 'package' in comp.keys() else None
        footprint = comp['footprint']
        if footprint == 'DPAK' and args.fname == 'GR106772-A':
            footprint += '_cpu' # to avoid conflict with other DPAKs in other boards with different dimensions
        footprint_name = footprint

        if 'outOfBoard' in comp.keys():
            outOfBoard = (comp['outOfBoard'] == 'YES')
        else:
            outOfBoard = False
        position = ProcessPoint(comp['location'])
        outposition = ProcessPoint(comp['outOfBoardLocation']) if 'outOfBoardLocation' in comp.keys() else position
        side = comp['placementSide']
        placed = (comp['placed'] == 'YES')
        lock = (comp['locationLock'] == 'YES')
        angle = comp['angle']
        print(reference,footprint,side,angle)
        if 'propertyF' in comp.keys():
            fname = comp['propertyF']['name']
            if fname!='Scale':
                print('Unsupported propertyF',comp['propertyF'],fname)
                sys.exit()
            scale = comp['propertyF']['value']
            ProcessFootprint(fpFolder,fpDict[footprint],padDict, padstackDict,scale,map3d, conductorLayers)
            footprint += "_" + str(scale)
        else:
            scale = 1
        if 'pin' in comp.keys():
            pins = comp['pin']
            if not type(pins) is list:
                pins = [pins]

            # See if footprint needs to be updated
            mustUpdate = False
            for p, pin in enumerate(pins):
                padstack = pin['layout']['layer']['padstack']
                pName = pin['name']
                # Sanity-check
                lpName = fpDict[footprint]['toeprint']['pin'][p]['name'] if type(fpDict[footprint]['toeprint']['pin']) is list else fpDict[footprint]['toeprint']['pin']['name']
                if pName != lpName:
                    print('Conflict pin padstack',footprint,pName,lpName)
                    sys.exit()
                fpstck = fpDict[footprint]['toeprint']['pin'][p]['layout']['layer']['fpadstack']['padstackGroup']['padstack'] if type(fpDict[footprint]['toeprint']['pin']) is list else fpDict[footprint]['toeprint']['pin']['layout']['layer']['fpadstack']['padstackGroup']['padstack']
                if padstack['name'] != fpstck:
                    print('Warning: conflict padstack',footprint,pName,padstack['name'],fpstck)
                    if args.force:
                        print('overwriting with PCB content')
                        if type(fpDict[footprint]['toeprint']['pin']) is list:
                            fpDict[footprint]['toeprint']['pin'][p]['layout']['layer']['fpadstack']['padstackGroup']['padstack'] = padstack['name']
                        else:
                            fpDict[footprint]['toeprint']['pin']['layout']['layer']['fpadstack']['padstackGroup']['padstack'] = padstack['name']
                        mustUpdate = True
                psname = padstack['name']
                lpinPos = ProcessPoint(fpDict[footprint]['toeprint']['pin'][p] if type(fpDict[footprint]['toeprint']['pin']) is list else fpDict[footprint]['toeprint']['pin'])
                lpinPos = [lpinPos.x, lpinPos.y]
                opinPos = ProcessPoint(padstack)
                pPos = [opinPos.x-position.x,opinPos.y-position.y]
                dangle = -angle*math.pi/180
#                print(angle,padstack['angle'] if 'angle' in padstack.keys() else '')
#                if 'angle' in padstack.keys() and angle != padstack['angle']:
#                    print('Warning diff angle',angle,padstack['angle'])
                sign = 1 if side == 'A' else -1
                pinPos = [sign*round(math.cos(dangle)*pPos[0] - math.sin(dangle)*pPos[1]), round(math.sin(dangle)*pPos[0] + math.cos(dangle)*pPos[1])]
                if int(pinPos[0]) != int(lpinPos[0]) or int(pinPos[1]) != int(lpinPos[1]):
                    print('Warning: conflict pstck pin pos',footprint,pName,lpinPos,pinPos)
                    if args.force:
                        print(lpinPos,pinPos,angle,'overwriting with PCB content')
                        if type(fpDict[footprint_name]['toeprint']['pin']) is list:
    #                        print(fpDict[footprint]['toeprint']['pin'][p]['layout']['layer']['fpadstack']['pt'],fpDict[footprint]['toeprint']['pin'][p]['layout']['layer']['fpadstack']['angle'] if 'angle' in fpDict[footprint]['toeprint']['pin'][p]['layout']['layer']['fpadstack'].keys() else '',pinPos,angle,padstack['angle'] if 'angle' in padstack.keys() else '')
                            fpDict[footprint_name]['toeprint']['pin'][p]['layout']['layer']['fpadstack']['pt']['x'] = pinPos[0]
                            fpDict[footprint_name]['toeprint']['pin'][p]['layout']['layer']['fpadstack']['pt']['y'] = pinPos[1]
                            if 'angle' in padstack.keys():
                                fpDict[footprint_name]['toeprint']['pin'][p]['layout']['layer']['fpadstack']['angle'] = padstack['angle']
                        else:
    #                        print(fpDict[footprint_name]['toeprint']['pin']['layout']['layer']['fpadstack']['pt'],pinPos)
                            fpDict[footprint_name]['toeprint']['pin']['layout']['layer']['fpadstack']['pt']['x'] = pinPos[0]
                            fpDict[footprint_name]['toeprint']['pin']['layout']['layer']['fpadstack']['pt']['y'] = pinPos[1]
                            if 'angle' in padstack.keys():
                                fpDict[footprint_name]['toeprint']['pin']['layout']['layer']['fpadstack']['angle'] = padstack['angle']
                        mustUpdate = True

            # Update footprint if any pin changed wrt to ftf file
            if mustUpdate and args.force:
                ProcessFootprint(fpFolder,fpDict[footprint_name],padDict, padstackDict,1,map3d, conductorLayers)

            # Remove copper of some pin-vias if needed
            subname = ''
            removeCopperDict = {}
            npoffset = 0

            # Look for NPTH pins (pad-holes with empty pad number in KiCad)
            if 'layout' in comp.keys():
                npthlayers = comp['layout']['layer']
                if not type(npthlayers) is list:
                    npthlayers = [npthlayers]
                mtl = [l for l in npthlayers if 'systemLayer' in l.keys() and 'type' in l['systemLayer'].keys() and l['systemLayer']['type'] == 'PADSTACK' ]
                if len(mtl) > 1:
                    print('wrong npth mtl',mtl)
                    sys.exit()
                elif len(mtl) == 1:
                    sl = mtl[0]
                    padstacks = sl['padstack']
                    if not type(padstacks) is list:
                        padstacks = [padstacks]
                    for p, padstack in enumerate(padstacks):
                        npoffset += 1
                        psname = padstack['name']

                        # Sanity checks:
                        fromTo = padstack['fromTo']
                        if not 'To' in fromTo:
                            # SMD pad
                            if fromTo['from'] != 1 and fromTo['from']!=conductorLayers:
                                print('wrong from',fromTo)
                                sys.exit()
                            else:
                                removeCopper = []
                                for rc in range(conductorLayers):
                                    subname+= ('0' if (rc+1) in removeCopper else '1')
                                removeCopperDict[p] = removeCopper
                                continue
                        else:
                            if fromTo['from'] != 1 or fromTo['To']!=conductorLayers:
                                print('wrong fromTo',fromTo)
                                sys.exit()
                            cond = padstack['conductive']['layerNumber']
                            if len(cond) != conductorLayers:
                                print('wrong cond',cond)
                                sys.exit()

                        removeCopper = []
                        for con in cond:
                            if con['id']<1 or con['id']>conductorLayers:
                                print('wrong id',con)
                                sys.exit()
                            if con['status']=='NOCONNECT' or con['status']=='CLEARANCE':
                                removeCopper.append(int(con['id']))
                        if len(removeCopper) != 0:
                            pt = padstack['pt']
                            point = Point4(pt['x'],pt['y'],pt['o'])
                            vpos = Point2(point.x,point.y)
                            # Verify if a track ends on this via, make it stick and add a connection even if NOCONNECT in the pcf, there is a connection in the Gerber
                            removeCopper2 = []
                            netname = ProcessNetName(padstack['net'] if 'net' in padstack.keys() else '',powerNets)
                            netcode = board.GetNetcodeFromNetname(netname)
                            for rm in range(len(removeCopper)):
                                if removeCopper[rm] in [1, conductorLayers] and psname in reconnectDict.keys():
#                                    print('Reconnecting copper on Layer',removeCopper[rm],'for ref',reference,'NPTH',p,'Net',netname)
                                    continue
                                elif removeCopper[rm] in list(range(2, conductorLayers)) and psname in innerReconnectDict.keys():
#                                    print('Reconnecting copper on Layer',removeCopper[rm],'for ref',reference,'NPTH',p,'Net',netname)
                                    continue
                                else:
                                    removeCopper2.append(removeCopper[rm])
                            removeCopper = removeCopper2

                        for rc in range(conductorLayers):
                            subname+= ('0' if (rc+1) in removeCopper else '1')
                        removeCopperDict[p] = removeCopper

                        # manual clearance with keepouts because diff clearances in diff layers padstack not supported by KiCad
                        if 'interne2' in negatives and args.fname == 'GR106772-A':#dirty hack:
                            CreateThermalKeepouts(vpos, psname,cond,removeCopper,padDict,padstackDict,board,powerNets,'GND')

            # Now do the same with real pins
            for p, pin in enumerate(pins):
                padstack = pin['layout']['layer']['padstack']
                psname = padstack['name']
                pName = pin['name']
                removedOnes = 0
                if footprint_name == 'AWM3300V_BT-BLINDAGE':
                    if pName in ['4','5','6','7']:
                        if 'interne2' in negatives and args.fname == 'GR106772-A':#dirty hack:
                            pt = padstack['pt']
                            point = Point4(pt['x'],pt['y'],pt['o'])
                            vpos = Point2(point.x,point.y)
                            CreateThermalKeepouts(vpos, psname,cond,[2],padDict,padstackDict,board,powerNets,'GND')
                        continue # these will go in an extra footprint
                    elif int(pName) > 7:
                        removedOnes += 4

                # Sanity checks:
                fromTo = padstack['fromTo']
                if not 'To' in fromTo:
                    # SMD pad
                    if fromTo['from'] != 1 and fromTo['from']!=conductorLayers:
                        print('wrong from',fromTo)
                        sys.exit()
                    else:
                        removeCopper = []
                        for rc in range(conductorLayers):
                            subname+= ('0' if (rc+1) in removeCopper else '1')
                        removeCopperDict[p+npoffset-removedOnes] = removeCopper
                        continue
                else:
                    if fromTo['from'] != 1 or fromTo['To']!=conductorLayers:
                        print('wrong fromTo',fromTo)
                        sys.exit()
                    cond = padstack['conductive']['layerNumber']
                    if len(cond) != conductorLayers:
                        print('wrong cond',cond)
                        sys.exit()
                removeCopper = []
                for con in cond:
                    if con['id']<1 or con['id']>conductorLayers:
                        print('wrong id',con)
                        sys.exit()
                    if con['status']=='NOCONNECT' or con['status']=='CLEARANCE':
                        removeCopper.append(int(con['id']))
                pt = padstack['pt']
                point = Point4(pt['x'],pt['y'],pt['o'])
                vpos = Point2(point.x,point.y)
                if len(removeCopper) != 0:
                    # Verify if a track ends on this via, make it stick and add a connection even if NOCONNECT in the pcf, there is a connection in the Gerber
                    removeCopper2 = []
                    netname = ProcessNetName(padstack['net'] if 'net' in padstack.keys() else '',powerNets)
                    netcode = board.GetNetcodeFromNetname(netname)
                    for rm in range(len(removeCopper)):
                        if removeCopper[rm] in [1, conductorLayers] and psname in reconnectDict.keys():
#                            print('Reconnecting copper on Layer',removeCopper[rm],'for ref',reference,'pin',pName,'Net',netname)
                            continue
                        elif removeCopper[rm] in list(range(2, conductorLayers)) and psname in innerReconnectDict.keys():
#                            print('Reconnecting copper on Layer',removeCopper[rm],'for ref',reference,'pin',pName,'Net',netname)
                            continue
                        else:
                            removeCopper2.append(removeCopper[rm])
                    removeCopper = removeCopper2

                for rc in range(conductorLayers):
                    subname+= ('0' if (rc+1) in removeCopper else '1')
                removeCopperDict[p+npoffset-removedOnes] = removeCopper

                # manual clearance with keepouts because diff clearances in diff layers padstack not supported by KiCad
                if 'interne2' in negatives and args.fname == 'GR106772-A':#dirty hack:
                    CreateThermalKeepouts(vpos, psname,cond,removeCopper,padDict,padstackDict,board,powerNets,'GND')

            if subname == '' or all(ch == '1' for ch in subname):
                subname = ''
            else:
                subname = '_'+subname

            # Hack for 3d variations
            if args.fname == 'GR106772-A' and reference in ['ZM1','ZM2','ZM3','ZM4']:
                footprint += '_display'

            module = AddComponent(fpFolder,args.fname,Component(reference,part,package,footprint,outOfBoard,position,outposition,side,placed,lock,angle),board,subname,removeCopperDict,conductorLayers,layerMap)

            # Assign now net
            netcodes = {}
            for p, pin in enumerate(pins):
                pstck = pin['layout']['layer']['padstack']
                netname = ProcessNetName(pstck['net'] if 'net' in pstck.keys() else '',powerNets)
                netcode = board.GetNetcodeFromNetname(netname)
                netcodes[pin['name']] = netcode

            # Look for extra pads
            epcounter = 0
            namedPads = [m.GetName() for m in module.Pads() if m.GetName()!= '']
            removedOnes = 0
            if footprint_name == 'AWM3300V_BT-BLINDAGE':
                removedOnes = 4
            if len(pins) != len(namedPads)+removedOnes:
                print('Looking for external pads at',reference,footprint_name,len(pins),len(namedPads)+removedOnes)
                eps = len(namedPads)+removedOnes - len(pins)
                if eps<=0:
                    print('Wrong eps')
                    sys.exit()
                if not 'layout' in comp.keys():
                    print('Wrong epad struct',comp.keys())
                    sys.exit()
                llayers = comp['layout']['layer']
                if not type(llayers) is list:
                    llayers = [llayers]
                mtl = [l for l in llayers if 'type' in l.keys() and l['type'] == 'conductive' ]
                if len(mtl)==0:
                    print('Non-match mtl',footprint_name)
                    sys.exit()
                if len(mtl)>1:
                    print('Multi mtl not yet implemented',footprint_name)
                    sys.exit()
                ml = mtl[0]
                if layerMap[int(ml['name'])-1] != module.GetLayer():
                    print('ep wrong layer',footprint_name)
                    sys.exit()
                refers = ml['refer']
                if not type(refers) is list:
                    refers = [refers]
                if(len(refers))!=eps:
                    print('Wrong eps count',refers,eps)
                for refer in refers:
                    epcounter += 1
                    pinName = '9' if footprint_name == 'SO08-PPAD_COMPOSANT' else 'D4' if footprint_name == 'SO08-PPAD_SI7884' else 'EP'+str(epcounter) if footprint_name == 'MOL52610-22' else ''
                    if pinName == '':
                        print('Unsupported epad',footprint_name)
                        sys.exit()
                    netname = ProcessNetName(refer['net'] if 'net' in refer.keys() else '',powerNets)
                    netcode = board.GetNetcodeFromNetname(netname)
                    netcodes[pinName] = netcode

            if not module is None:
                for subpad in module.Pads():
                    if subpad.GetName() != '':
                        subpad.SetNetCode(netcodes[subpad.GetName()])
        else:
            module = AddComponent(fpFolder,args.fname,Component(reference,part,package, footprint,outOfBoard,position,outposition,side,placed,lock,angle),board)

        # Fix text and cam position
        if 'layer' in comp['layout']:
            ls = comp['layout']['layer']
            if not type(ls) is list:
                ls = [ls]

            # Silk symbols
            symbols = [l for l in ls if 'symbolText' in l.keys() if l['symbolText']['type'] == 'REFERENCE' if l['name'] in ['Symbol-A','Symbol-B']]
            if len(symbols)>1:
                if len(symbols)==2:
                    text1 = ProcessText(symbols[0]['symbolText'])
                    text2 = ProcessText(symbols[1]['symbolText'])
                    if text1.string != text2.string:
                        print('wrong multitext',text1,text2)
                        sys.exit()
                    if text1.string != module.GetReference():
                        print('wrong multi ref',text,reference)
                        sys.exit()
                    tklay = pcbnew.F_SilkS if 'Symbol-A' == symbols[0]['name'] else pcbnew.B_SilkS
                    ModifyText(tklay, module.Reference(),text1,True, module.GetOrientationDegrees())
                    AddText(board,tklay,text2)
                else:
                    print('Warning: skipping, wrong multisymbol',symbols)
            else:
                if len(symbols)==1:
                    text = ProcessText(symbols[0]['symbolText'])
                    if text.string != module.GetReference():
                        print('wrong ref',text,reference)
                        sys.exit()
                    tklay = pcbnew.F_SilkS if 'Symbol-A' == symbols[0]['name'] else pcbnew.B_SilkS
                    ModifyText(tklay, module.Reference(),text,True, module.GetOrientationDegrees())
                else:
                    module.Reference().SetVisible(False)

            # Fab symbols
            symbols = [l for l in ls if 'symbolText' in l.keys() if l['symbolText']['type'] == 'REFERENCE' if l['name'] in ['Symbol-A-1','Symbol-B-1']]
            if len(symbols)>1:
                if len(symbols)==2:
                    text1 = ProcessText(symbols[0]['symbolText'])
                    text2 = ProcessText(symbols[1]['symbolText'])
                    if text1.string != text2.string:
                        print('wrong multitext',text1,text2)
                        sys.exit()
                    if text1.string != module.GetReference():
                        print('wrong multi ref',text,reference)
                        sys.exit()
                    tklay = pcbnew.F_Fab if 'Symbol-A-1' == symbols[0]['name'] else pcbnew.B_Fab
                    AddText(board,tklay,text1,True)
                    AddText(board,tklay,text2,True)
                else:
                    print('Warning: skipping, wrong multisymbol',symbols)
            else:
                if len(symbols)==1:
                    text = ProcessText(symbols[0]['symbolText'])
                    if text.string != module.GetReference():
                        print('wrong ref',text,reference)
                        sys.exit()
                    tklay = pcbnew.F_Fab if 'Symbol-A-1' == symbols[0]['name'] else pcbnew.B_Fab
                    AddText(board,tklay,text,True)

            # Fix left corner position
            if args.force and reference in ['cam8','cam9'] and 'COIN_EXTERNE' in footprint:
                origPos = [position[0], position[1]]
                idx = -2 if reference == 'cam8' else -1 if reference == 'cam9' else 0
#                print(reference,idx)
                newPos = [ls[0]['surface']['geometry']['surface']['vertex']['pt'][idx]['x'],ls[0]['surface']['geometry']['surface']['vertex']['pt'][idx]['y']]
                deltaPos = [(newPos[0]-origPos[0])/zunit,scaleY*(newPos[1]-origPos[1])/zunit]
                mPos = module.GetPosition()
                module.SetPosition(pcbnew.wxPoint(mPos.x + scale*pcbnew.FromMM(deltaPos[0]), mPos.y + scale*pcbnew.FromMM(deltaPos[1])))
    #                print(deltaPos)
        else:
            print('Warning: no layer',reference,footprint)

    print('Done with modules')

    # Draw the non-conducting stuff
#    nclayers = [l for l in layers if 'type' in l.keys() if l['type']=='nonConductive']
#    slayers = [l for l in layers if 'systemLayer' in l.keys() if re.match('(BOARD_FIGURE|LAYOUT_AREA)',l['systemLayer']['type']) ]
#    drlayers = [l for l in layers if 'drawOf' in l.keys()]
#    ilayers = [l for l in layers if 'infoOf' in l.keys() if 'systemLayer' in l['infoOf'].keys() if re.match('(BOARD_FIGURE)',l['infoOf']['systemLayer']['type']) ]
#    ilayers2 = [l for l in layers if 'infoOf' in l.keys() if 'conductive' in l['infoOf'].keys() ]
    nclayers = [l for l in layers if ('type' in l.keys() and l['type']!='conductive') or (not 'type' in l.keys() and not ('systemLayer' in l.keys() and 'type' in l['systemLayer'] and l['systemLayer']['type']=='PADSTACK'))]
    for i, layer in enumerate(nclayers):
        lkeys = layer.keys()
        lid = layer['name'] if 'name' in lkeys else -1
        klay = pcbnew.Cmts_User if 'infoOf' in lkeys else pcbnew.Dwgs_User if 'drawOf' in lkeys else pcbnew.Eco1_User if 'systemLayer' in lkeys else pcbnew.Eco2_User
        if lid != -1 and layer['name'] in layerDict.keys():
            if type(layerDict[layer['name']]) is list:
                klay = [layerDict2[mlay] for mlay in layerDict[layer['name']]]
            else:
                klay = layerDict2[layerDict[layer['name']]]
        ltype = ltypeMap[lid] if lid in ltypeMap.keys() else ''
        isKeepout = (ltype=='PROHIBIT')
        if isKeepout:
            klay = pcbnew.F_Cu if '_FC' in lid else pcbnew.B_Cu if '_FS' in lid else klay
            if layer['name'] == 'No_PISTE+VIA':
                klay = [pcbnew.F_Cu]
                for i in range(conductorLayers-2):
                    klay.append(pcbnew.F_Cu+i+1)
                klay.append(pcbnew.B_Cu)

        # Sanity check
        possibleKeys = ['text', 'line', 'arc', 'surface', 'area', 'pad', 'dimension', 'info', 'message', 'type', 'name', 'systemLayer', 'drawOf', 'infoOf', 'o']
        for k in lkeys:
            if not k in possibleKeys:
                print('Non-found key',k,layer[k])
                print(sys.exit())

        # Add text fields
        if 'text' in lkeys:
            texts = layer['text']
            if not type(texts) is list:
                texts = [texts]
            for text in texts:
                tobj = ProcessText(text)
                AddText(board,klay,tobj)

        # Add line segments
        if 'line' in lkeys:
            lines = layer['line']
            if not type(lines) is list:
                lines = [lines]
            for line in lines:
                if not type(line) is dict:
                    print('empty line seg',line)
                    sys.exit()

                toSurface = False
                knet = ''
                ow = -1
                if lid in negatives and args.fname == 'GR106772-A':#dirty hack
                    toSurface = True
                    knet = 'GND'

                # Analyze vertex
                if not isKeepout and not toSurface:
                    arcs, lines = ProcessArcsLines(line['geometry']['line'],layer['name'])
                    for line in lines:
                        for subline in line:
                            if any(p.tarc for p in subline.pts):
                                pts = subline.pts
                                tarcs = [p.tarc for p in pts]
                                first = tarcs.index(True)
                                if len(pts) != 6:
                                    print('Non supported tarc len',subline)
                                    sys.exit()
                                if first != 1:
                                    print('Non supported tarc',subline)
                                    sys.exit()
                                if not all(p.w == pts[first].w for p in pts if p.tarc == True):
                                    print('Warning: wrong tarc radius',pts)
                                if pts[0].w != pts[-1].w or pts[0].tarc != False or pts[-1].tarc != False or pts[0].x != pts[-1].x or pts[0].y != pts[-1].y:
                                    print('Non supported tarc p',subline)

                                outlineWidth = pts[0].w
                                radius = pts[first].w
                                pt = pts[1:-1]
                                for j in range(len(pt)):
                                    # Add line and arc
                                    p0 = Point2(pt[j-1].x,pt[j-1].y)
                                    p1 = Point2(pt[j  ].x,pt[j  ].y)
                                    dx = p1.x - p0.x
                                    dy = p1.y - p0.y
                                    d = math.sqrt(pow(dx,2)+pow(dy,2))
                                    ux = dx/d
                                    uy = dy/d
                                    p0x = p0.x + ux*radius
                                    p0y = p0.y + uy*radius
                                    p1x = p1.x - ux*radius
                                    p1y = p1.y - uy*radius
                                    pc0 = Point2(p0x,p0y)
                                    pc1 = Point2(p1x,p1y)
                                    AddOutline([pc0,pc1],board,klay,False,outlineWidth)
                                    vx = uy
                                    vy = -ux
                                    cptx = p1x + vx*radius
                                    cpty = p1y + vy*radius
                                    cpt = Point5(0,cptx,cpty,0)
                                    spt = Point5(0,p1x,p1y,-90)
                                    AddOutline([cpt,spt],board,klay,False,outlineWidth)
                            else:
                                points = RemoveDuplicates(subline.pts)
                                points = [ Track3(pt.x,pt.y,pt.w) for pt in points]
                                AddGraphicLines(points,board,klay)
                    for arc in arcs:
                        for subarc in arc:
                            AddArcObj(subarc,board,klay)
                else:
                    # Analyze vertex
                    vertex = line['geometry']['line']['vertex']

                    # Sanity check
                    possibleKeys = ['arc', 'pt', 'o']
                    for k in vertex.keys():
                        if not k in possibleKeys:
                            print('wrong surf keys',k)
                            print(sys.exit())

                    points = []
    #                width = zone['outlineWidth']
                    if 'pt' in vertex.keys():
                        pts = vertex['pt']
                        if not type(pts) is list:
                            pts = [pts]
                        points += [ Point4(pt['x'],pt['y'],pt['o']) for pt in pts ]
                        if lid in negatives and args.fname == 'GR106772-A':#dirty hack
                            ow = pts[0]['width']
                            if ow != 200000:
                                toSurface = False
                                knet = ''
                    if 'arc' in vertex.keys():
                        arcs = vertex['arc']
                        if not type(arcs) is list:
                            arcs = [arcs]
                        points += ProcessArcs(arcs)# sampling needed because it is zone
                    # Sort resulting polygon by original order ID and close polygon before adding to zone
                    points = sorted(points, key=lambda p: p.o)
                    if len(points) < 2:
                        print('Warning: no valid shapes found')
                        continue
                    if points[0] != points[-1]:
                        points.append( Point4(points[0].x, points[0].y, points[-1].o))#Close polygon
                    points = RemoveDuplicates(points)
                    AddZone(points,board,klay,board.GetNetcodeFromNetname(ProcessNetName(knet,powerNets)),isKeepout and not toSurface,False,False,True, 0, True, 0.0254, ow)

        # Add drill hole annotations
        if 'pad' in lkeys:
            pads = layer['pad']
            if not type(pads) is list:
                pads = [pads]
            for pad in pads:
                string = pad['name']
                position = Point2(pad['pt']['x'],pad['pt']['y'])
                size = Point2(0.5*zunit,0.5*zunit)
                AddText(board,klay,Text(string,position,size,-1,'',0,'NONE',1))

        # Add surfaces
        if 'surface' in lkeys or 'area' in lkeys:
            surfaces = layer['surface'] if 'surface' in lkeys else layer['area']
            if not type(surfaces) is list:
                surfaces = [surfaces]
            for surface in surfaces:
                zone = surface['geometry']['surface']
                # Analyze vertex
                vertex = zone['vertex']

                # Sanity check
                possibleKeys = ['arc', 'pt', 'o']
                for k in vertex.keys():
                    if not k in possibleKeys:
                        print('wrong surf keys',k)
                        print(sys.exit())

                toKeepout = False
                if lid in negatives and args.fname == 'GR106772-A':#dirty hack
                    toKeepout = True

                points = []
#                width = zone['outlineWidth']
                if 'pt' in vertex.keys():
                    pts = vertex['pt']
                    if not type(pts) is list:
                        pts = [pts]
                    if any('tarc' in p.keys() for p in pts):
                        if not all('tarc' in p.keys() for p in pts):
                            print('Warning: tarc mix still unsupport',lid,pts)
                        else:
                            if not all(p['tarc']['r'] == pts[0]['tarc']['r'] for p in pts):
                                print('Warning: wrong tarc radius',pts)
                            radius = pts[0]['tarc']['r']
                            for j in range(len(pts)):# we start at 0 to get j-1 from last list element, to close outline
                                # Add line and arc
                                p0 = Point2(pts[j-1]['x'],pts[j-1]['y'])
                                p1 = Point2(pts[j  ]['x'],pts[j  ]['y'])
                                dx = p1.x - p0.x
                                dy = p1.y - p0.y
                                d = math.sqrt(pow(dx,2)+pow(dy,2))
                                ux = dx/d
                                uy = dy/d
                                p0x = p0.x + ux*radius
                                p0y = p0.y + uy*radius
                                p1x = p1.x - ux*radius
                                p1y = p1.y - uy*radius
                                pc0 = Point2(p0x,p0y)
                                pc1 = Point2(p1x,p1y)
                                points.append(Point4(pc0.x,pc0.y,pts[j]['o'],outlineWidth))
                                points.append(Point4(pc1.x,pc1.y,pts[j]['o'],outlineWidth))
                                vx = uy
                                vy = -ux
                                cptx = p1x + vx*radius
                                cpty = p1y + vy*radius
                                resolution = 0.1*zunit
                                perimeter = 2*math.pi*radius/4
                                subdivisions = int(perimeter/resolution)
                                subdivisions = max(subdivisions,3) # Avoid division by zero later on
                                dalpha = -90
                                start_angle = math.atan2(p1y-cpty, p1x-cptx) % (2*math.pi)
                                for s in range(subdivisions+1):
                                    alpha = start_angle + s/subdivisions*dalpha*math.pi/180
                                    points.append(Point4(cptx+radius*math.cos(alpha),cpty+radius*math.sin(alpha),pts[j]['o'],outlineWidth))
                    else:
                        points += [ Point4(pt['x'],pt['y'],pt['o']) for pt in pts ]
                if 'arc' in vertex.keys():
                    arcs = vertex['arc']
                    if not type(arcs) is list:
                        arcs = [arcs]
                    points += ProcessArcs(arcs)# sampling needed because it is zone
                # Sort resulting polygon by original order ID and close polygon before adding to zone
                points = sorted(points, key=lambda p: p.o)
                if len(points) < 2:
                    print('Warning: no valid shapes found',lid,vertex['pt'])
                    continue
                if points[0] != points[-1]:
                    points.append( Point4(points[0].x, points[0].y, points[-1].o))#Close polygon
                points = RemoveDuplicates(points)
                AddZone(points,board,klay,board.GetNetcodeFromNetname(ProcessNetName('',powerNets)),isKeepout or toKeepout,False or toKeepout,True, True)

        # Add dimension fields
        if 'dimension' in lkeys:
            # Prevent dimensions in Silk/Fab layer
            if klay in [pcbnew.F_Fab,pcbnew.F_SilkS]:
                dlay = pcbnew.Dwgs_User
            elif klay in [pcbnew.B_Fab,pcbnew.B_SilkS]:
                dlay = pcbnew.Cmts_User
            else:
                dlay = klay

            dimensions = layer['dimension']
            if not type(dimensions) is list:
                dimensions = [dimensions]
            for dimension in dimensions:

                geometry = dimension['geometry']
                # Sanity-check
                possibleKeys = ['linearDimension', 'radiusDimension', 'o']
                for k in geometry.keys():
                    if not k in possibleKeys:
                        print('wrong dimension keys',k)
                        print(sys.exit())

                modes = ['linearDimension', 'radiusDimension']
                for mode in modes:
                    if not mode in geometry.keys():
                        continue
                    d = geometry[mode]

    #                arrowDir = d['arrowDir']
                    lineOmit = d['lineOmit']
                    if lineOmit != 'NO':
                        print('Warning: skipping arrow')
                        continue

                    for field in ['str', 'postStr', 'toleStr']:
                        tobj = ProcessText(d['text'],field)
                        AddText(board,dlay,tobj)

                    if mode == 'linearDimension':
                        arrow = []
                        for tag in ['arrowPoint', 'assistPoint']:
                            begin =  d['beginPoint']['assistArrow'][tag]['pt']
                            end   =  d[  'endPoint']['assistArrow'][tag]['pt']
                            p0 = Point4( begin['x'], begin['y'], begin['o'])
                            p1 = Point4(   end['x'],   end['y'],   end['o'])
                            arrow.append(p0)
                            arrow.append(p1)
                        AddDimension(board,dlay,arrow[0],arrow[1],arrow[2],arrow[3])
                    elif mode == 'radiusDimension':
                        arrow = []
                        for tag in ['centerPoint', 'arcPoint']:
                            pt = d[tag]['arrow']['arrowPoint']['pt']
                            p = Point4( pt['x'], pt['y'], pt['o'])
                            arrow.append(p)
                        AddDimension(board,dlay,arrow[0],arrow[1],arrow[0],arrow[1])

        # Add info
        if 'info' in lkeys:
            infos = layer['info']
            if not type(infos) is list:
                infos = [infos]
            for info in infos:
                # Sanity check
                possibleKeys = ['infoId','infoTagRef','box','o']
                for k in info.keys():
                    if not k in possibleKeys:
                        print('wrong info keys',k)
                        print(sys.exit())

                if 'box' in info.keys():
                    box = info['box']
                    p0 = Point4(box['pt'][0]['x'], box['pt'][0]['y'], box['pt'][0]['o'])
                    p1 = Point4(box['pt'][1]['x'], box['pt'][1]['y'], box['pt'][1]['o'])
    #                AddBox(board,klay,p0,p1,0.1*zunit)
    # TODO Add messages and reenable AddBox above!
#        if 'message' in lkeys:

    # Post-processing
    if args.remap != '':
        remapName = os.path.join(args.path,args.remap)
        print('Post-processing',remapName)
        remap = json.load(open(remapName))

        if 'footprints' in remap.keys():
            footprints = remap['footprints']
            for fp in footprints:
                libid = fp['lib'] if fp['lib'] != './' else args.path
                footprint_name = fp['name']
                ref = fp['reference']
                pos = fp['position']
                ang = fp['orientation']
                nets = fp['nets']
                flip = fp['flip'] == 'True'
                # Create module
                footprint = pcbnew.FootprintLoad(libid, footprint_name)
                #https://www.reddit.com/r/KiCad/comments/cfejdz/place_a_component_with_a_python_script/
                if footprint is None:
                    print('error: footprint',footprint_name,'not found')
                    return None
                m = pcbnew.MODULE(footprint)
                pcbnew.FootprintSave(fpFolder,m)# Copy it to footprint folder
                fpid = pcbnew.LIB_ID(args.fname, footprint_name)
                m.SetFPID(fpid)
                m.SetReference(ref)
                m.Reference().SetVisible(False)
                m.SetValue(footprint_name)
                m.Value().SetVisible(False)
                m.SetPosition(pcbnew.wxPointMM(pos[0],pos[1]))
                m.SetOrientationDegrees(ang)
                m.SetIsPlaced(True)
                m.SetLocked(True)
                m.SetLayer(pcbnew.F_Cu)
                if flip:
                    m.Flip(m.GetPosition())
                board.Add(m)
                allfps.add(footprint_name)

                for j, p in enumerate(m.Pads()):
                    try:
                        netcode = board.GetNetcodeFromNetname(nets[j])
                    except IndexError as e:
                        print(nets[j],footprint)
                        print(e)
                        sys.exit()
                    p.SetNetCode(netcode)

        if 'polygons' in remap.keys():
            repolygons = remap['polygons']
            for rp in repolygons:
                ptype = rp['type']
                klay = layerDict2[rp['layer']]
                netcode = rp['net']
                if ptype == 'zone2polygon':
                    tracks = rp['tracks']
                    kx = pcbnew.FromMM(rp['pos'][0])
                    ky = pcbnew.FromMM(rp['pos'][1])
                    # This is a workaround due to https://gitlab.com/kicad/code/kicad/-/issues/4373
                    mtz = [z for z in board.Zones() if z.GetNetCode() == netcode if z.GetLayer() == klay if z.Outline().Outline(0).Point(0).x == kx if  z.Outline().Outline(0).Point(0).y == ky]
                    if len(mtz)==0:
                        print('Did not find a matching zone',rp)
                        sys.exit()
                    elif len(mtz)>1:
                        print('Ambiguous match zone',rp)
                        sys.exit()
                    z = mtz[0]
                    outl = z.Outline().Outline(0)
                    woutl = []
                    for o in range(outl.PointCount()):
                        woutl.append(pcbnew.wxPoint(outl.Point(o).x,outl.Point(o).y))
                    if 'cutouts' in rp.keys():
                        cutouts = rp['cutouts']
                        for cp in cutouts:
                            # Find joining point
                            jx = pcbnew.FromMM(cp['joint'][0])
                            jy = pcbnew.FromMM(cp['joint'][1])
                            bx = pcbnew.FromMM(cp['before'][0])
                            by = pcbnew.FromMM(cp['before'][1])
                            ax = pcbnew.FromMM(cp['after'][0])
                            ay = pcbnew.FromMM(cp['after'][1])
                            before = woutl.index(pcbnew.wxPoint(bx,by))
                            after = woutl.index(pcbnew.wxPoint(ax,ay))
                            if after == 0:# last point
                                if woutl[before+1] == woutl[0]:
                                    after = before + 1
                            if abs(before-after)!=1:
                                print('Wrong bef aft points',before,after)
                                print(woutl[before-1:before+1])
                                sys.exit()
                            insertIndex = min(before,after)+1
                            # Insert joint point twice
                            woutl.insert(insertIndex,pcbnew.wxPoint(jx,jy))
                            woutl.insert(insertIndex,pcbnew.wxPoint(jx,jy))
                            insertIndex += 1
                            # Insert now cutout
                            reverse = (cp['reverse']=='True')
                            clay = layerDict2[cp['layer']]
                            cnetcode = cp['net']
                            cx = pcbnew.FromMM(cp['pos'][0])
                            cy = pcbnew.FromMM(cp['pos'][1])
                            mtc = [c for c in board.Zones() if c.GetNetCode() == cnetcode if c.GetLayer() == clay if c.Outline().Outline(0).Point(0).x == cx if  c.Outline().Outline(0).Point(0).y == cy]
                            if len(mtc)==0:
                                print('Did not find a matching keepout',cp)
                                sys.exit()
                            elif len(mtc)>1:
                                print('Ambiguous match keepout',cp)
                                sys.exit()
                            c = mtc[0]
                            coutl = c.Outline().Outline(0)
                            ccoutl = []
                            for o in range(coutl.PointCount()):
                                ccoutl.append(pcbnew.wxPoint(coutl.Point(o).x,coutl.Point(o).y))
                            if 'joint2' in cp.keys():
                                j2x = pcbnew.FromMM(cp['joint2'][0])
                                j2y = pcbnew.FromMM(cp['joint2'][1])
                                for o in range(coutl.PointCount()):
                                    if coutl.Point(o).x == j2x and coutl.Point(o).y == j2y:
                                        ccoutl = ccoutl[o:] + ccoutl[1:o+1]
                                        break
                            wcoutl = []
                            for o in range(coutl.PointCount()):
                                wcoutl.append(ccoutl[o])
                            if reverse:
                                wcoutl.reverse()
    #                        https://www.geeksforgeeks.org/python-insert-list-in-another-list/
                            woutl[insertIndex:insertIndex] = wcoutl

                    AddPolygon(woutl, board, klay)

                    if len(tracks)>0:
                        for subtr in tracks:
                            toutl = [Track3(p['pos'][0],p['pos'][1],p['width']) for p in subtr]
                            AddTrackRaw(toutl,board,klay,netcode) # to effectively connect the nets, as polygon has no net

                    board.Delete(z)
#                    https://forum.kicad.info/t/deleting-tracks-with-python/12895
                elif ptype == 'zone2keepout':
#                    tracks = rp['tracks']
                    kx = pcbnew.FromMM(rp['pos1'][0])
                    ky = pcbnew.FromMM(rp['pos1'][1])
                    # This is a workaround due to https://gitlab.com/kicad/code/kicad/-/issues/4373
                    mtz = [z for z in board.Zones() if z.GetNetCode() == netcode if z.GetLayer() == klay if z.Outline().Outline(0).Point(0).x == kx if  z.Outline().Outline(0).Point(0).y == ky]
                    if len(mtz)==0:
                        print('Did not find a matching zone1',rp)
                        sys.exit()
                    elif len(mtz)>1:
                        print('Ambiguous match zone1',rp)
                        sys.exit()
                    z1 = mtz[0]
                    kx = pcbnew.FromMM(rp['pos2'][0])
                    ky = pcbnew.FromMM(rp['pos2'][1])
                    # This is a workaround due to https://gitlab.com/kicad/code/kicad/-/issues/4373

                    outl1 = z1.Outline().Outline(0)
                    woutl1 = []
                    for o in range(outl1.PointCount()):
                        woutl1.append(pcbnew.wxPoint(outl1.Point(o).x,outl1.Point(o).y))

                    # Find joining points
                    bx = pcbnew.FromMM(rp['before1'][0])
                    by = pcbnew.FromMM(rp['before1'][1])
                    ax = pcbnew.FromMM(rp['after1'][0])
                    ay = pcbnew.FromMM(rp['after1'][1])
                    before1 = woutl1.index(pcbnew.wxPoint(bx,by))
                    after1 = woutl1.index(pcbnew.wxPoint(ax,ay))
                    if after1 == 0:# last point
                        if woutl1[before1+1] == woutl1[0]:
                            after1 = before1 + 1
                    if abs(before1-after1)!=1:
                        print('Wrong bef1 aft1 points',before1,after1)
                        print(woutl[before1-1:before1+1])
                        sys.exit()
                    insertIndex = min(before1,after1)+1
                    # Insert joint point twice
#                    woutl.insert(insertIndex,pcbnew.wxPoint(jx,jy))
#                    woutl.insert(insertIndex,pcbnew.wxPoint(jx,jy))
#                    insertIndex += 1

                    # Find joining points of second zone
                    mtz = [z for z in board.Zones() if z.GetNetCode() == netcode if z.GetLayer() == klay if z.Outline().Outline(0).Point(0).x == kx if  z.Outline().Outline(0).Point(0).y == ky]
                    if len(mtz)==0:
                        print('Did not find a matching zone2',rp)
                        sys.exit()
                    elif len(mtz)>1:
                        print('Ambiguous match zone2',rp)
                        sys.exit()
                    z2 = mtz[0]

                    outl2 = z2.Outline().Outline(0)
                    woutl2 = []
                    for o in range(outl2.PointCount()):
                        woutl2.append(pcbnew.wxPoint(outl2.Point(o).x,outl2.Point(o).y))

                    bx = pcbnew.FromMM(rp['before2'][0])
                    by = pcbnew.FromMM(rp['before2'][1])
                    ax = pcbnew.FromMM(rp['after2'][0])
                    ay = pcbnew.FromMM(rp['after2'][1])
                    before2 = woutl2.index(pcbnew.wxPoint(bx,by))
                    after2 = woutl2.index(pcbnew.wxPoint(ax,ay))
                    if after2 == 0:# last point
                        if woutl2[before2+1] == woutl2[0]:
                            after2 = before2 + 1
                    if abs(before2-after2)!=1:
                        print('Wrong bef1 aft1 points',before2,after2)
                        print(woutl[before2-1:before2+1])
                        sys.exit()
                    jointIndex = min(before2,after2)+1
                    woutl2 = woutl2[jointIndex:] + woutl2[1:jointIndex]
                    reverse2 = (rp['reverse2']=='True')
                    skip2 = (rp['skip2']=='True')
                    if reverse2:
                        woutl2.reverse()
                    if skip2:
                        woutl2.pop(0)
                        woutl2.pop()
                    skip1 = (rp['skip1']=='True')
                    if skip1:
                        woutl1.pop(insertIndex-1)
                        woutl1.pop(insertIndex-1)
                        insertIndex -= 1

#                        https://www.geeksforgeeks.org/python-insert-list-in-another-list/
                    woutl1[insertIndex:insertIndex] = woutl2

                    ze = pcbnew.ZONE_CONTAINER(board)
                    ze.SetLayer(z1.GetLayer())
                    ze.SetNetCode(z1.GetNetCode())
                    bOutline = pcbnew.wxPoint_Vector()
                    for point in woutl1:
                        bOutline.append(point)
                    ze.AddPolygon(bOutline)
                    ze.SetIsKeepout(z1.GetIsKeepout())
                    ze.SetDoNotAllowCopperPour(z1.GetDoNotAllowCopperPour())
                    ze.SetDoNotAllowTracks(z1.GetDoNotAllowTracks())
                    ze.SetDoNotAllowVias(z1.GetDoNotAllowVias())
                    ze.SetZoneClearance(z1.GetZoneClearance())
                    ze.SetMinThickness(z1.GetMinThickness())
                    ze.SetPadConnection(z1.GetPadConnection())
                    ze.SetLocked(z1.IsLocked())

                    board.Delete(z1)
                    board.Delete(z2)
                    board.Add(ze)

#                    https://forum.kicad.info/t/deleting-tracks-with-python/12895
                else:
                    print('Unsupported ptype',rp)
                    sys.exit()


        if 'texts' in remap.keys():
            retexts = remap['texts']
            for rt in retexts:
                klay = layerDict2[rt['layer']]

                pos = rt['oldPos']
                ktp = pcbnew.wxPointMM(pos[0],pos[1])
                mte = [t for t in board.GetDrawings() if t.GetPosition() == ktp and t.GetLayer() == klay]
                if len(mte)==0:
                    print('Did not find a matching text',rt)
                    continue
                elif len(mte)>1:
                    print('Ambiguous match text',rt)
                    sys.exit()
                pos = rt['newPos']
                ktp = pcbnew.wxPointMM(pos[0],pos[1])
                te = mte[0]
                te.SetPosition(ktp)
                if 'scale' in rt.keys():
                    shrinkFactor = rt['scale']
                    te.SetTextSize(pcbnew.wxSize(shrinkFactor*te.GetTextSize().x, shrinkFactor*te.GetTextSize().y))

        if 'tracks' in remap.keys():
            retracks = remap['tracks']
            for rt in retracks:
                klay = layerDict2[rt['layer']]
                try:
                    ncod = board.GetNetcodeFromNetname(rt['netname'])
                except IndexError as e:
                    print(e)
                    print(ncod,rt['netname'])
                    sys.exit()

#                print(rt,klay,ncod)
                if 'oldPos' in rt.keys():
                    pos = rt['oldPos']
                    sp = pos['start']
                    ep = pos['end']
                    ksp = pcbnew.wxPointMM(sp[0],sp[1])
                    kep = pcbnew.wxPointMM(ep[0],ep[1])
                    mtr = [t for t in board.GetTracks() if t.GetStart() == ksp and t.GetEnd() == kep and t.GetLayer() == klay and t.GetNetCode() == ncod]
                    if len(mtr)==0:
                        print('Did not find a matching track',rt)
                        sys.exit()
                    elif len(mtr)>1:
                        print('Ambiguous match track',rt)
                        sys.exit()
                    tr = mtr[0]
                else:
                    tr = pcbnew.TRACK(board)
                    tr.SetLayer(klay)
                    tr.SetNetCode(ncod)
                    tr.SetLocked(True)
                    board.Add(tr)
                if 'newPos' in rt.keys():
                    pos = rt['newPos']
                    sp = pos['start']
                    ep = pos['end']
                    ksp = pcbnew.wxPointMM(sp[0],sp[1])
                    kep = pcbnew.wxPointMM(ep[0],ep[1])
                    tr.SetStart(ksp)
                    tr.SetEnd(kep)
                if 'newname' in rt.keys():
                    try:
                        ncod = board.GetNetcodeFromNetname(rt['newname'])
                    except IndexError as e:
                        print(e)
                        print(ncod,rt['newname'])
                        sys.exit()
                    tr.SetNetCode(ncod)
                if 'oldPos' in rt.keys() and not 'newPos' in rt.keys() and not 'newname' in rt.keys():
                    board.Delete(tr)

        if 'symbols' in remap.keys():
            symbols = remap['symbols']
            for key, value in symbols.items():
                mtm = [m for m in board.GetModules() if m.GetReference() == key]
                if len(mtm)==0:
                    print('Did not find a matching module',key,value)
                    continue
                elif len(mtm)>1:
                    print('Ambiguous match module',key,value)
                    sys.exit()
                m = mtm[0]
                m.SetReference(value)

        if 'nets' in remap.keys():
            renets = remap['nets']
            for rn in renets:
                reference = rn['symbol']
                pName = rn['pin']
                netname = rn['netname']
                code = board.GetNetcodeFromNetname(netname)
                newname = rn['newname']
                newcode = board.GetNetcodeFromNetname(newname)
                mtm = [m for m in board.GetModules() if m.GetReference() == reference]
                if len(mtm)==0:
                    print('Did not find a matching ref module',reference)
                    sys.exit()
                elif len(mtm)>1:
                    print('Ambiguous match ref module',reference)
                    sys.exit()
                m = mtm[0]
                for p in m.Pads():
                    if p.GetName() == pName and p.GetNetCode() == code:
                        p.SetNetCode(newcode)

        if 'zones' in remap.keys():
            rezones = remap['zones']
            for rz in rezones:
                klay = layerDict2[rz['layer']]
                netcode = rz['net']
                kx = pcbnew.FromMM(rz['pos'][0])
                ky = pcbnew.FromMM(rz['pos'][1])
                mtz = [z for z in board.Zones() if z.GetNetCode() == netcode if z.GetLayer() == klay if z.Outline().Outline(0).Point(0).x == kx if  z.Outline().Outline(0).Point(0).y == ky]
                if len(mtz)==0:
                    print('Did not find a matching rezone',rz)
                    sys.exit()
                elif len(mtz)>1:
                    print('Ambiguous match rezone',rz)
                    sys.exit()
                z = mtz[0]
                if 'tvp' in rz.keys():
                    tracks = (rz['tvp'][0]==1)
                    vias = (rz['tvp'][1]==1)
                    pour = (rz['tvp'][2]==1)
                    z.SetDoNotAllowTracks(tracks)
                    z.SetDoNotAllowVias(vias)
                    z.SetDoNotAllowCopperPour(pour)

    # Fill zones
    print('Filling zones...')
    filler = pcbnew.ZONE_FILLER(board)
    zones = board.Zones()
    filler.Fill(zones)
    filler.Fill(zones)# Fill again is needed sometimes, see https://gitlab.com/kicad/code/kicad/-/issues/4436

    # Save PCB to disk
    SaveFile(board, args)

    # Generate Gerbers
    GenerateGerbers(board, args.fname, args.outdir, conductorLayers)
    print('************************************************************************************')

#    snapshot = tracemalloc.take_snapshot()
#    top_stats = snapshot.statistics('traceback')
#    for stat in top_stats[:10]:
#        print("%s memory blocks: %.1f KiB" % (stat.count, stat.size / 1024))
#        for line in stat.traceback.format():
#            print(line)


    # Print out some info for synchro w schematic
    synch = open(os.path.join(args.outdir,'eeschema_sync.txt'), 'w')
    print('Board',args.fname,'\n',file=synch)
    # ~ print('Via nets',file=synch)
    viaDict = {}
    for m in board.GetModules():
        if m.GetReference()[0:2] == 'VI':
            reference = m.GetReference()
            netname = m.Pads().GetNetname()
            if not netname in viaDict.keys():
                viaDict[netname] = []
            viaDict[netname].append(reference)
#            print(reference,netname,file=synch)
    GenerateHoleSheet(viaDict,os.path.join(args.outdir,'holes.sch')) # holy shit
    print('',file=synch)
    print('Unique footprint names',file=synch)
    for f in sorted(allfps):
        print(f,file=synch)
    print('',file=synch)
    print('Unique nets at module pins',file=synch)
    allnets = set([])
    for m in board.GetModules():
        if m.GetReference()[0:2] != 'VI':
            for p in m.Pads():
                if p.GetNetname()!='':
                    if not p.GetNetname() in allnets:
                        allnets.add(p.GetNetname())
                        print(m.GetReference(),p.GetName(),p.GetNetname(),file=synch)
    synch.close()

if __name__ == '__main__':
    main(sys.argv[0], sys.argv[1:])
#    print('Main')
#    viaDict = {
#            "GND": ['V1', 'V4', 'V5', 'V1', 'V4', 'V5', 'V1', 'V4', 'V5', 'V1', 'V4', 'V5','V1', 'V4', 'V5'],
#            "ANA": ['V8', 'V9']
#            }
#    GenerateHoleSheet(viaDict,'/tmp/holes.sch')
