Info: Processing component “C1:/5E8FB3FB:AllModules:C0603_1b”.
Info: Change symbol path “PT2:” to “/5E9E73E6”.
Info: Change symbol path “C1:” to “/5E8FB3FB”.
Info: Processing component “PT2:/5E9E73E6:AllModules:MIRECMS_D-1.5_1”.
Info: Change symbol path “PT1:” to “/5E9E685B”.
Info: Processing component “PT1:/5E9E685B:AllModules:MIRECMS_D-1.5_1”.
Info: Change symbol path “J6:” to “/5E9A6A5B”.
Info: Processing component “C2:/5E90C557:AllModules:C0603_1b”.
Info: Processing component “J6:/5E9A6A5B:AllModules:GKS913_1”.
Info: Change symbol path “C2:” to “/5E90C557”.
Info: Change symbol path “J5:” to “/5E99E657”.
Info: Processing component “J5:/5E99E657:AllModules:811S1-02_1”.
Info: Change symbol path “J4:” to “/5E99CA09”.
Info: Processing component “J4:/5E99CA09:AllModules:811S1-02_1”.
Info: Processing component “C3:/5E91F4F8:AllModules:C0402_1b”.
Info: Change symbol path “J3:” to “/5E99A69C”.
Info: Change symbol path “C3:” to “/5E91F4F8”.
Info: Processing component “J3:/5E99A69C:AllModules:811S1-02_1”.
Info: Change symbol path “J1:” to “/5E95772A”.
Info: Processing component “J1:/5E95772A:AllModules:BP2X5C_1”.
Info: Change symbol path “FI3:” to “/5E9C95BC”.
Info: Processing component “C4:/5E9213A5:AllModules:C0402_1b”.
Info: Processing component “FI3:/5E9C95BC:AllModules:C0603_1b”.
Info: Change symbol path “C4:” to “/5E9213A5”.
Info: Change symbol path “FI2:” to “/5E9D0294”.
Info: Processing component “FI2:/5E9D0294:AllModules:C0603_1b”.
Info: Change symbol path “FI1:” to “/5E8EDF96”.
Info: Processing component “FI1:/5E8EDF96:AllModules:BNX002_1”.
Info: Processing component “CAM1:/5E9EFCDE:AllModules:FILM18_HORIZONTAL_1”.
Info: Change symbol path “CAM16:” to “/5E9FCA1F”.
Info: Change symbol path “CAM1:” to “/5E9EFCDE”.
Info: Processing component “CAM2:/5E9F043D:AllModules:A3_EQFC_COVIDIEN_1”.
Info: Processing component “CAM16:/5E9FCA1F:AllModules:FEE_COMP_1”.
Info: Change symbol path “CAM2:” to “/5E9F043D”.
Info: Processing component “CAM3:/5E9F1828:AllModules:PER-COVIDIEN_TEXTE_1”.
Info: Change symbol path “CAM14:” to “/5E9FCA0B”.
Info: Change symbol path “CAM3:” to “/5E9F1828”.
Info: Processing component “CAM4:/5E9F1832:AllModules:A3_EQFS_COVIDIEN_1”.
Info: Processing component “CAM14:/5E9FCA0B:AllModules:PERCAGE-MC_TABLEAU_1”.
Info: Change symbol path “CAM4:” to “/5E9F1832”.
Info: Processing component “CAM6:/5E9F54AD:AllModules:COIN_EXTERNE_1”.
Info: Change symbol path “CAM12:” to “/5E9FC9F7”.
Info: Change symbol path “CAM6:” to “/5E9F54AD”.
Info: Processing component “CAM7:/5E9F54B7:AllModules:COIN_EXTERNE_1”.
Info: Processing component “CAM12:/5E9FC9F7:AllModules:FABRICATION-FEE_1”.
Info: Change symbol path “CAM7:” to “/5E9F54B7”.
Info: Processing component “CAM8:/5E9F54C1:AllModules:COIN_EXTERNE_1”.
Info: Change symbol path “CAM11:” to “/5E9FC9ED”.
Info: Change symbol path “CAM8:” to “/5E9F54C1”.
Info: Processing component “CAM9:/5E9FC9D9:AllModules:COIN_EXTERNE_1”.
Info: Processing component “CAM11:/5E9FC9ED:AllModules:FEE_COMP_2”.
Info: Change symbol path “CAM9:” to “/5E9FC9D9”.
Info: Processing component “CAM10:/5E9FC9E3:AllModules:A3_PER_COVIDIEN_1”.
Info: Change symbol path “CAM10:” to “/5E9FC9E3”.
Change CAM10 value from A3_PER_COVIDIEN to Fiducial.
Change CAM9 value from COIN_EXTERNE to Fiducial.
Change CAM11 value from FEE_COMP to Fiducial.
Change CAM8 value from COIN_EXTERNE to Fiducial.
Change CAM7 value from COIN_EXTERNE to Fiducial.
Change CAM12 value from FABRICATION-FEE to Fiducial.
Change CAM6 value from COIN_EXTERNE to Fiducial.
Change CAM4 value from A3_EQFS_COVIDIEN to Fiducial.
Change CAM14 value from PERCAGE-MC_TABLEAU to Fiducial.
Change CAM3 value from PER-COVIDIEN_TEXTE to Fiducial.
Change CAM2 value from A3_EQFC_COVIDIEN to Fiducial.
Change CAM16 value from FEE_COMP to Fiducial.
Change CAM1 value from FILM18_HORIZONTAL to Fiducial.
Reconnect C4 pin 2 from T to GND.
Change FI1 value from FL-10-BNX002-01 to BNX002-11.
Add net GND.
Disconnect FI1 pin B.
Disconnect FI1 pin CB.
Disconnect FI1 pin CG1.
Disconnect FI1 pin CG2.
Disconnect FI1 pin CG3.
Disconnect FI1 pin PSG.
Reconnect C4 pin 1 from CG to /T.
Change FI2 value from SF-BC-S0603 to 74279266.
Add net /T.
Add net /BAT_EEPROM.
Reconnect FI2 pin 1 from EEPROM_BAT to /BAT_EEPROM.
Add net Net-(FI2-Pad2).
Reconnect FI2 pin 2 from N$5 to Net-(FI2-Pad2).
Change C4 value from C-BC-C0402 to 0402_47pF_C0402C470J5GAC.
Change FI3 value from SF-BC-S0603 to 74279266.
Reconnect C3 pin 2 from EEPROM_BAT to GND.
Add net Net-(FI3-Pad1).
Reconnect FI3 pin 1 from N$4 to Net-(FI3-Pad1).
Add net /T.
Reconnect FI3 pin 2 from T to /T.
Add net GND.
Change J1 value from F-CO-HE10-2010C to 09185107323.
Reconnect C3 pin 1 from CG to /BAT_EEPROM.
Add net Earth.
Reconnect J1 pin 1 from PSG to Earth.
Add net Earth.
Reconnect J1 pin 2 from PSG to Earth.
Add net Earth.
Reconnect J1 pin 3 from PSG to Earth.
Add net Earth.
Reconnect J1 pin 4 from PSG to Earth.
Add net Net-(FI3-Pad1).
Reconnect J1 pin 5 from N$4 to Net-(FI3-Pad1).
Add net Net-(FI2-Pad2).
Reconnect J1 pin 6 from N$5 to Net-(FI2-Pad2).
Add net VDD.
Reconnect J1 pin 7 from B to VDD.
Add net VDD.
Reconnect J1 pin 8 from B to VDD.
Add net VDD.
Reconnect J1 pin 9 from B to VDD.
Add net VDD.
Reconnect J1 pin 10 from B to VDD.
Add net /BAT_EEPROM.
Change J3 value from BAR-CMS254-811S1-02 to 811-S1-002-30-014101.
Change C3 value from C-BC-C0402 to 0402_47pF_C0402C470J5GAC.
Add net /+.
Reconnect J3 pin 1 from CB to /+.
Add net /+.
Reconnect J3 pin 2 from CB to /+.
Reconnect C2 pin 2 from CB to /+.
Change J4 value from BAR-CMS254-811S1-02 to 811-S1-002-30-014101.
Add net /+.
Add net /BAT_EEPROM.
Reconnect J4 pin 1 from EEPROM_BAT to /BAT_EEPROM.
Add net /BAT_EEPROM.
Reconnect J4 pin 2 from EEPROM_BAT to /BAT_EEPROM.
Reconnect C2 pin 1 from CG to GND.
Change J5 value from BAR-CMS254-811S1-02 to 811-S1-002-30-014101.
Add net GND.
Add net /T.
Reconnect J5 pin 1 from T to /T.
Add net /T.
Reconnect J5 pin 2 from T to /T.
Change C2 value from C-BC-C0603 to 10nF.
Change J6 value from PT-10-GKS913 to GKS-913-0024.
Reconnect C1 pin 2 from B to VDD.
Add net GND.
Reconnect J6 pin 1 from CG to GND.
Add net VDD.
Change PT1 value from MIRECMS to TestPoint.
Reconnect C1 pin 1 from PSG to Earth.
Add net GND.
Connect PT1 pin 1 to GND.
Add net Earth.
Change PT2 value from MIRECMS to TestPoint.
Change C1 value from C-BC-C0603 to 10nF.
Add net GND.
Connect PT2 pin 1 to GND.
Reconnect copper zone from CG to GND.
Warning: Copper zone (CG) has no pads connected.
Warning: Copper zone (CG) has no pads connected.
Warning: Copper zone (CG) has no pads connected.
